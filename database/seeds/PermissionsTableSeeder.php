<?php

use Illuminate\Database\Seeder;
use Carfid\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        Permission::create(['code' => $faker->unique()->regexify('[A-Z0-9]{8}'), 'name' => 'listar', 'description' => 'brinda acceso al módulo', 'creator_user' => 'configuraciones iniciales']);
        Permission::create(['code' => $faker->unique()->regexify('[A-Z0-9]{8}'), 'name' => 'crear', 'description' => 'otorga el permiso de creación en un módulo', 'creator_user' => 'configuraciones iniciales']);
        Permission::create(['code' => $faker->unique()->regexify('[A-Z0-9]{8}'), 'name' => 'ver', 'description' => 'otorga el permiso de ver en un módulo', 'creator_user' => 'configuraciones iniciales']);
        Permission::create(['code' => $faker->unique()->regexify('[A-Z0-9]{8}'), 'name' => 'actualizar', 'description' => 'otorga el permiso de actualizar en un módulo', 'creator_user' => 'configuraciones iniciales']);
        Permission::create(['code' => $faker->unique()->regexify('[A-Z0-9]{8}'), 'name' => 'eliminar', 'description' => 'otorga el permiso de eliminar en un módulo', 'creator_user' => 'configuraciones iniciales']);
    }
}

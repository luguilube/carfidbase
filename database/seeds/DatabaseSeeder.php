<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carfid\Models\Company;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersRolesTableSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(RolesModulesPermissionsTableSeeder::class);
        $this->call(OptionsTableSeeder::class);
        // Model::unguard();
        // factory(Company::class,10)->create();
        // Model::reguard();
        // $this->call(UsersCompaniesTableSeeder::class);
    }
}

<?php

use Illuminate\Database\Seeder;
use Carfid\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(['name' => 'amin', 'email' => 'oscarenriquex@gmail.com', 'password' => bcrypt('admin1234'), 'phone_number' => null, 'creator_user' => 'configuraciones iniciales']);
    }
}

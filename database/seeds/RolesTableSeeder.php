<?php

use Illuminate\Database\Seeder;
use Carfid\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'usuario', 'description' => 'rol por defecto', 'creator_user' => 'configuraciones iniciales']);
        Role::create(['name' => 'super amin', 'description' => 'rol super administrador', 'creator_user' => 'configuraciones iniciales']);
    }
}

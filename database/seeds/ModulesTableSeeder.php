<?php

use Illuminate\Database\Seeder;
use Carfid\Models\Module;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Module::create(['name' => 'empresas', 'description' => 'brinda acceso al módulo de empresas', 'creator_user' => 'configuraciones iniciales']);
        Module::create(['name' => 'usuarios', 'description' => 'brinda acceso al módulo de usuarios', 'creator_user' => 'configuraciones iniciales']);
        Module::create(['name' => 'roles', 'description' => 'brinda acceso al módulo de roles', 'creator_user' => 'configuraciones iniciales']);
        Module::create(['name' => 'clientes', 'description' => 'brinda acceso al módulo de clientes', 'creator_user' => 'configuraciones iniciales']);
        Module::create(['name' => 'grupos', 'description' => 'brinda acceso al módulo de grupos', 'creator_user' => 'configuraciones iniciales']);
        Module::create(['name' => 'opciones', 'description' => 'brinda acceso al módulo de opciones', 'creator_user' => 'configuraciones iniciales']);
        Module::create(['name' => 'camiones', 'description' => 'brinda acceso al módulo de camiones', 'creator_user' => 'configuraciones iniciales']);
        Module::create(['name' => 'contenedores', 'description' => 'brinda acceso al módulo de contenedores', 'creator_user' => 'configuraciones iniciales']);
        Module::create(['name' => 'video grabadores', 'description' => 'brinda acceso al módulo de drv', 'creator_user' => 'configuraciones iniciales']);
        Module::create(['name' => 'camaras', 'description' => 'brinda acceso al módulo de camaras', 'creator_user' => 'configuraciones iniciales']);
        Module::create(['name' => 'mini servidores', 'description' => 'brinda acceso al módulo de mini servidores', 'creator_user' => 'configuraciones iniciales']);
        Module::create(['name' => 'antenas', 'description' => 'brinda acceso al módulo de antenas', 'creator_user' => 'configuraciones iniciales']);
        Module::create(['name' => 'operaciones', 'description' => 'brinda acceso al módulo de operaciones', 'creator_user' => 'configuraciones iniciales']);
    }
}

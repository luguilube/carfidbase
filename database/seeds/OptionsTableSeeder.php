<?php

use Illuminate\Database\Seeder;
use Carfid\Models\Option;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Option::create(['title' => 'compañias', 'url' => 'companies.index', 'position' => 1, 'icon' => '1.ico', 'creator_user' => 'configuraciones iniciales', 'user_id' => 1]);
        Option::create(['title' => 'usuarios', 'url' => 'users.index', 'position' => 2, 'icon' => '2.ico', 'creator_user' => 'configuraciones iniciales', 'user_id' => 1]);
        Option::create(['title' => 'roles', 'url' => 'roles.index', 'position' => 3, 'icon' => '3.png', 'creator_user' => 'configuraciones iniciales', 'user_id' => 1]);
        Option::create(['title' => 'clientes', 'url' => 'clients.index', 'position' => 4, 'icon' => '4.ico', 'creator_user' => 'configuraciones iniciales', 'user_id' => 1]);
        Option::create(['title' => 'grupos', 'url' => 'groups.index', 'position' => 5, 'icon' => '5.ico', 'creator_user' => 'configuraciones iniciales', 'user_id' => 1]);
        Option::create(['title' => 'opciones', 'url' => 'options.index', 'position' => 6, 'icon' => '6.ico', 'creator_user' => 'configuraciones iniciales', 'user_id' => 1]);
        Option::create(['title' => 'vehiculos', 'url' => 'vehicles.index', 'position' => 7, 'icon' => '7.ico', 'creator_user' => 'configuraciones iniciales', 'user_id' => 1]);
        Option::create(['title' => 'contenedores', 'url' => 'containers.index', 'position' => 8, 'icon' => '8.ico', 'creator_user' => 'configuraciones iniciales', 'user_id' => 1]);
        Option::create(['title' => 'video grabadores', 'url' => 'video-records.index', 'position' => 9, 'icon' => null, 'creator_user' => 'configuraciones iniciales', 'user_id' => 1]);
        Option::create(['title' => 'camaras', 'url' => 'cameras.index', 'position' => 10, 'icon' => null, 'creator_user' => 'configuraciones iniciales', 'user_id' => 1]);
        Option::create(['title' => 'mini servidores', 'url' => 'mini-servers.index', 'position' => 11, 'icon' => null, 'creator_user' => 'configuraciones iniciales', 'user_id' => 1]);
        Option::create(['title' => 'antenas', 'url' => 'antennas.index', 'position' => 12, 'icon' => null, 'creator_user' => 'configuraciones iniciales', 'user_id' => 1]);
        // Option::create(['title' => 'compañias', 'url' => 'companies.index', 'position' => 1, 'icon' => '2.ico', 'creator_user' => 'configuraciones iniciales', 'user_id' => 1]);
    }
}

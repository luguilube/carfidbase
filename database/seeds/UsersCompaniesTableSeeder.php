<?php

use Illuminate\Database\Seeder;
use Carfid\Models\Company;
use Carfid\Models\UserCompany;

class UsersCompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = Company::all();

        foreach ($companies as $key => $value) {
            UserCompany::create([
                'user_id' => 1,
                'company_id' => $key + 1,
                'creator_user' => 'configuraciones iniciales'
            ]);
        }
    }
}

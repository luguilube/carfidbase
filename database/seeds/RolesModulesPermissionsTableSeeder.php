<?php

use Illuminate\Database\Seeder;
use Carfid\Models\Module;
use Carfid\Models\Permission;
use Carfid\Models\RoleModulePermission;

class RolesModulesPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = Module::all();
        $permissions = Permission::all();

        foreach ($modules as $module) {
            foreach ($permissions as $permission) {
                RoleModulePermission::create([
                    'role_id' => 2,
                    'module_id' => $module->id,
                    'permission_id' => $permission->id,
                    'creator_user' => 'configuraciones iniciales'
                ]);
            }
        }
    }
}

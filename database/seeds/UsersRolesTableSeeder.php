<?php

use Illuminate\Database\Seeder;
use Carfid\Models\UserRole;

class UsersRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserRole::create(['user_id' => 1, 'role_id' => 2, 'creator_user' => 'configuraciones iniciales']);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',45)->unique();
            $table->string('name',100)->unique();
            $table->string('phone_number',15)->nullable();
            $table->string('address')->nullable();
            $table->string('responsable',45)->nullable();
            $table->string('email')->nullable();
            $table->integer('limit_user')->nullable();
            $table->string('ruc',45)->nullable();
            $table->string('favicon',100)->nullable();
            $table->string('hostname',45)->nullable();
            $table->string('system_vehicle_name',45)->nullable();
            $table->string('system_container_name',45)->nullable();
            $table->string('creator_user',45);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}

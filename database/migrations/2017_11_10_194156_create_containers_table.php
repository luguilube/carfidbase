<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('containers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tag',105)->nullable()->unique();
            $table->string('plate_number',45)->unique();
            $table->float('height_container');
            $table->float('width_container');
            $table->float('long_container');
            $table->float('total_container');
            $table->float('height_extension')->nullable();
            $table->float('width_extension')->nullable();
            $table->float('long_extension')->nullable();
            $table->float('total_extension');
            $table->float('height_piston')->nullable();
            $table->float('width_piston')->nullable();
            $table->float('long_piston')->nullable();
            $table->float('total_piston');
            $table->float('yards');
            $table->float('full_payment')->nullable();
            $table->string('creator_user',45);
            $table->unsignedInteger('vehicle_id');
            $table->timestamps();

            $table->foreign('vehicle_id')->references('id')->on('vehicles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('containers');
    }
}

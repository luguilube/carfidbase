<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nick_name',45)->unique();
            $table->ipAddress('ip_address');
            $table->string('port',45);
            $table->string('user_name',45);
            $table->string('password');
            $table->string('creator_user',45);
            $table->unsignedInteger('company_id');
            $table->timestamps();


            $table->foreign('company_id')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_records');
    }
}

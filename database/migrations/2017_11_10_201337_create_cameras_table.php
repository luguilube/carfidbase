<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCamerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cameras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',45)->nullable();
            $table->string('description')->nullable();
            $table->boolean('enable');
            $table->integer('position');
            $table->string('creator_user',45);
            $table->unsignedInteger('video_record_id');
            $table->timestamps();

            $table->foreign('video_record_id')->references('id')->on('video_records');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cameras');
    }
}

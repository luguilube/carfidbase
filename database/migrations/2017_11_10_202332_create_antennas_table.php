<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAntennasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antennas', function (Blueprint $table) {
            $table->increments('id');
            $table->ipAddress('ip_address')->nullable();
            $table->string('port',45);
            $table->string('power',45);
            $table->string('description')->nullable();
            $table->string('creator_user',45);
            $table->unsignedInteger('mini_server_id');
            $table->timestamps();

            $table->foreign('mini_server_id')->references('id')->on('mini_servers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('antennas');
    }
}

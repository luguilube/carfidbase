<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMiniServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mini_servers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fixed_ip',45);
            $table->string('user_name',45);
            $table->string('password');
            $table->string('description')->unique();
            $table->string('port',45);
            $table->string('creator_user',45);
            $table->unsignedInteger('company_id');
            $table->timestamps();


            $table->foreign('company_id')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mini_servers');
    }
}

<?php

use Faker\Generator as Faker;
use Carfid\Models\Company;

$factory->define(Company::class, function (Faker $faker) {

    return [
        'code' => $faker->unique()->regexify('[A-Z0-9]{8}'),
        'name' => $faker->company(),
        'phone_number' => $faker->e164PhoneNumber(),
        'address' => $faker->address,
        'responsable' => $faker->name(),
        'email' => $faker->email(),
        'limit_user' => $faker->randomNumber(),
        'ruc' => $faker->randomNumber(),
        'favicon' => null,
        'hostname' => $faker->company(),
        'creator_user' => $faker->email()
    ];
});

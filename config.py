# Archivo de configuracion

# Configuraciond e la Base de Datos
DB_HOST = 'localhost'
DB_NAME = 'commData'
DB_UNAME = 'commData'
DB_PASS = 'd4t4f4c1l0n!'

# Ruta de la aplicacion
APP_DIR = '/home/arenera/commCore/'
LIB_DIR = APP_DIR + 'libs/'

# Configuracion del log
LOG_DIR = APP_DIR + 'logs/'
LOG_LEVEL = 20

# Ruta de los archivos de datos generados
DATA_ANT_FILE = '/vertedero/antena.txt'
DATA_BAL_FILE = '/vertedero/balanza.txt'

# Configuracion de las antenas RFID
READERS = [
  {}]

# Intervalo entre lectura y lectura RFID
SLEEP_RFID_TIME =1

# Intervalo entre lectura y lectura Balanza
# sleep: cuando no se ha detectado presencia
# active: cuando hay un vehiculo en la balanza
SLEEP_BAL_TIME = 5
ACTIVE_BAL_TIME = .2

# Configuracion de puerto Balanza
SERIAL_PORT_NAME = "/dev/ttyUSB0"
SERIAL_PORT_BAUD = 1200

# Limite minimo de peso para considerar que hay un vehiculo
MIN_WEIGHT_VALUE = 500

# Numero de segundos para considerar que el proceso
# balanza/antena esta muerto porque los registros del
# archivo son muy antiguos
SEC_TOO_OLD = 60

# Tamano de lectura minima para considerar un epc valido
MIN_EPC_SIZE = 7

# Cuando se calcula el histograma de pesos, son los valores de
# las X mayores ocurrencias las que se toman en cuenta para
# obtener el valor valido del peso. Se toma el mayor peso de
# esos X valores.
HISTO_VALID_VALUES = 5

# Es la cantidad de segundos que pasan luego de que la medida de peso
# ya no es valida o que ya no hay ningun tag detectado por la antena
# con la que se considera ya cerrada la transaccion actual, si durante
# ese tiempo vuelve a haber actividad, se toma como parte de la transaccion
# actual
TIME_TO_END_TRANSACTION = 2

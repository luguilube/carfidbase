<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('auth')->group(function(){
    Route::get('/select', 'SelectController@index')->name('select.index');
    Route::post('/select', 'SelectController@selected')->name('select.selected');
    Route::resource('/roles', 'RoleController');
    Route::resource('/companies', 'CompanyController');
    Route::resource('/users', 'UserController');
    Route::resource('/options', 'OptionController');
    Route::resource('/clients', 'ClientController');
    Route::resource('/groups', 'GroupController');
    Route::resource('/vehicles', 'VehicleController');
    Route::resource('/containers', 'ContainerController');
    Route::resource('/video-records', 'VideoRecordController');
    Route::resource('/cameras', 'CameraController');
    Route::resource('/mini-servers', 'MiniServerController');
    Route::resource('/antennas', 'AntennaController');
    Route::post('/image-management', 'FunctionController@imageModal')->name('images.modal');
    Route::post('/image-delete/{id}', 'FunctionController@imagesDestroy')->name('images.delete');

//  Route::get('/company', 'CompanydtController@index');
//  Route::get('/company', 'CompanydtController@getcompany')->name('datatable.company');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

<?php

namespace Carfid\Http\Controllers;

use Illuminate\Http\Request;
use Carfid\Http\Controllers\FunctionController;
use Carfid\Models\Client;
use Carfid\Models\Vehicle;
use Carfid\Models\GroupClient;
use Carfid\Http\Requests\Clients\CreateRequest;
use Carfid\Http\Requests\Clients\UpdateRequest;
use Log;
use Auth;
use DB;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!FunctionController::roleValidate(4, 1)) {
            FunctionController::accessDeniedMessage('ClientController');

            return redirect()->route('home');
        }

        $clients = Client::where('company_id', session('company')->id)
        ->get();

        return view('clients.index')->with(['clients' => $clients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!FunctionController::roleValidate(4, 2)) {
            FunctionController::accessDeniedMessage('ClientController');

            return redirect()->route('home');
        }

        return view('clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if (!FunctionController::roleValidate(4, 2)) {
            FunctionController::accessDeniedMessage('ClientController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();
            $faker = \Faker\Factory::create();

            $request->request->add(['code' => $faker->unique()->regexify('[A-Z0-9]{8}')]);
            $request->request->add(['creator_user' => Auth::user()->email]);
            $request->request->add(['company_id' => session('company')->id]);

            Client::create($request->all());

            DB::commit();

            FunctionController::successMessage($request->name, 'ClientController');

        } catch (\Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'ClientController');

            return back();
        }

        return redirect()->route('clients.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!FunctionController::roleValidate(4, 3)) {
            FunctionController::accessDeniedMessage('ClientController');

            return redirect()->route('home');
        }

        $client = Client::FindOrFail($id);

        return view('clients.show')->with(['client' => $client]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!FunctionController::roleValidate(4, 4)) {
            FunctionController::accessDeniedMessage('ClientController');

            return redirect()->route('home');
        }

        $client = Client::FindOrFail($id);

        return view('clients.edit')->with(['client' => $client]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        if (!FunctionController::roleValidate(4, 4)) {
            FunctionController::accessDeniedMessage('ClientController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $client = Client::FindOrFail($id)->update($request->all());

            DB::commit();

            FunctionController::successMessage($request->name, 'ClientController');
        } catch (\Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'ClientController');

            return back();
        }

        return redirect()->route('clients.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!FunctionController::roleValidate(4, 5)) {
            FunctionController::accessDeniedMessage('ClientController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $vehicle =  Client::FindOrFail($id)->vehicle;

            if (empty($vehicle[0]->id)) {
                GroupClient::where('client_id', $id)->delete();

                Client::destroy($id);

                DB::commit();

                FunctionController::successMessage($id, 'ClientController');

                return redirect()->route('clients.index');
            }else {
                DB::rollBack();
                FunctionController::noticeMessage($id,'ClientController');
            }

        } catch (Exception $e) {
            FunctionController::errorMessage($e, 'ClientController');
        }

        return redirect()->route('clients.show', $id);
    }
}

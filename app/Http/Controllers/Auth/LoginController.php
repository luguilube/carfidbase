<?php

namespace Carfid\Http\Controllers\Auth;

use Carfid\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Carfid\Models\UserRole;
use Carfid\Models\Company;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';
    protected function redirectTo()
    {
        $roles = UserRole::select('role_module_permission.*')
        ->join('role_module_permission', 'role_module_permission.role_id', 'user_role.role_id')
        ->where('user_id', Auth::user()->id)
        ->get()->toArray();

        session(['roles' => $roles]);

        $companies = Company::first();

        if (!session('company') && !empty($companies->id)) {
            return route('select.index');
        }else {
            return route('companies.index');
        }

        return '/home';
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate()
    {
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }
    }
}

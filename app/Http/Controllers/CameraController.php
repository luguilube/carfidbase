<?php

namespace Carfid\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use Auth;
use DB;
use Carfid\Models\{Camera, VideoRecord};
use Carfid\Http\Requests\Cameras\CreateRequest;
use Carfid\Http\Requests\Cameras\UpdateRequest;

class CameraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!FunctionController::roleValidate(10, 1)) {
            FunctionController::accessDeniedMessage('CameraController');

            return redirect()->route('home');
        }

        $cameras = Camera::camera(session('company')->id)->get();
        // http://mearenera.zippyttech.com/vertedero/camara.php?n=1

        foreach ($cameras as $camera) {

            $image = file_get_contents("http://admin:arenera123@mearenera.zippyttech.com:8081/PSIA/Streaming/channels/".$camera->id."02/picture?videoResolutionWidth=1920&videoResolutionHeight=1080");
            $encode = base64_encode($image);

            $image = imagecreatefromstring($image);
            $image_p = imagecreatetruecolor(300, 200);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, 300, 200, 1920, 1080);
            ob_start();
            imagepng($image_p);
            $data = ob_get_contents();
            ob_end_clean();
            $encode2 = base64_encode($data);

            $camera['image'] = $encode;
            $camera['thumb_image'] = $encode2;

        }

        return view('cameras.index')->with(['cameras' => $cameras]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!FunctionController::roleValidate(10, 2)) {
            FunctionController::accessDeniedMessage('CameraController');

            return redirect()->route('home');
        }

        $video_records = VideoRecord::videoRecord(session('company')->id)->get();
        $total_positions = Camera::camera(session('company')->id)->get()->count() + 1;

        return view('cameras.create')->with(['video_records' => $video_records, 'total_positions' => $total_positions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if (!FunctionController::roleValidate(10, 2)) {
            FunctionController::accessDeniedMessage('CameraController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            // $faker = \Faker\Factory::create();

            // $request->request->add(['code' => $faker->unique()->regexify('[A-Z0-9]{8}')]);
            $request->request->add(['creator_user' => Auth::user()->email]);
            $request->request->add(['enable' => false]);

            $camera = Camera::where('position', $request->position)->first();

            if (!empty($camera->id)) {
                $camera->position = $request->total_positions;
                $camera->save();
            }

            Camera::create($request->all());

            DB::commit();

            FunctionController::successMessage("Camara: ".$request->code, 'CameraController');

        } catch (\Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'CameraController');

            return back();
        }

        return redirect()->route('cameras.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!FunctionController::roleValidate(10, 3)) {
            FunctionController::accessDeniedMessage('CameraController');

            return redirect()->route('home');
        }

        $camera = Camera::FindOrFail($id);

        $image = file_get_contents("http://admin:arenera123@mearenera.zippyttech.com:8081/PSIA/Streaming/channels/".$camera->id."02/picture?videoResolutionWidth=1920&videoResolutionHeight=1080");

        $encode = base64_encode($image);

        $image = imagecreatefromstring($image);
        $image_p = imagecreatetruecolor(300, 200);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, 300, 200, 1920, 1080);
        ob_start();
        imagepng($image_p);
        $data = ob_get_contents();
        ob_end_clean();
        $encode2 = base64_encode($data);

        $camera['image'] = $encode;
        $camera['thumb_image'] = $encode2;

        return view('cameras.show')->with(['camera' => $camera]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!FunctionController::roleValidate(10, 4)) {
            FunctionController::accessDeniedMessage('CameraController');

            return redirect()->route('home');
        }

        $camera = Camera::FindOrFail($id);
        $video_records = VideoRecord::videoRecord(session('company')->id)->get();
        $total_positions = Camera::camera(session('company')->id)->get()->count();

        return view('cameras.edit')->with([
            'camera' => $camera,
            'video_records' => $video_records,
            'total_positions' => $total_positions
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        if (!FunctionController::roleValidate(10, 4)) {
            FunctionController::accessDeniedMessage('CameraController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $camera = Camera::where('position', $request->position)->first();
            $update_camera = Camera::FindOrFail($id);

            if (!empty($camera->id)) {
                $camera->position = $update_camera->position;
                $camera->save();
            }

            Camera::FindOrFail($id)->update($request->all());

            DB::commit();

            FunctionController::successMessage("Camara: ".$request->code, 'CameraController');

        } catch (\Exception $e) {
            DB::rollBack();

            FunctioNController::errorMessage($e, 'CameraController');

            return back();
        }

        return redirect()->route('cameras.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!FunctionController::roleValidate(10, 5)) {
            FunctionController::accessDeniedMessage('CameraController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            Camera::destroy($id);

            DB::commit();

            FunctionController::successMessage($id, 'CameraController');

        } catch (Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'CameraController');
        }

        return redirect()->route('cameras.index');
    }
}

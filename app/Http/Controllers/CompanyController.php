<?php

namespace Carfid\Http\Controllers;

use Illuminate\Http\Request;
use Carfid\Models\Company;
use Carfid\Models\Menu;
use Carfid\User;
use Carfid\Models\UserCompany;
use Carfid\Models\UserRole;
use Carfid\Models\Option;
use Carfid\Http\Requests\Companies\CreateRequest;
use Carfid\Http\Requests\Companies\UpdateRequest;
use Carfid\Http\Controllers\FunctionController;
use Log;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Collection;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!FunctionController::roleValidate(1, 1)) {
            FunctionController::accessDeniedMessage('CompanyController');

            return redirect()->route('home');
        }

        $companies = Company::all();

        return view('companies.index')->with(['companies' => $companies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!FunctionController::roleValidate(1, 2)) {
            FunctionController::accessDeniedMessage('CompanyController');

            return redirect()->route('home');
        }

        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if (!FunctionController::roleValidate(1, 2)) {
            FunctionController::accessDeniedMessage('CompanyController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $faker = \Faker\Factory::create();

            $request->request->add(['code' => $faker->unique()->regexify('[A-Z0-9]{8}')]);
            $request->request->add(['creator_user' => Auth::user()->email]);

            if (!empty($request -> file('image'))) {
                $request->request->add(['favicon' => $this->imageManagement($request -> file('image'), null)]);
            }

            $company = Company::create($request->all());

            $user = User::create([
                'name' => 'Admin De ' . ucwords($request->name),
                'email' => 'dev@' . strtolower(preg_replace('([^A-Za-z0-9])', '',  $request->name)) .'.com' ,
                'password' => bcrypt('admin1234'),
                'phone_number' => null,
                'creator_user' => Auth::user()->email
            ]);

            UserCompany::create(['user_id' => 1, 'company_id' => $company->id, 'creator_user' => Auth::user()->email]);
            UserCompany::create(['user_id' => $user->id, 'company_id' => $company->id, 'creator_user' => Auth::user()->email]);
            UserRole::create(['user_id' => $user->id, 'role_id' => 1, 'creator_user' => Auth::user()->email]);

            DB::commit();

            FunctionController::successMessage($request->name, 'CompanyController');

        } catch (\Exception $e) {
            DB::rollBack();

            FunctioNController::errorMessage($e, 'CompanyController');

            return back();
        }

        return redirect()->route('companies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!FunctionController::roleValidate(1, 3)) {
            FunctionController::accessDeniedMessage('CompanyController');

            return redirect()->route('home');
        }

        $company = Company::FindOrFail($id);

        return view('companies.show')->with(['company' => $company]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!FunctionController::roleValidate(1, 4)) {
            FunctionController::accessDeniedMessage('CompanyController');

            return redirect()->route('home');
        }

        $company = Company::FindOrFail($id);

        return view('companies.edit')->with(['company' => $company]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        if (!FunctionController::roleValidate(1, 4)) {
            FunctionController::accessDeniedMessage('CompanyController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $company = Company::FindOrFail($id);

            if (!empty($request -> file('image'))) {
                $request->request->add(['favicon' => $this->imageManagement($request -> file('image'), $company->favicon)]);
            }

            $company->update($request->all());

            DB::commit();

            FunctionController::successMessage($request->name, 'CompanyController');

        } catch (\Exception $e) {
            DB::rollBack();

            FunctioNController::errorMessage($e, 'CompanyController');

            return back();
        }

        return redirect()->route('companies.index', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!FunctionController::roleValidate(1, 5)) {
            FunctionController::accessDeniedMessage('CompanyController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $users_companies = Company::FindOrFail($id)->userCompany;
            $clients = Company::FindOrFail($id)->client;
            $video_records = Company::FindOrFail($id)->videoRecord;
            $mini_servers = Company::FindOrFail($id)->miniServer;

            if (empty($users_companies[0]->id)
            && empty($clients[0]->id)
            && empty($video_records[0]->id)
            && empty($mini_servers[0]->id)) {


                $users_companies = UserCompany::where('company_id', $id)->get()->toArray();
                $users = User::whereIn('id', array_column($users_companies, 'user_id'))->where('id', '>', 1)->get()->Toarray();
                Option::whereIn('user_id', array_column($users, 'id'))->where('user_id', '>', 1)->delete();

                UserRole::whereIn('user_id', array_column($users_companies, 'user_id'))->where('user_id', '>', 1)->delete();
                UserCompany::whereIn('user_id', array_column($users_companies, 'user_id'))->delete();
                User::whereIn('id', array_column($users, 'id'))->where('id', '>',  1)->delete();

                Company::destroy($id);

                DB::commit();

                FunctionController::successMessage($id, 'CompanyController');

            }else {
                FunctionController::noticeMessage($id,'CompanyController');
            }

        } catch (Exception $e) {
            DB::rollBack();
            FunctioNController::errorMessage($e, 'CompanyController');
        }

        return redirect()->route('companies.index');
    }

    public function imageManagement($file, $image)
    {
        //Recibimos el archivo enviado mediante el formulario
        // $file = $request -> file('image'); // Asignamos el archivo a una valiable
        $type_file = $file -> getClientOriginalExtension(); //Obtenemos la extención del archivo
        $file_name = time() . mt_rand() . ".$type_file"; //Asignamos un nuevo nombre al archivo

        if (Storage::disk('companies') -> has($image)) {
            Storage::disk('companies') -> delete($image);
        }

        $file -> move('images/companies', $file_name );

        return $file_name;
    }
}

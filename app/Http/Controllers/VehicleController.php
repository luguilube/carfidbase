<?php

namespace Carfid\Http\Controllers;

use Illuminate\Http\Request;
use Carfid\Models\Vehicle;
use Carfid\Models\Media;
use Carfid\Models\Client;
use Carfid\Models\Company;
use Carfid\Models\Container;
use Carfid\Http\Requests\Vehicles\CreateRequest;
use Carfid\Http\Requests\Vehicles\UpdateRequest;
use Carfid\Http\Controllers\FunctionController;
use Log;
use Auth;
use DB;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!FunctionController::roleValidate(8, 1)) {
            FunctionController::accessDeniedMessage('VehicleController');

            return redirect()->route('home');
        }

        $vehicles = Vehicle::vehicle(session('company')->id)->get();

        return view('vehicles.index')->with(['vehicles' => $vehicles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!FunctionController::roleValidate(7, 2)) {
            FunctionController::accessDeniedMessage('VehicleController');

            return redirect()->route('home');
        }

        $clients = Client::client(session('company')->id)->get();

        return view('vehicles.create')->with(['clients' => $clients]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if (!FunctionController::roleValidate(7, 2)) {
            FunctionController::accessDeniedMessage('VehicleController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $request->request->add(['creator_user' => Auth::user()->email]);

            $vehicle = Vehicle::create($request->all());

            $result = FunctionController::imageStore('vehicles', $vehicle->id, $request -> file('image'));

            DB::commit();

            FunctionController::successMessage($request->name, 'VehicleController');

        } catch (\Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'VehicleController');

            return back();
        }

        return redirect()->route('vehicles.show', $vehicle->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!FunctionController::roleValidate(7, 3)) {
            FunctionController::accessDeniedMessage('VehicleController');

            return redirect()->route('home');
        }

        $vehicle = Vehicle::FindOrFail($id);

        $images = Media::where(['table' => 'vehicles', 'identificator' => $id])->get();

        return view('vehicles.show')->with(['vehicle' => $vehicle, 'images' => $images]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!FunctionController::roleValidate(7, 4)) {
            FunctionController::accessDeniedMessage('VehicleController');

            return redirect()->route('home');
        }

        $vehicle = Vehicle::FindOrFail($id);
        $clients = Client::client(session('company')->id)->get();

        return view('vehicles.edit')->with(['vehicle' => $vehicle, 'clients' => $clients]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        if (!FunctionController::roleValidate(7, 4)) {
            FunctionController::accessDeniedMessage('VehicleController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            Vehicle::FindOrFail($id)->update($request->all());

            DB::commit();

            FunctionController::successMessage($request->plate_number, 'VehicleController');
        } catch (\Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'VehicleController');

            return back();
        }

        return redirect()->route('vehicles.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!FunctionController::roleValidate(7, 5)) {
            FunctionController::accessDeniedMessage('VehicleController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $container = Container::where('vehicle_id', $id)->first();
            if (empty($container->id)) {
                $vehicle = Vehicle::FindOrFail($id);

                Container::destroy($id);

                Vehicle::destroy($id);

                DB::commit();

                FunctionController::successMessage($id, 'VehicleController');
            }else {
                FunctionController::noticeMessage($id,'VehicleController');
            }

        } catch (Exception $e) {
            FunctionController::errorMessage($e, 'VehicleController');

            return back();
        }

        return redirect()->route('clients.show', $vehicle->client_id);
    }
}

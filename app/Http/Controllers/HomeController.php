<?php

namespace Carfid\Http\Controllers;

use Illuminate\Http\Request;
use Carfid\Models\{Company, Option};
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Auth::logout();
        $options = null;

        if (session('company')) {
           $options = Option::option(session('company')->id, Auth::user()->id)
           ->orderBy('position','ASC')
           ->get();

           if (empty($options[0]->id)) {
               $options = Option::orderBy('position','ASC')->get();
           }
       }elseif (!session('company') && Company::all()->count() == 1) {
          session(['company' => Company::first()]);

          $options = Option::option(session('company')->id, Auth::user()->id)
          ->orderBy('position','ASC')
          ->get();
      }elseif (!session('company') && Company::all()->count() == 0) {
          return redirect()->route('companies.create');
      }

        return view('home')->with(['options' => $options]);
    }
}

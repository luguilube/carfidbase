<?php

namespace Carfid\Http\Controllers;

use Illuminate\Http\Request;
use Carfid\Models\Vehicle;
use Carfid\Models\Company;
use Carfid\Models\Container;
use Carfid\Http\Requests\Containers\CreateRequest;
use Carfid\Http\Requests\Containers\UpdateRequest;
use Log;
use Auth;
use DB;

class ContainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!FunctionController::roleValidate(8, 1)) {
            FunctionController::accessDeniedMessage('ContainerController');

            return redirect()->route('home');
        }

        $containers = Container::container(session('company')->id)->get();

        return view('containers.index')->with(['containers' => $containers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!FunctionController::roleValidate(8, 2)) {
            FunctionController::accessDeniedMessage('ContainerController');

            return redirect()->route('home');
        }

        $vehicles = Vehicle::vehicle(session('company')->id)->get();

        return view('containers.create')->with(['vehicles' => $vehicles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if (!FunctionController::roleValidate(8, 2)) {
            FunctionController::accessDeniedMessage('ContainerController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $container = explode('/', $request->vehicle);

            $request->request->add(['creator_user' => Auth::user()->email]);
            $request->request->add(['vehicle_id' => $request->vehicle_id]);
            $request->request->add(['plate_number' => $request->plate_number]);

            $container = Container::create($request->all());

            FunctionController::imageStore('containers', $container->id, $request -> file('image'));

            DB::commit();

            FunctionController::successMessage($request->name, 'ContainerController');

        } catch (\Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'ContainerController');

            return back();
        }

        return redirect()->route('containers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!FunctionController::roleValidate(8, 3)) {
            FunctionController::accessDeniedMessage('ContainerController');

            return redirect()->route('home');
        }

        $container = Container::FindOrFail($id);

        return view('containers.show')->with(['container' => $container]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!FunctionController::roleValidate(8, 4)) {
            FunctionController::accessDeniedMessage('ContainerController');

            return redirect()->route('home');
        }

        $container = Container::FindOrFail($id);
        $vehicles = Vehicle::vehicle(session('company')->id)->get();

        return view('containers.edit')->with(['container' => $container, 'vehicles' => $vehicles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        if (!FunctionController::roleValidate(8, 4)) {
            FunctionController::accessDeniedMessage('ContainerController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $container = explode('/', $request->vehicle);
            
            $request->request->add(['creator_user' => Auth::user()->email]);
            $request->request->add(['vehicle_id' => $request->vehicle_id]);
            $request->request->add(['plate_number' => $request->plate_number]);

            Container::FindOrFail($id)->update($request->all());

            DB::commit();

            FunctionController::successMessage($request->plate_number, 'ContainerController');
        } catch (\Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'ContainerController');

            return back();
        }

        return redirect()->route('containers.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!FunctionController::roleValidate(8, 5)) {
            FunctionController::accessDeniedMessage('ContainerController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            Container::destroy($id);

            DB::commit();

            FunctionController::successMessage($id, 'ContainerController');

        } catch (Exception $e) {
            FunctionController::errorMessage($e, 'ContainerController');

            return back();
        }

        return redirect()->route('containers.index');
    }
}

<?php

namespace Carfid\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use Auth;
use DB;
use Carfid\Http\Requests\Antennas\CreateRequest;
use Carfid\Http\Requests\Antennas\UpdateRequest;
use Carfid\Models\Antenna;
use Carfid\Models\MiniServer;

class AntennaController extends Controller
{
    private $model;
    private $transport;
    private $addr;
    private $timeout;
    private $scantime;
    private $workmode;
    private $tries;
    private $enabled;

    public function __construct()
    {
        $this->model = '5112';
        $this->transport = 'SOCKET';
        $this->addr = '0';
        $this->timeout = '3';
        $this->scantime = '255';
        $this->workmode = '[0,
                            2,
                            4,
                            0,
                            1,
                            0]';
        $this->tries = '10';
        $this->enabled = '1';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!FunctionController::roleValidate(12, 1)) {
            FunctionController::accessDeniedMessage('AntennaController');

            return redirect()->route('home');
        }
 
        $antennas = Antenna::antenna(session('company')->id)->get();

        return view('antennas.index')->with(['antennas' => $antennas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!FunctionController::roleValidate(12, 2)) {
            FunctionController::accessDeniedMessage('AntennaController');

            return redirect()->route('home');
        }

        $mini_servers = MiniServer::miniServer(session('company')->id)->get();

        return view('antennas.create')->with(['mini_servers' => $mini_servers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if (!FunctionController::roleValidate(12, 2)) {
            FunctionController::accessDeniedMessage('AntennaController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $request->request->add(['creator_user' => Auth::user()->email]);

            $antenna = Antenna::create($request->all());

            $this->updateFile($request, $antenna->id);

            DB::commit();

            FunctionController::successMessage("Camara: ".$request->code, 'AntennaController');

        } catch (\Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'AntennaController');

            return back();
        }

        return redirect()->route('antennas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!FunctionController::roleValidate(12, 3)) {
            FunctionController::accessDeniedMessage('AntennaController');

            return redirect()->route('home');
        }

        $antenna = Antenna::FindOrFail($id);

        return view('antennas.show')->with(['antenna' => $antenna]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!FunctionController::roleValidate(12, 4)) {
            FunctionController::accessDeniedMessage('AntennaController');

            return redirect()->route('home');
        }

        $antenna = Antenna::FindOrFail($id);
        $mini_servers = MiniServer::miniServer(session('company')->id)->get();

        return view('antennas.edit')->with([
            'antenna' => $antenna,
            'mini_servers' => $mini_servers,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        if (!FunctionController::roleValidate(12, 4)) {
            FunctionController::accessDeniedMessage('AntennaController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $antenna = Antenna::FindOrFail($id);

            $this->updateDataOfFile($request, $id, $antenna);

            $antenna->update($request->all());

            DB::commit();

            FunctionController::successMessage($request->code, 'AntennaController');

        } catch (\Exception $e) {
            DB::rollBack();

            FunctioNController::errorMessage($e, 'AntennaController');

            return back();
        }

        return redirect()->route('antennas.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!FunctionController::roleValidate(12, 5)) {
            FunctionController::accessDeniedMessage('AntennaController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            Antenna::destroy($id);

            DB::commit();

            FunctionController::successMessage($id, 'AntennaController');

        } catch (Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'AntennaController');
        }

        return redirect()->route('antennas.index');
    }

    public function updateFile($request, $id)
    {
        $result = false;
        try {
            $file = File('../config.py');
            foreach ($file as $key => $value) {
                if (strpos($value, '}]') !== FALSE) {
                        $file[$key] = str_replace('}]', '},
                        {
                            \'id\': \''.$id.'\',
                            \'model\': \''.$this->model.'\',
                            \'transport\': \''.$this->transport.'\',
                            \'addr\': '.$this->addr.',
                            \'ip\': \''.$request->ip_address.'\',
                            \'port\': '.$request->port.',
                            \'timeout\': '.$this->timeout.',
                            \'power\': '.$request->power.',
                            \'scantime\': '.$this->scantime.',
                            \'workmode\': '.$this->workmode.',
                            \'tries\': '.$this->tries.',
                            \'enabled\': '.$this->enabled.' }]', $value);
                }
            }
            file_put_contents('../config.py',$file);

            $result = true;
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error al intentar actualizar el archivo config.py por el usuario: '. Auth::user()->email);
        }

        return $result;
    }

    public function updateDataOfFile($request, $id, $antenna)
    {
        $result = false;
        $searched = false;
        try {
            $file = File('../config.py');
            foreach ($file as $key => $value) {
                if (strpos($value, '\'id\': \''.$id.'\'') !== FALSE) {
                    $searched = true;
                }elseif ($searched) {
                    if (strpos($value, '\'ip\': \''.$antenna->ip_address.'\',') !== FALSE) {
                        $file[$key] = str_replace(
                            '\'ip\': \''.$antenna->ip_address.'\',',
                            '\'ip\': \''.$request->ip_address.'\',',
                            $value
                        );
                    }elseif (strpos($value, '\'port\': '.$antenna->port.',') !== FALSE) {
                        $file[$key] = str_replace(
                            '\'port\': '.$antenna->port.',',
                            '\'port\': '.$request->port.',',
                            $value
                        );
                    }elseif (strpos($value, '\'power\': '.$antenna->power.',') !== FALSE) {
                        $file[$key] = str_replace(
                            '\'power\': '.$antenna->power.',',
                            '\'power\': '.$request->power.',',
                            $value
                        );

                        $searched = false;
                        break;
                    }
                }
            }
            file_put_contents('../config.py',$file);

            $result = true;
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error al intentar actualizar el archivo config.py por el usuario: '. Auth::user()->email. "[$e]");
        }

        return $result;
    }
}

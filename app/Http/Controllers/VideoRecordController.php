<?php

namespace Carfid\Http\Controllers;

use Illuminate\Http\Request;
use Carfid\Models\VideoRecord;
use Carfid\Http\Requests\VideoRecords\CreateRequest;
use Carfid\Http\Requests\VideoRecords\UpdateRequest;
use Log;
use Auth;
use DB;

class VideoRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!FunctionController::roleValidate(9, 1)) {
            FunctionController::accessDeniedMessage('VideoRecordController');

            return redirect()->route('home');
        }

        $video_records = VideoRecord::videoRecord(session('company')->id)->get();

        return view('video_records.index')->with(['video_records' => $video_records]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!FunctionController::roleValidate(9, 2)) {
            FunctionController::accessDeniedMessage('VideoRecordController');

            return redirect()->route('home');
        }

        return view('video_records.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if (!FunctionController::roleValidate(9, 2)) {
            FunctionController::accessDeniedMessage('VideoRecordController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $request->request->add(['creator_user' => Auth::user()->email]);
            $request->request->add(['company_id' => session('company')->id]);

            VideoRecord::create($request->all());

            DB::commit();

            FunctionController::successMessage($request->nick_name, 'VideoRecordController');

        } catch (\Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'VideoRecordController');

            return back();
        }

        return redirect()->route('video-records.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!FunctionController::roleValidate(9, 3)) {
            FunctionController::accessDeniedMessage('VideoRecordController');

            return redirect()->route('home');
        }

        $video_record = VideoRecord::FindOrFail($id);

        return view('video_records.show')->with(['video_record' => $video_record]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!FunctionController::roleValidate(9, 4)) {
            FunctionController::accessDeniedMessage('VideoRecordController');

            return redirect()->route('home');
        }

        $video_record = VideoRecord::FindOrFail($id);

        return view('video_records.edit')->with(['video_record' => $video_record]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        if (!FunctionController::roleValidate(9, 4)) {
            FunctionController::accessDeniedMessage('VideoRecordController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            VideoRecord::FindOrFail($id)->update($request->all());

            DB::commit();

            FunctionController::successMessage($request->name, 'VideoRecordController');

        } catch (\Exception $e) {
            DB::rollBack();

            FunctioNController::errorMessage($e, 'VideoRecordController');

            return back();
        }

        return redirect()->route('video-records.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!FunctionController::roleValidate(9, 5)) {
            FunctionController::accessDeniedMessage('VideoRecordController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $cameras = VideoRecord::FindOrFail($id)->camera;

            if (empty($cameras[0]->id)) {

                VideoRecord::destroy($id);

                DB::commit();

                FunctionController::successMessage($id, 'VideoRecordController');

                return redirect()->route('video-records.index');
            }else {
                FunctionController::noticeMessage($id,'VideoRecordController');
            }

        } catch (Exception $e) {
            DB::rollBack();
            FunctioNController::errorMessage($e, 'VideoRecordController');
        }

        return redirect()->route('video-records.index');
    }
}

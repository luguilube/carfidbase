<?php

namespace Carfid\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use Auth;
use DB;
use Carfid\Http\Requests\MiniServers\CreateRequest;
use Carfid\Http\Requests\MiniServers\UpdateRequest;
use Carfid\Models\MiniServer;

class MiniServerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!FunctionController::roleValidate(11, 1)) {
            FunctionController::accessDeniedMessage('MiniServerController');

            return redirect()->route('home');
        }

        $mini_servers = MiniServer::miniServer(session('company')->id)->get();

        return view('mini_servers.index')->with(['mini_servers' => $mini_servers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!FunctionController::roleValidate(11, 2)) {
            FunctionController::accessDeniedMessage('MiniServerController');

            return redirect()->route('home');
        }

        return view('mini_servers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if (!FunctionController::roleValidate(11, 2)) {
            FunctionController::accessDeniedMessage('MiniServerController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $request->request->add(['creator_user' => Auth::user()->email]);
            $request->request->add(['company_id' => session('company')->id]);

            MiniServer::create($request->all());

            DB::commit();

            FunctionController::successMessage($request->user_name, 'MiniServerController');

        } catch (\Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'MiniServerController');

            return back();
        }

        return redirect()->route('mini-servers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!FunctionController::roleValidate(11, 3)) {
            FunctionController::accessDeniedMessage('MiniServerController');

            return redirect()->route('home');
        }

        $mini_server = MiniServer::FindOrFail($id);

        return view('mini_servers.show')->with(['mini_server' => $mini_server]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!FunctionController::roleValidate(11, 4)) {
            FunctionController::accessDeniedMessage('MiniServerController');

            return redirect()->route('home');
        }

        $mini_server = MiniServer::FindOrFail($id);

        return view('mini_servers.edit')->with(['mini_server' => $mini_server]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        if (!FunctionController::roleValidate(11, 4)) {
            FunctionController::accessDeniedMessage('MiniServerController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            MiniServer::FindOrFail($id)->update($request->all());

            DB::commit();

            FunctionController::successMessage($request->name, 'MiniServerController');

        } catch (\Exception $e) {
            DB::rollBack();

            FunctioNController::errorMessage($e, 'MiniServerController');

            return back();
        }

        return redirect()->route('mini-servers.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!FunctionController::roleValidate(11, 5)) {
            FunctionController::accessDeniedMessage('MiniServerController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $antennas = MiniServer::FindOrFail($id)->antenna;

            if (empty($antennas[0]->id)) {

                MiniServer::destroy($id);

                DB::commit();

                FunctionController::successMessage($id, 'MiniServerController');

                return redirect()->route('mini-servers.index');
            }else {
                FunctionController::noticeMessage($id,'MiniServerController');
            }

        } catch (Exception $e) {
            DB::rollBack();
            FunctioNController::errorMessage($e, 'MiniServerController');
        }

        return redirect()->route('mini-servers.index');
    }
}

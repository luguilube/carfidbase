<?php

namespace Carfid\Http\Controllers;

use Illuminate\Http\Request;
use Carfid\Models\Media;
use Carfid\Models\UserCompany;
use Carfid\User;
use Log;
use Auth;
use DB;
use Storage;

class FunctionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public static function successMessage($name, $controller)
    {
        flash('¡Se realizo la operación satisfactoriamente!')->success();
        Log::info($controller. ' -> Operación exitosa: '. $name . ' por: '. Auth::user()->name . ' email de usuario: ' . Auth::user()->email);
    }

    public static function errorMessage($e, $controller)
    {
        flash('¡Ha ocurrido un error al intentar realizar la operación!')->error();
        Log::error($controller. ' -> Se ha detectado el error: ['. $e . '] Al intentar realizar la operación');
    }

    public static function noticeMessage($id, $controller)
    {
        flash('¡No es posible eliminar el registro porque existen otros registros asociados a él!')->warning();
        Log::notice($controller. ' -> No es posible eliminar el registro: '. $id . ' por: '. Auth::user()->name . ' nombre de usuario: ' . Auth::user()->email);
    }

    public static function accessDeniedMessage($controller)
    {
        flash('¡Acceso denegado! no tiene permitido el ingreso a este módulo')->error();
        Log::error('Acceso denegado al módulo: ' . $controller . ' para el usuario: '. Auth::user()->id);
    }

    public static function roleValidate($module, $permission)
    {
        $session_roles = session('roles');

        if (in_array($module, array_column($session_roles, 'module_id'))) {
            foreach ($session_roles as $key => $value) {
                if ($value['module_id'] == $module && $value['permission_id'] == $permission) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function imageStore($disk, $identificator, $files)
    {
        $result = false;
        $type_file = null;
        try {
            DB::beginTransaction();

            if ($files != null) {
                $path = "images/$disk";

                foreach ($files as $key => $file) {
                    $type_file = $file -> getClientOriginalExtension();
                    $file_name = time() . mt_rand() . ".$type_file";

                    Media::create([
                        'image' => $file_name,
                        'table' => $disk,
                        'identificator' => $identificator,
                        'creator_user' => Auth::user()->email
                    ]);

                    $file -> move($path, $file_name );
                }
            }else {
                Media::create([
                    'image' => $type_file,
                    'table' => $disk,
                    'identificator' => $identificator,
                    'creator_user' => Auth::user()->email
                ]);
            }

            DB::commit();
            $result = true;
        } catch (\Exception $e) {
            DB::rollBack();
        }

        return $result;
    }

    public static function imageModal(Request $request)
    {
        $disk = $request->disk;
        $identificator = $request->identificator;
        $id = $request->id;
        $file = $request-> file('image');
        $result = false;
        $type_file = null;

        try {
            DB::beginTransaction();

            $path = "images/$disk";
            $type_file = $file -> getClientOriginalExtension();
            $file_name = time() . mt_rand() . ".$type_file";

            if (!empty($id)) {
                $media = Media::FindOrFail($id);

                if (!empty($media->id)) {
                    if (Storage::disk($disk) -> has($media->image)) {
                        Storage::disk($disk) -> delete($media->image);
                    }
                }

                $media->image = $file_name;
                $media->save();
            }else {
                Media::create([
                    'image' => $file_name,
                    'table' => $disk,
                    'identificator' => $identificator,
                    'creator_user' => Auth::user()->email
                ]);
            }

            $file -> move($path, $file_name );

            DB::commit();
            $result = true;
        } catch (\Exception $e) {
            DB::rollBack();
        }

        return back();
    }

    public static function imagesDestroy(Request $request)
    {
        $id = $request->id;
        $disk = $request->disk;
        
        try {
            DB::beginTransaction();

            $media = Media::FindOrFail($id);

            if (!empty($media->id)) {
                if (Storage::disk($disk) -> has($media->image)) {
                    Storage::disk($disk) -> delete($media->image);
                }
            }

            Media::destroy($id);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }

        return back();
    }

    public static function cleanString($string)
    {
         return preg_replace('([^A-Za-z0-9.])', '', $string);
    }
}

<?php

namespace Carfid\Http\Controllers;

use Illuminate\Http\Request;
use Carfid\User;
use Carfid\Models\{UserCompany, Company , UserRole, Role};
use Carfid\Http\Controllers\FunctionController;
use Carfid\Http\Requests\Users\CreateRequest;
use Carfid\Http\Requests\Users\UpdateRequest;
use Log;
use Auth;
use DB;
use Carfid\Models\Option;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!FunctionController::roleValidate(2, 1)) {
            FunctionController::accessDeniedMessage('RoleController');

            return redirect()->route('home');
        }

        $users = null;

        if (session('company')) {
            $users = User::users(session('company')->id)->get();
        }

        $companies = Company::all();
        $roles = Role::all();    

        return view('users.index', compact('users', 'companies', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!FunctionController::roleValidate(2, 2)) {
            FunctionController::accessDeniedMessage('RoleController');

            return redirect()->route('home');
        }

        $companies = Company::all();
        $roles = Role::all();

        return view('users.create')->with(['companies' => $companies, 'roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if (!FunctionController::roleValidate(2, 2)) {
            FunctionController::accessDeniedMessage('RoleController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $request->request->add(['creator_user' => Auth::user()->email]);
            $request->request->add(['password' => bcrypt('1234')]);

            $user = User::create($request->all());

            UserRole::create([
                'user_id' => $user->id,
                'role_id' => 1,
                'creator_user' => Auth::user()->email
            ]);

            if (!empty($request->companies)) {
                UserCompany::where('user_id', $user->id)->delete();

                $this->assignCompanies($request->companies, $user->id);
            }

            DB::commit();

            FunctionController::successMessage($request->name, 'UserController');
        } catch (\Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'UserController');

            return back();
        }

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!FunctionController::roleValidate(2, 3)) {
            FunctionController::accessDeniedMessage('RoleController');

            return redirect()->route('home');
        }

        $user = User::FindOrFail($id);

        return view('users.show')->with(['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!FunctionController::roleValidate(2, 4)) {
            FunctionController::accessDeniedMessage('RoleController');

            return redirect()->route('home');
        }

        $user = User::FindOrFail($id);
        $roles = Role::all();
        $companies = Company::all();

        return view('users.edit')->with([
            'user' => $user, 'roles' => $roles, 'companies' => $companies]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        if (!FunctionController::roleValidate(2, 4)) {
            FunctionController::accessDeniedMessage('RoleController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            User::FindOrFail($id)->update($request->all());

            if (!empty($request->roles)) {
                UserRole::where('user_id', $id)->delete();

                $this->assignRoles($request->roles, $id);
            }

            if (!empty($request->companies)) {
                UserCompany::where('user_id', $id)->delete();

                $this->assignCompanies($request->companies, $id);
            }

            DB::commit();

            FunctionController::successMessage($request->name, 'UserController');
        } catch (\Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'UserController');

            return back();
        }

        return redirect()->route('users.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!FunctionController::roleValidate(2, 5)) {
            FunctionController::accessDeniedMessage('RoleController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            if ($id != 1) {
                UserCompany::where('user_id', $id)->delete();
                UserRole::where('user_id', $id)->delete();
                Option::where('user_id', $id)->delete();

                User::destroy($id);

                DB::commit();

                FunctionController::successMessage($id, 'UserController');
            }else {
                flash('¡No es posible eliminar a este usuario en particular')->warning();
                Log::notice('UserController'. ' -> No es posible eliminar el registro: '. $id . ' por: '. Auth::user()->name . ' nombre de usuario: ' . Auth::user()->email);
            }
        } catch (Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'UserController');
        }

        return redirect()->route('users.index');
    }

    public function assignRoles($roles, $id)
    {
        foreach ($roles as $role) {
            UserRole::create([
                'user_id' => $id,
                'role_id' => $role,
                'creator_user' => Auth::user()->email
            ]);
        }
    }

    public function assignCompanies($companies, $id)
    {
        foreach ($companies as $company) {
            UserCompany::create([
                'user_id' => $id,
                'company_id' => $company,
                'creator_user' => Auth::user()->email
            ]);
        }
    }
}

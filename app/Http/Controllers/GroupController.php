<?php

namespace Carfid\Http\Controllers;

use Illuminate\Http\Request;
use Carfid\Http\Controllers\FunctionController;
use Carfid\Models\Group;
use Carfid\Models\Client;
use Carfid\Models\GroupClient;
use Carfid\Models\UserCompany;
use Carfid\Http\Requests\Groups\CreateRequest;
use Carfid\Http\Requests\Groups\UpdateRequest;
use Log;
use Auth;
use DB;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!FunctionController::roleValidate(5, 1)) {
            FunctionController::accessDeniedMessage('GroupController');

            return redirect()->route('home');
        }

        $groups = Group::groups(session('company')->id)->get();

        return view('groups.index')->with(['groups' => $groups]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!FunctionController::roleValidate(5, 2)) {
            FunctionController::accessDeniedMessage('GroupController');

            return redirect()->route('home');
        }

        $clients = Client::where('company_id', session('company')->id)
        ->get();

        return view('groups.create')->with('clients', $clients);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if (!FunctionController::roleValidate(5, 2)) {
            FunctionController::accessDeniedMessage('GroupController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $request->request->add(['creator_user' => Auth::user()->email]);

            $group = Group::create($request->all());

            if (!empty($request->clients)) {
                $this->clientRegister($request->clients, $group->id);
            }

            DB::commit();

            FunctionController::successMessage($request->name, 'GroupController');
        } catch (\Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'GroupController');

            return back();
        }

        return redirect()->route('groups.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!FunctionController::roleValidate(5, 3)) {
            FunctionController::accessDeniedMessage('GroupController');

            return redirect()->route('home');
        }

        $group = Group::FindOrFail($id);
        $clients = Group::FindOrFail($id)->groupClient;

        return view('groups.show')->with(['group' => $group, 'clients' => $clients]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!FunctionController::roleValidate(5, 4)) {
            FunctionController::accessDeniedMessage('GroupController');

            return redirect()->route('home');
        }

        $group = Group::FindOrFail($id);
        $clients = Client::where('company_id', session('company')->id)
        ->get();

        return view('groups.edit')->with(['group' => $group, 'clients' => $clients]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        if (!FunctionController::roleValidate(5, 4)) {
            FunctionController::accessDeniedMessage('GroupController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            Group::FindOrFail($id)->update($request->all());

            if (!empty($request->clients)) {
                GroupClient::where('group_id', $id)->delete();

                $this->clientRegister($request->clients, $id);
            }

            DB::commit();

            FunctionController::successMessage($request->name, 'GroupController');
        } catch (\Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'GroupController');

            return back();
        }

        return redirect()->route('groups.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!FunctionController::roleValidate(5, 5)) {
            FunctionController::accessDeniedMessage('GroupController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $clients =  Group::FindOrFail($id)->groupClient;

            if (empty($clients->id)) {
                Group::destroy($id);

                DB::commit();

                FunctionController::successMessage($id, 'GroupController');
            }else {
                FunctionController::noticeMessage($id,'GroupController');
            }

        } catch (Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'GroupController');
        }

        return redirect()->route('groups.show', $id);
    }

    public function clientRegister($clients, $id)
    {
        foreach ($clients as $client) {
            GroupClient::create([
                'group_id' => $id,
                'client_id' => $client,
                'creator_user' => Auth::user()->email
            ]);
        }
    }
}

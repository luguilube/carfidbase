<?php

namespace Carfid\Http\Controllers;

use Illuminate\Http\Request;
use Carfid\Models\Role;
use Carfid\Models\Module;
use Carfid\Models\UserRole;
use Carfid\Models\Permission;
use Carfid\Models\RoleModulePermission;
use Carfid\Http\Requests\Roles\CreateRequest;
use Carfid\Http\Requests\Roles\UpdateRequest;
use Log;
use Auth;
use DB;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!FunctionController::roleValidate(3, 1)) {
            FunctionController::accessDeniedMessage('RoleController');

            return redirect()->route('home');
        }

        $roles = Role::all();

        return view('roles.index')->with(['roles' => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!FunctionController::roleValidate(3, 2)) {
            FunctionController::accessDeniedMessage('RoleController');

            return redirect()->route('home');
        }

        $modules = Module::all();
        $permissions = Permission::all();

        return view('roles.create')->with(['modules' => $modules, 'permissions' => $permissions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if (!FunctionController::roleValidate(3, 2)) {
            FunctionController::accessDeniedMessage('RoleController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $request->request->add(['creator_user' => Auth::user()->email]);

            $role = Role::create($request->all());

            $result = null;
            foreach ($request->privileges as $privilege) {
                $result = explode('/', $privilege);
                RoleModulePermission::create([
                    'role_id' => $role->id,
                    'module_id' => $result[0],
                    'permission_id' => $result[1],
                    'creator_user' => Auth::user()->email
                ]);
            }

            DB::commit();

            FunctionController::successMessage($request->name, 'RoleController');

        } catch (\Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'RoleController');

            return back();
        }

        return redirect()->route('roles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!FunctionController::roleValidate(3, 3)) {
            FunctionController::accessDeniedMessage('RoleController');

            return redirect()->route('home');
        }

        $role = Role::FindOrFail($id);
        $modules = Module::all();
        $permissions = Permission::all();
        $rma = RoleModulePermission::where('role_id', $id)
        ->get()
        ->toArray();

        return view('roles.show')->with(['modules' => $modules, 'permissions' => $permissions, 'role' => $role, 'rma' => $rma]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!FunctionController::roleValidate(3, 4)) {
            FunctionController::accessDeniedMessage('RoleController');

            return redirect()->route('home');
        }

        $role = Role::FindOrFail($id);
        $modules = Module::all();
        $permissions = Permission::all();
        $rma = RoleModulePermission::where('role_id', $id)
        ->get()
        ->toArray();

        return view('roles.edit')->with(['modules' => $modules, 'permissions' => $permissions, 'role' => $role, 'rma' => $rma]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        if (!FunctionController::roleValidate(3, 4)) {
            FunctionController::accessDeniedMessage('RoleController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            Role::FindOrFail($id)
                ->update($request->all());

            $rma = RoleModulePermission::where('role_id', $id)->delete();

            if (!empty($request->privileges)) {
                session()->forget('roles');


                $result = null;
                foreach ($request->privileges as $key => $privilege) {
                    $result = explode('/', $privilege);

                    RoleModulePermission::create([
                       'role_id' => $id,
                       'module_id' => $result[0],
                       'permission_id' => $result[1],
                       'creator_user' => Auth::user()->email
                   ]);
                }

                $roles = UserRole::select('role_module_permission.*')
                ->join('role_module_permission', 'role_module_permission.role_id', 'user_role.role_id')
                ->where('user_id', Auth::user()->id)
                ->get()->toArray();

                session(['roles' => $roles]);
            }


            DB::commit();

            FunctionController::successMessage($request->name, 'RoleController');
        } catch (\Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'RoleController');

            return back();
        }

        return redirect()->route('roles.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!FunctionController::roleValidate(3, 5)) {
            FunctionController::accessDeniedMessage('RoleController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            if ($id != 1) {
                UserRole::where('role_id', $id)->delete();
                RoleModulePermission::where('role_id', $id)->delete();
                Role::destroy($id);

                DB::commit();

                FunctionController::successMessage($id, 'RoleController');

                return redirect()->route('roles.index');
            }else {
                DB::rollBack();
                FunctionController::noticeMessage($id,'RoleController');
            }

        } catch (Exception $e) {
            FunctionController::errorMessage($e, 'RoleController');
        }

        return redirect()->route('roles.show', $id);
    }
}

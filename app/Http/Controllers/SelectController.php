<?php

namespace Carfid\Http\Controllers;

use Illuminate\Http\Request;
use Carfid\Models\Company;
use Auth;

class SelectController extends Controller
{
    public function index()
    {
        $companies = Company::companies(Auth::user()->id)->get();

        return view('selection')->with(['companies' => $companies]);
    }

    public function selected(Request $request)
    {
        $company = Company::FindOrFail($request->company);
        session(['company' => $company]);
        
        return redirect()->route('home');
    }
}

<?php

namespace Carfid\Http\Controllers;

use Illuminate\Http\Request;
use Carfid\Http\Controllers\FunctionController;
use Carfid\Models\Option;
use Carfid\Http\Requests\Options\CreateRequest;
use Carfid\Http\Requests\Options\UpdateRequest;
use Log;
use Auth;
use DB;
use Storage;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!FunctionController::roleValidate(6, 1)) {
            FunctionController::accessDeniedMessage('OptionController');

            return redirect()->route('home');
        }

        $options = Option::option(session('company')->id, Auth::user()->id)
        ->orderBy('position')
        ->get();

        return view('options.index')->with(['options' => $options]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!FunctionController::roleValidate(6, 2)) {
            FunctionController::accessDeniedMessage('OptionController');

            return redirect()->route('home');
        }

        $total_positions = Option::option(session('company')->id, Auth::user()->id)
        ->get()
        ->count() + 1;

        return view('options.create')->with(['total_positions' => $total_positions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if (!FunctionController::roleValidate(6, 2)) {
            FunctionController::accessDeniedMessage('OptionController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $request->request->add(['creator_user' => Auth::user()->email]);
            $request->request->add(['user_id' => Auth::user()->id]);
            $request->merge(['url' => FunctionController::cleanString($request->url)]);
            
            $option_update = Option::storeOption(session('company')->id, Auth::user()->id, $request->position)->first();

            if (!empty($option_update->id)) {
                $option_update = Option::storeOption(session('company')->id, Auth::user()->id, $request->position)
                ->get();

                foreach ($option_update as $key => $value) {
                    $value->position = $value->position + 1 ;
                    $value->save();
                }
            }

            if (!empty($request -> file('image'))) {
                $request->request->add(['icon' => $this->imageManagement($request -> file('image'), null)]);
            }

            Option::create($request->all());

            DB::commit();

            FunctionController::successMessage($request->name, 'OptionController');
        } catch (\Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'OptionController');

            return back();
        }

        return redirect()->route('options.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!FunctionController::roleValidate(6, 3)) {
            FunctionController::accessDeniedMessage('OptionController');

            return redirect()->route('home');
        }

        $option = Option::FindOrFail($id);

        return view('options.show')->with(['option' => $option]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!FunctionController::roleValidate(6, 4)) {
            FunctionController::accessDeniedMessage('OptionController');

            return redirect()->route('home');
        }

        $option = Option::FindOrFail($id);

        $total_positions = Option::option(session('company')->id, Auth::user()->id)
        ->get()
        ->count();

        return view('options.edit')->with(['option' => $option, 'total_positions' => $total_positions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        if (!FunctionController::roleValidate(6, 4)) {
            FunctionController::accessDeniedMessage('OptionController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $option = Option::FindOrFail($id);

            if ($option->position < $request->position) {
                $option_update = Option::updateOption(session('company')->id, Auth::user()->id, $request->position, $option->position)
                ->get();
                foreach ($option_update as $key => $value) {
                    $value->position = $value->position - 1 ;
                    $value->save();
                }

            }else {
                $option_update = Option::storeOption(session('company')->id, Auth::user()->id, $request->position)
                ->get();
                foreach ($option_update as $key => $value) {
                    $value->position = $value->position + 1 ;
                    $value->save();
                }
            }

            if (!empty($request -> file('image'))) {
                $request->request->add(['icon' => $this->imageManagement($request -> file('image'), null)]);
            }

            $request->merge(['url' => FunctionController::cleanString($request->url)]);
            $option->update($request->all());

            DB::commit();

            FunctionController::successMessage($request->name, 'OptionController');
        } catch (\Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'OptionController');

            return back();
        }

        return redirect()->route('options.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!FunctionController::roleValidate(6, 5)) {
            FunctionController::accessDeniedMessage('OptionController');

            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            Option::destroy($id);

            DB::commit();

            FunctionController::successMessage($id, 'OptionController');

        } catch (Exception $e) {
            DB::rollBack();
            FunctionController::errorMessage($e, 'OptionController');
        }

        return redirect()->route('options.index');
    }

    public function imageManagement($file, $image)
    {
        //Recibimos el archivo enviado mediante el formulario
        // $file = $request -> file('image'); // Asignamos el archivo a una valiable
        $type_file = $file -> getClientOriginalExtension(); //Obtenemos la extención del archivo
        $file_name = time() . mt_rand() . ".$type_file"; //Asignamos un nuevo nombre al archivo

        if (Storage::disk('options') -> has($image)) {
            Storage::disk('options') -> delete($image);
        }

        $file -> move('images/options', $file_name );

        return $file_name;
    }
}

<?php

namespace Carfid\Http\Requests\Clients;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class UpdateRequest extends FormRequest
{
    /**
     * [__construct to get the route]
     * @param Route $route [The route of the petition]
     */
    public  function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'nullable|string|min:0|max:99999999|unique:clients,code,' . $this->route->parameter('client'),
            'name' => 'required|string|min:3|max:100|unique:clients,name,' . $this->route->parameter('client'),
            'phone_number' => 'nullable|string|min:7|max:15',
            'address' => 'nullable|string|min:10|max:250',
            'ruc' => 'nullable|string|min:7|max:45|unique:clients,ruc,' . $this->route->parameter('client')
        ];
    }
}

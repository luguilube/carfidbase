<?php

namespace Carfid\Http\Requests\Clients;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:100|unique:clients,name',
            'phone_number' => 'nullable|string|min:7|max:15',
            'address' => 'nullable|string|min:10|max:250',
            'ruc' => 'nullable|string|min:7|max:45|unique:clients,ruc'
        ];
    }
}

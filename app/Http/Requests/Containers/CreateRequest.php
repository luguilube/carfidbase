<?php

namespace Carfid\Http\Requests\Containers;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'vehicle_id' => 'required|integer|min:1|exists:vehicles,id',
            'plate_number' => 'required|string|min:3|max:45|unique:containers,plate_number',
            'tag' => 'nullable|string|min:1|max:105|unique:containers,tag',
            'yards' => 'nullable|numeric|min:1|max:99999999',
            'height_container' => 'required|numeric|min:1|max:999999',
            'width_container' => 'required|numeric|min:1|max:999999',
            'long_container' => 'required|numeric|min:1|max:999999',
            'height_piston' => 'nullable|numeric|min:1|max:999999',
            'width_piston' => 'nullable|numeric|min:1|max:999999',
            'long_piston' => 'nullable|numeric|min:1|max:999999',
            'height_extension' => 'nullable|numeric|min:1|max:999999',
            'width_extension' => 'nullable|numeric|min:1|max:999999',
            'long_extension' => 'nullable|numeric|min:1|max:999999',
            'full_payment' => 'nullable|numeric|min:0|max:99999999',
            'image' => 'nullable|array'
        ];

        if (!empty($this->files->get('image'))) {
            foreach($this->files->get('image') as $key => $val)
            {
                $rules['image.'.$key] = 'required|image|mimes:jpeg,jpg,png';
            }
        }

        return $rules;
    }
}

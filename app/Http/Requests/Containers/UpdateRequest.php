<?php

namespace Carfid\Http\Requests\Containers;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class UpdateRequest extends FormRequest
{
    /**
     * [__construct to get the route]
     * @param Route $route [The route of the petition]
     */
    public  function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vehicle_id' => 'required|integer|min:1|exists:vehicles,id',
            'plate_number' => 'required|string|min:3|max:45|unique:containers,plate_number,' . $this->route->parameter('container'),
            'tag' => 'nullable|string|min:1|max:105|unique:containers,tag,' . $this->route->parameter('container'),
            'yards' => 'nullable|numeric|min:1|max:99999999',
            'height_container' => 'required|numeric|min:1|max:999999',
            'width_container' => 'required|numeric|min:1|max:999999',
            'long_container' => 'required|numeric|min:1|max:999999',
            'height_piston' => 'nullable|numeric|min:1|max:999999',
            'width_piston' => 'nullable|numeric|min:1|max:999999',
            'long_piston' => 'nullable|numeric|min:1|max:999999',
            'height_extension' => 'nullable|numeric|min:1|max:999999',
            'width_extension' => 'nullable|numeric|min:1|max:999999',
            'long_extension' => 'nullable|numeric|min:1|max:999999',
            'full_payment' => 'nullable|numeric|min:0|max:99999999',
            'image' => 'nullable|image|mimes:jpeg,jpg,png'
        ];
    }
}

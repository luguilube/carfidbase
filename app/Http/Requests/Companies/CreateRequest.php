<?php

namespace Carfid\Http\Requests\Companies;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        return [
            'name' => 'required|string|unique:companies|min:3|max:100',
            'phone_number' => 'nullable|string|min:7|max:15',
            'address' => 'nullable|string|min:10|max:250',
            'responsable' => 'nullable|string|min:3|max:45',
            'email' => 'nullable|string|email|max:255',
            'limit_user' => 'nullable|integer|min:0|max:99999999',
            'ruc' => 'nullable|string|min:7|max:45',
            'image' => 'nullable|image|mimes:jpeg,jpg,png',
            'hostname' => 'nullable|string|unique:companies|min:3|max:100',
            'system_vehicle_name' => 'required|string|min:3|max:100',
            'system_container_name' => 'required|string|min:3|max:100',
        ];
    }
}

<?php

namespace Carfid\Http\Requests\Companies;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class UpdateRequest extends FormRequest
{
    /**
     * [__construct to get the route]
     * @param Route $route [The route of the petition]
     */
    public  function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|string|min:4|max:255|unique:companies,code,' . $this->route->parameter('company'),
            'name' => 'required|string|min:3|max:100|unique:companies,name,' . $this->route->parameter('company'),
            'phone_number' => 'nullable|string|min:7|max:15',
            'address' => 'nullable|string|min:10|max:250',
            'responsable' => 'nullable|string|min:3|max:45',
            'email' => 'nullable|string|email|max:255',
            'limit_user' => 'nullable|integer|min:0|max:99999999',
            'ruc' => 'nullable|string|min:7|max:45',
            'image' => 'nullable|image|mimes:jpeg,jpg,png',
            'hostname' => 'nullable|string|min:3|max:100|unique:companies,hostname,' . $this->route->parameter('company'),
            'system_vehicle_name' => 'required|string|min:3|max:100',
            'system_container_name' => 'required|string|min:3|max:100',
        ];
    }
}

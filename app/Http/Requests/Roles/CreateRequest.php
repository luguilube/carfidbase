<?php

namespace Carfid\Http\Requests\Roles;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:45|unique:roles,name',
            'description' => 'nullable|string|min:10|max:250',
            'privileges' => 'required|array'
        ];

    }

    public function messages()
    {
        return [
            'privileges.required' => 'El campo Privilegios es obligatorio.',
        ];
    }
}

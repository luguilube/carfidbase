<?php

namespace Carfid\Http\Requests\Roles;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class UpdateRequest extends FormRequest
{
    /**
     * [__construct to get the route]
     * @param Route $route [The route of the petition]
     */
    public  function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:45|unique:roles,name,' . $this->route->parameter('role'),
            'description' => 'nullable|string|min:10|max:250',
            'privileges' => 'required|array'
        ];

    }

    public function messages()
    {
        return [
            'privileges.required' => 'El campo Privilegios es obligatorio.',
        ];
    }
}

<?php

namespace Carfid\Http\Requests\Vehicles;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'plate_number' => 'required|string|min:3|max:45|unique:vehicles,plate_number',
            'image' => 'nullable|array',
            'tag' => 'nullable|string|min:3|max:105|unique:vehicles,tag'
        ];

        if (!empty($this->files->get('image'))) {
            foreach($this->files->get('image') as $key => $val)
            {
                $rules['image.'.$key] = 'required|image|mimes:jpeg,jpg,png';
            }
        }

        return $rules;
    }
}

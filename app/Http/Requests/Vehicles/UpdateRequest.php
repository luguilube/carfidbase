<?php

namespace Carfid\Http\Requests\Vehicles;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class UpdateRequest extends FormRequest
{
    /**
     * [__construct to get the route]
     * @param Route $route [The route of the petition]
     */
    public  function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'plate_number' => 'required|string|min:3|max:45|unique:vehicles,plate_number,' . $this->route->parameter('vehicle'),
            'tag' => 'nullable|string|min:3|max:105|unique:vehicles,tag,' . $this->route->parameter('vehicle')
        ];
    }
}

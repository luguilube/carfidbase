<?php

namespace Carfid\Http\Requests\Cameras;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'video_record_id' => 'required|integer|min:1|exists:video_records,id',
            'code' => 'required|integer|min:0',
            'description' => 'nullable|string|min:10|max:250',
            'position' => 'required|integer|min:1',
        ];
    }
}

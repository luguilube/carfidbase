<?php

namespace Carfid\Http\Requests\VideoRecords;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nick_name' => 'required|string|max:45|unique:video_records,nick_name',
            'ip_address' => 'required|ip|min:8',
            'port' => 'required|string|min:2',
            'user_name' => 'required|string|max:45',
            'password' => 'required|string|min:6|confirmed',
        ];
    }
}

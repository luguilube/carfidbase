<?php

namespace Carfid\Http\Requests\VideoRecords;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class UpdateRequest extends FormRequest
{
    /**
     * [__construct to get the route]
     * @param Route $route [The route of the petition]
     */
    public  function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nick_name' => 'required|string|max:45|unique:video_records,nick_name,' . $this->route->parameter('video_record'),
            'ip_address' => 'required|ip|min:8',
            'port' => 'required|string|min:2',
            'user_name' => 'required|string|max:45',
            'password' => 'required|string|min:6|confirmed',
        ];
    }
}

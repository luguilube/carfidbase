<?php

namespace Carfid\Http\Requests\Options;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:1|max:45',
            'url' => 'required|string|min:3|max:345',
            'position' => 'nullable|integer|min:1',
            'icon' => 'nullable|image|mimes:jpeg,jpg,png',
        ];
    }
}

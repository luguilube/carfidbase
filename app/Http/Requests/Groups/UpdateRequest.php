<?php

namespace Carfid\Http\Requests\Groups;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class UpdateRequest extends FormRequest
{
    /**
     * [__construct to get the route]
     * @param Route $route [The route of the petition]
     */
    public  function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|min:3|max:45|unique:groups,name,' . $this->route->parameter('group'),
            'description' => 'nullable|string|min:10|max:250',
            'clients' => 'nullable|array'
        ];

        if (!empty($this->files->get('clients'))) {
            foreach($this->files->get('clients') as $key => $val)
            {
                $rules['clients.'.$key] = 'required|integer|min:1|exists:clients,id';
            }
        }

        return $rules;
    }
}

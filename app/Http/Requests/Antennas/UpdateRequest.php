<?php

namespace Carfid\Http\Requests\Antennas;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ip_address' => 'required|ip|min:8',
            'port' => 'required|integer|min:0',
            'power' => 'required|string|min:1',
            'description' => 'nullable|string|min:10|max:250',
        ];
    }
}

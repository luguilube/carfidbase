<?php

namespace Carfid\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class UpdateRequest extends FormRequest
{

    /**
     * [__construct to get the route]
     * @param Route $route [The route of the petition]
     */
    public  function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,name, ' . $this->route->parameter('user'),
            'phone_number' => 'nullable|string|min:7|max:15',
            'roles' => 'nullable|array',
            'companies' => 'nullable|array'
        ];

        if (!empty($this->files->get('roles'))) {
            foreach($this->files->get('roles') as $key => $val)
            {
                $rules['roles.'.$key] = 'required|integer|min:1|exists:roles,id';
            }
        }

        if (!empty($this->files->get('companies'))) {
            foreach($this->files->get('companies') as $key => $val)
            {
                $rules['companies.'.$key] = 'required|integer|min:1|exists:companies,id';
            }
        }

        return $rules;
    }
}

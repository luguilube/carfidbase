<?php

namespace Carfid\Http\Requests\MiniServers;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fixed_ip' => 'required|ip|min:8',
            'user_name' => 'required|string|max:45',
            'port' => 'required|string|min:2',
            'description' => 'nullable|string|min:10|max:250',
            'password' => 'required|string|min:6|confirmed',
        ];
    }
}

<?php

namespace Carfid;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone_number', 'creator_user'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
         return $this->belongsToMany('Carfid\Models\Role', 'user_role');
    }

    public function company()
    {
         return $this->belongsToMany('Carfid\Models\Company', 'user_company');
    }

    public function option()
    {
         return $this->hasMany('Carfid\Models\Option');
    }

    public function scopeUsers($query, $company_id)
    {
        return $query->select('users.*')
        ->join('user_company', 'user_company.user_id', 'users.id')
        ->where(['user_company.company_id' => $company_id]);
    }
}

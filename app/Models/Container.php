<?php

namespace Carfid\Models;

use Illuminate\Database\Eloquent\Model;

class Container extends Model
{
    protected $table = 'containers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'plate_number', 'tag', 'height_container', 'width_container', 'long_container',
        'total_container', 'height_piston', 'width_piston', 'long_piston',
        'total_extension', 'height_extension', 'width_extension', 'long_extension',
        'total_piston', 'yards', 'full_payment', 'creator_user', 'vehicle_id'
    ];

    public function vehicle()
    {
         return $this->belongsTo('Carfid\Models\Vehicle');
    }

    public function scopeContainer($query, $company_id)
    {
        return $query->select('containers.*')
        ->join('vehicles', 'vehicles.id', 'containers.vehicle_id')
        ->join('clients', 'clients.id', 'vehicles.client_id')
        ->where(['clients.company_id' => $company_id]);
    }
}

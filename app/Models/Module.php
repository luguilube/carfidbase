<?php

namespace Carfid\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'modules';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'creator_user'
    ];

    public function roleModulePermission()
    {
         return $this->hasMany('Carfid\Models\RoleModulePermission');
    }

    public function user()
    {
         return $this->belongsToMany('Carfid\Models\User', 'user_role');
    }
}

<?php

namespace Carfid\Models;

use Illuminate\Database\Eloquent\Model;

class Camera extends Model
{
    protected $table = 'cameras';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'description', 'enable', 'position', 'creator_user', 'video_record_id'
    ];

    public function videoRecord()
    {
         return $this->belongsTo('Carfid\Models\VideoRecord');
    }

    public function scopeCamera($query, $company_id)
    {
        return $query->select('cameras.*')
        ->join('video_records', 'video_records.id', 'cameras.video_record_id')
        ->where('video_records.company_id', $company_id)
        ->orderBy('position', 'ASC');
    }
}

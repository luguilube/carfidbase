<?php

namespace Carfid\Models;

use Illuminate\Database\Eloquent\Model;

class GroupClient extends Model
{
    protected $table = 'group_client';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'creator_user', 'group_id', 'client_id'
    ];

}

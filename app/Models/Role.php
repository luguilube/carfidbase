<?php

namespace Carfid\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'creator_user'
    ];

    public function users()
    {
         return $this->belongsToMany('Carfid\User', 'user_role');
    }

    public function roleModulePermission()
    {
         return $this->hasMany('Carfid\Models\RoleModulePermission');
    }

}

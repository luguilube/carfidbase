<?php

namespace Carfid\Models;

use Illuminate\Database\Eloquent\Model;

class Antenna extends Model
{
    protected $table = 'antennas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ip_address', 'port', 'power', 'description', 'creator_user', 'mini_server_id'
    ];

    public function miniServers()
    {
         return $this->belongsTo('Carfid\Models\MiniServer', 'mini_server_id');
    }

    public function scopeAntenna($query, $company_id)
    {
        return $query->select('antennas.*')
        ->join('mini_servers', 'mini_servers.id', 'antennas.mini_server_id')
        ->where('mini_servers.company_id', $company_id);
    }
}

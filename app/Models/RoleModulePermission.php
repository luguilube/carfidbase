<?php

namespace Carfid\Models;

use Illuminate\Database\Eloquent\Model;

class RoleModulePermission extends Model
{
    protected $table = 'role_module_permission';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'creator_user', 'role_id',  'module_id', 'permission_id'
    ];
}

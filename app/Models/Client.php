<?php

namespace Carfid\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name', 'phone_number', 'address', 'ruc', 'creator_user',
        'company_id'
    ];

    public function group()
    {
         return $this->belongsToMany('Carfid\Models\Group', 'group_client');
    }

    public function vehicle()
    {
         return $this->hasMany('Carfid\Models\Vehicle');
    }

    public function company()
    {
        return $this->belongsTo('Carfid\Models\Company');
    }

    public function scopeClient($query, $company_id)
    {
        return $query->where('company_id', $company_id);
    }
}

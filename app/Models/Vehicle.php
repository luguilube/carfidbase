<?php

namespace Carfid\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $table = 'vehicles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'plate_number', 'tag', 'creator_user', 'client_id'
    ];

    public function client()
    {
         return $this->belongsTo('Carfid\Models\Client');
    }

    public function container()
    {
         return $this->hasMany('Carfid\Models\Container');
    }
    
    public function scopeVehicle($query, $company_id)
    {
        return $query->select('vehicles.*')
        ->join('clients', 'clients.id', 'vehicles.client_id')
        ->where('clients.company_id', $company_id);
    }
}

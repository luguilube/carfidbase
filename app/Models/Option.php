<?php

namespace Carfid\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $table = 'options';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'url', 'position', 'icon', 'creator_user', 'user_id'
    ];

    public function user()
    {
         return $this->belongsTo('Carfid\User');
    }

    public function scopeOption($query, $company_id, $user_id)
    {
        return $query->select('options.*')
        ->join('users', 'users.id', 'options.user_id')
        ->join('user_company', 'user_company.user_id', 'users.id')
        ->where(['user_company.company_id' => $company_id, 'user_company.user_id' => $user_id]);
    }

    public function scopeStoreOption($query, $company_id, $user_id, $position)
    {
        return $query->option($company_id, $user_id)
        ->where([
            'user_company.company_id' => $company_id,
            'user_company.user_id' => $user_id,
            ['options.position', '>=', $position]
        ]);
    }

    public function scopeUpdateOption($query, $company_id, $user_id, $position, $old_position)
    {
        return $query->option($company_id, $user_id)
        ->where([
            'user_company.company_id' => $company_id,
            'user_company.user_id' => $user_id,
            ['options.position', '<=', $position],
            ['options.position', '>', $old_position]
        ]);
    }
}

<?php

namespace Carfid\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'creator_user' 
    ];

    public function groupClient()
    {
         return $this->belongsToMany('Carfid\Models\Client', 'group_client');
    }

    public function scopeGroups($query, $company_id)
    {
        return $query->select('groups.*')
        ->join('group_client', 'group_client.group_id', 'groups.id')
        ->join('clients', 'clients.id', 'group_client.client_id')
        ->where('clients.company_id', $company_id);
    }
}

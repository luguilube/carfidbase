<?php

namespace Carfid\Models;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    protected $table = 'operations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vehicle_plate_number', 'antenna_code', 'client_name', 'container_plate_number',
        'camera_code'
    ];
}

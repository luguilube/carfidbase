<?php

namespace Carfid\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name', 'phone_number', 'address', 'responsable', 'responsable',
        'email', 'limit_user', 'ruc', 'favicon', 'hostname', 'system_vehicle_name',
        'system_container_name', 'creator_user'
    ];

    public function scopeCompanies($query, $user_id)
    {
        return $query->select('companies.*')
        ->join('user_company', 'user_company.company_id', 'companies.id')
        ->where('user_company.user_id', $user_id);
    }

    public function menu()
    {
         return $this->hasOne('Carfid\Models\Menu');
    }

    public function user()
    {
         return $this->belongsToMany('Carfid\Models\User', 'user_company');
    }

    public function client()
    {
        return $this->hasMany('Carfid\Models\Client');
    }

    public function videoRecord()
    {
        return $this->hasMany('Carfid\Models\VideoRecord');
    }

    public function miniServer()
    {
        return $this->hasMany('Carfid\Models\MiniServer');
    }
}

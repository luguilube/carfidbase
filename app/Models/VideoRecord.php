<?php

namespace Carfid\Models;

use Illuminate\Database\Eloquent\Model;

class VideoRecord extends Model
{
    protected $table = 'video_records';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nick_name', 'ip_address', 'port', 'user_name', 'password', 'creator_user', 'company_id'
    ];

    public function camera()
    {
         return $this->hasMany('Carfid\Models\Camera');
    }

    public function company()
    {
         return $this->belongsTo('Carfid\Models\Company');
    }

    public function scopeVideoRecord($query, $company_id)
    {
        return $query->where('company_id', $company_id);
    }
}

<?php

namespace Carfid\Models;

use Illuminate\Database\Eloquent\Model;

class MiniServer extends Model
{
    protected $table = 'mini_servers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fixed_ip', 'user_name', 'password', 'description', 'port', 'creator_user', 'company_id'
    ];

    public function antenna()
    {
         return $this->hasMany('Carfid\Models\Antenna');
    }

    public function company()
    {
         return $this->belongsTo('Carfid\Models\Company');
    }

    public function scopeMiniServer($query, $company_id)
    {
        return $query->where('company_id', $company_id);
    }
}

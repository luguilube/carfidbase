<?php

namespace Carfid\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name', 'description', 'creator_user'
    ];

    public function roleModulePermission()
    {
         return $this->hasMany('Carfid\Models\RoleModulePermission');
    }
}

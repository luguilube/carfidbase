@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                <div class="sign-in-form-top">
                    <span>Vehicles - Editar</span>
                    <div class="pull-right">                                     
                        <a href="{{ route('vehicles.show', $vehicle -> id)}}" class="btn btn-warning" id="atras">
                                    <i class="fa fa-reply" aria-hidden="true"></i> Atrás
                                </a>
                    </div>
                   </div>
                <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                    <form class="form-horizontal" method="POST" action="{{ route('vehicles.update', $vehicle -> id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group{{ $errors->has('client_id') ? ' has-error' : '' }}">
                            <label for="client_id" class="col-md-4 control-label">Cliente</label>

                            <div class="col-md-6">
                                <select id="client_id" name="client_id" class="form-control" required="required">
                                    <option value="">Seleccione un Cliente...</option>
                                    @foreach($clients as $client)
                                        <option value="{{ $client->id }}"
                                            @if ($client->id == $vehicle->client_id)
                                                selected="selected"
                                            @endif
                                            >{{ $client -> name }}
                                        </option>
                                    @endforeach
                                </select>

                                @if ($errors->has('client_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('client_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('plate_number') ? ' has-error' : '' }}">
                            <label for="plate_number" class="col-md-4 control-label">Número de placa</label>

                            <div class="col-md-6">
                                <input id="plate_number" type="text" class="form-control" name="plate_number" value="{{ $vehicle->plate_number }}" required autofocus>

                                @if ($errors->has('plate_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('plate_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('tag') ? ' has-error' : '' }}">
                            <label for="tag" class="col-md-4 control-label">Tag</label>

                            <div class="col-md-6">
                                <input id="tag" type="text" class="form-control" name="tag" value="{{ $vehicle->tag }}" required autofocus>

                                @if ($errors->has('tag'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tag') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{-- <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="image" class="col-md-4 control-label">Fotos: {{ $vehicle->client->company->system_vehicle_name }}</label>

                            <div class="col-md-6">
                                <input id="image" type="file" class="form-control" name="image[]" multiple>

                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> --}}

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Actualizar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    
@endsection

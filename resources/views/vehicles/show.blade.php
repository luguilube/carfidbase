@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
    <div class="graphs">
        <div class="sign-in-form">
            <div class="sign-in-form-top">
                  <span>Ver - {{$vehicle->client->company->system_vehicle_name}}</span>
                <div class="pull-right ">
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">
                        <i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar
                    </button>
                    <a href="{{ route('vehicles.index') }}" type="button" class="btn btn-warning">
                        <i class="fa fa-reply" aria-hidden="true"></i> Atrás
                    </a>
                    <a href="{{ route('vehicles.edit', $vehicle -> id) }}" type="button" class="btn btn-primary">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar
                    </a>
                </div>
            </div>
            <div class="signin">
                <div class="log-input">
					@include('flash::message')
					<div class="panel-body">
		                <ul class="panel-list" style="list-style:none">
		                     <li class="row">
		                     	<div class="col-md-4 col-sm-4">
		                            <p><b>Código</b></p>
		                            <p class="text-capitalize">{{ $vehicle -> id }}</p>
		                        </div>
		                        <div class="col-md-4 col-sm-4">
		                            <p><b># Placa</b></p>
		                            <p class="text-capitalize">{{ $vehicle -> plate_number }}</p>
		                        </div>
		                        <div class="col-md-4 col-sm-4">
		                            <p><b>TAG</b></p>
		                            <p class="text-capitalize">{{ $vehicle -> tag }}</p>
		                        </div>
		                    </li>
		                    <li class="row">
		                    	<div class="col-md-6 col-sm-6">
		                            <p><b>Código de cliente</b></p>
		                            <p class="text-capitalize">{{ $vehicle -> client_id }}</p>
		                        </div>
		                        <div class="col-md-6 col-sm-6">
		                            <p><b>Creación de usuario</b></p>
		                            <p class="text-capitalize">{{ $vehicle -> creator_user }}</p>
		                        </div>
		                    </li>
		                    <li class="row">
		                        <div class="col-xs-12 col-sm-6">
		                            <p><b>Creación</b></p>
		                            <p class="text-capitalize">{{ date_format($vehicle -> created_at, 'd/m/Y h:i:s A') }}</p>
		                        </div>
		                        <div class="col-xs-12 col-sm-6">
		                            <p><b>Modificación</b></p>
		                            <p class="text-capitalize">{{ date_format($vehicle -> updated_at, 'd/m/Y h:i:s A' ) }}</p>
		                        </div>
		                    </li>
		                </ul>

                		<div class="col-md-12">
                            <h5 class="text-capitalize"><b>Imagenes</b></h5>
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                @foreach ($images as $element)
                                    <div class="col-xs-6 col-sm-4 col-md-2">
                                        <div class="thumbnail relative">
                                            <div class="overlay">
                                                <div class="btns">
                                                    <button type="button" class="img-vehicle btn btn-success btn-xs" data-toggle="modal" data-target="#update-image"><i class="fa fa-pencil"></i><span class="data hidden">{{ $element -> id }}</span></button>
                                                    <button type="button" class="img-vehicle btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-image"><i class="fa fa-close"></i><span class="data hidden">{{ $element -> id }}</span></button>
                                                </div>
                                            </div>
                                            @if (empty($element-> image))
                                                <img class="image-thumbnail" src="{{ Storage::disk('default')->url('default.png') }}" alt="{{ $element -> nombre . ' ' . $element -> id}}">
                                            @else
                                                <img class="image-thumbnail" src="{{ Storage::disk('vehicles')->url($element-> image) }}" alt="{{ $element -> nombre . ' ' . $element -> id}}">
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                                @if (count($images)<4)
                                    <div class="col-xs-6 col-sm-4 col-md-2" >
                                        <div class="thumbnail">
                                            <a id="btn-add-image" data-toggle="modal" data-target="#update-image" style=" height: 140px; display: flex; align-items: center; text-align: center; cursor: pointer;">
                                                <i class="fa fa-plus" style="font-size: 50pt; display: block; margin: auto; color: #E1E1E1"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>
    	               </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('vehicles.destroy', $vehicle->id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Vehiculo</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de eliminar el Vehiculo <b>{{ $vehicle -> name }}</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- end modal -->

    <!-- MODAL PARA UPDATE UNA IMAGEN -->
    <div class="modal fade" id="update-image" tabindex="-1" role="dialog" aria-labelledby="add-image">
        <form action=" {{ route('images.modal', $vehicle -> id) }} " method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="change-image">Actualizar imagen</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="image" class="control-label">Imagen <span class="text-danger">*</span></label>
                            <input id="image" name="image" type="file" class="form-control" required="required">
                            <input class="id" name="id" type="hidden" readonly="readonly">
                            <input id="identificator" name="identificator" type="hidden" readonly="readonly" value="{{ $vehicle -> id }}">
                            <input id="disk" name="disk" type="hidden" value="vehicles" readonly="readonly">
                        </div>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- END MODAL PARA AGREGAR UNA IMAGEN -->

    <!-- MODAL PARA ELIMINAR UNA IMAGEN -->
    <div class="modal fade" id="delete-image" tabindex="-1" role="dialog" aria-labelledby="add-image">
        <form action=" {{ route('images.delete', $vehicle -> id) }} " method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="change-image">Eliminar imagen</h4>
                    </div>
                    <div class="modal-body">
                        <p class="">¿Esta seguro de eliminar la imagen?</p>
                        <input class="id" type="hidden" name="id" readonly="readonly">
                        <input id="disk" name="disk" type="hidden" value="vehicles" readonly="readonly">
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
						<button type="submit" class="btn btn-success">Si</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- END MODAL PARA ELIMINAR UNA IMAGEN -->
@stop
@section('scripts')
	<!-- page js tiene la funcionadad que me pasa el id de la imagen de dicho modal en show dde vehicles -->
	{{-- Mala implementación de los Jquery ya que debe funcionar declarado en el APP, Debe revisarce --}}
	<script type="text/javascript" src="{{ asset('assets/js/jquery-3.2.1.min.js') }}"></script>
    <script>
	    $(document).ready(function(){
		   // Obtiene el ID de la imagen y la inserta en el formulario para posteriormente enviarlo al metodo de cambio de image //
		   // alert('hola, funciona');
		    $('.img-vehicle').click(function(){
		    	value = $(this).children('.data').text();
		    	$('.id').val(value);
		 	});
	    });
    </script>
   <!-- page de js -->
@endsection

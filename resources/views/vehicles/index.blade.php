@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
    <div class="graphs">
        <div class="sign-in-form">
            <div class="sign-in-form-top">
                 <span class="title">Vehiculos</span>
                <div class="pull-right">
                    <a href="{{ route('home')}}" class="btn btn-md btn-warning" id="atras">
                        <i class="fa fa-reply" aria-hidden="true"></i></a> 
                    <a data-toggle="modal" data-target="#modal-add" type="button" class="btn btn-primary " title="Nuevo"> <i class="fa fa-plus-square-o" aria-hidden="true"></i></a>
                </div>
            </div>              
            <div class="signin">  
                <div class="log-input">
                    @include('flash::message')
                    <table id="companydata" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <th>ID</th>
                            <th># placa</th>
                            <th>TAG</th>
                            <th>Usuario Creador</th>
                            <th>Fecha Creación</th>
                            <th>Fecha Modificación</th>
                            <th>Cliente</th>
                            <th>Opciones</th>
                        </thead>
                        <tbody>
                            
                                @foreach ($vehicles as $key => $vehicle)
                                    <tr>
                                        <td>{{ $vehicle->id }}</td>
                                        <td>{{ $vehicle->plate_number }}</td>
                                        <td>{{ $vehicle->tag }}</td>
                                        <td>{{ $vehicle->creator_user }}</td>
                                        <td>{{ $vehicle->created_at }}</td>
                                        <td>{{ $vehicle->updated_at }}</td>
                                        <td>{{ $vehicle->client->name }}</td>
                                        <td>
                                            <button type="button" class="btn btn-primary " title="Modificar" data-toggle="modal" data-target="#modalEdit{{$key}}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>

                                                     <!-- Modal modificar registro -->
                                                     <div class="modal fade" id="modalEdit{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalEdit{{$key}}">
                                                         <form class="form-horizontal" action="{{ route('vehicles.update', $vehicle->id) }}" method="POST" enctype="multipart/form-data">

                                                    
                                                             {{ csrf_field() }}
                                                             <input type="hidden" name="_method" value="EDIT">
                                                             <div class="modal-dialog" role="document">
                                                                 <div class="modal-content">
                                                                     <div class="modal-header">
                                                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                        
                                                                         <h4 class="modal-title" id="myModalLabel"  style="color:#007bff">Editar Vehiculo</h4>
                                                                         
                                                                     </div>  
                                                                        
                                                                        <div class="modal-footer text-center">                             
                                                                         <input type="hidden" name="_method" value="PUT">
                                                                          {{-- <div class="{{ $errors->has('client_id') ? ' has-error' : '' }}">
                                                                            <label for="client_id" class="col-md-4 control-label">Cliente</label>

                                                                            <div class="col-md-6">
                                                                                <select id="client_id" name="client_id" class="form-control" required="required">
                                                                                    <option value="">Seleccione un Cliente...</option>
                                                                                    @foreach($clients as $client)
                                                                                        <option value="{{ $client->id }}"
                                                                                            @if ($client->id == $vehicle->client_id)
                                                                                                selected="selected"
                                                                                            @endif
                                                                                            >{{ $client -> name }}
                                                                                        </option>
                                                                                    @endforeach
                                                                                </select>

                                                                                @if ($errors->has('client_id'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('client_id') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>--}}

                                                                        <div class="{{ $errors->has('plate_number') ? ' has-error' : '' }}">
                                                                            <label for="plate_number" class="col-md-4 control-label">Número de placa</label>

                                                                            <div class="col-md-6">
                                                                                <input id="plate_number" type="text" class="form-control" name="plate_number" value="{{ $vehicle->plate_number }}" required autofocus>

                                                                                @if ($errors->has('plate_number'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('plate_number') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        <div class="{{ $errors->has('tag') ? ' has-error' : '' }}">
                                                                            <label for="tag" class="col-md-4 control-label">Tag</label>

                                                                            <div class="col-md-6">
                                                                                <input id="tag" type="text" class="form-control" name="tag" value="{{ $vehicle->tag }}" required autofocus>

                                                                                @if ($errors->has('tag'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('tag') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        {{-- <div class="{{ $errors->has('image') ? ' has-error' : '' }}">
                                                                            <label for="image" class="col-md-4 control-label">Fotos: {{ $vehicle->client->company->system_vehicle_name }}</label>

                                                                            <div class="col-md-6">
                                                                                <input id="image" type="file" class="form-control" name="image[]" multiple>

                                                                                @if ($errors->has('image'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('image') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div> --}}                                                                                 
                                                                    </div>  
                                                                    <div style="text-align: center !important; padding: 1%;">                                                                    
                                                                    <button  title="Editar" type="submit" class="btn btn-primary">Actualizar</button>                                                                                                     
                                                                    </div>
                                                               </div>
                                                             </div><!-- end modalcontent -->
                                                            </div><!-- end modaldialog -->
                                                         </form>
                                                     </div>
                                                     <!-- end modal -->


                                                     <button  type="button" class="btn boton_modaldanger" data-toggle="modal" data-target="#modalDelete{{$key}}" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></button>


                                                     <!-- Modal eliminar registro -->
                                                      <div class="modal fade" id="modalDelete{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalDelete{{$key}}">
                                                          <form action="{{ route('vehicles.destroy', $vehicle->id) }}" method="POST">
                                                              {{ csrf_field() }}
                                                              <input type="hidden" name="_method" value="DELETE">
                                                              <div class="modal-dialog" role="document">
                                                                  <div class="modal-content">
                                                                      <div class="modal-header">
                                                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                          <h4 class="modal-title" id="myModalLabel" style="color:#007bff">Eliminar Vehiculo</h4>
                                                                          
                                                                       </div>
                                                                      <div class="modal-body">
                                                                          <h5>¿Esta seguro de eliminar el vehicle id <b>{{ $vehicle -> id }}</b>?</h5>
                                                                      </div>
                                                                      <div class="modal-footer text-center">
                                                                          <button type="button" class="btn boton_modalwarning" data-dismiss="modal">Atrás</button>
                                                                          <button type="submit" class="btn boton_modaldanger">Eliminar</button>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </form>
                                                      </div>
                                                      <!-- end modal -->
                                        </td>
                                    </tr>
                                @endforeach
                           
                        </tbody>
                    </table>
                </div>
                     
            </div>
        </div>
    </div>
</div>
@stop
@section('modals')
    <!-- Modal agregar nuevo registro -->
    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="modal-add">
        <form class="form-horizontal" method="POST" action="{{ route('vehicles.store') }}" enctype="multipart/form-data" }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="ADD">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Agregar Vehiculo</h4>
                    </div>
                  <div class="modal-body">
                   <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                    {{--  <div class="form-group{{ $errors->has('client_id') ? ' has-error' : '' }}">
                            <label for="client_id" class="col-md-4 control-label">Cliente</label>

                            <div class="col-md-6">
                                <select id="client_id" name="client_id" class="form-control" required="required">
                                    <option value="">Seleccione un Cliente...</option>
                                    @foreach($clients as $client)
                                        <option value="{{ $client->id }}">{{ $client -> name }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('client_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('client_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>--}}

                        <div class="form-group{{ $errors->has('plate_number') ? ' has-error' : '' }}">
                            <label for="plate_number" class="col-md-4 control-label">Número de placa</label>

                            <div class="col-md-6">
                                <input id="plate_number" type="text" class="form-control" name="plate_number" value="{{ old('plate_number') }}" required autofocus>

                                @if ($errors->has('plate_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('plate_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('tag') ? ' has-error' : '' }}">
                            <label for="tag" class="col-md-4 control-label">Tag</label>

                            <div class="col-md-6">
                                <input id="tag" type="text" class="form-control" name="tag" value="{{ old('tag') }}" required autofocus>

                                @if ($errors->has('tag'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tag') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="image" class="col-md-4 control-label">Fotos: {{ session('company')->system_vehicle_name }}</label>

                            <div class="col-md-6">
                                <input id="image" type="file" class="form-control" name="image[]" multiple>

                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                     <div class="modal-footer text-center">
                                    <button type="button" class="btn boton_modalwarning" data-dismiss="modal">Atrás</button>
                                    <button type="submit" class="btn ">Aceptar</button>
                    </div> 
                  </div>
              </div>          
            </div>
        </form>
    </div>
    <!-- end modal -->
@stop
@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" /> 
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" /> 
    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.12.4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/Scriptdata.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.print.min.js') }}"></script>
@endsection

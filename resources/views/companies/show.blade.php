@extends('layouts.app')

@section('content')
 <div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                 <div class="sign-in-form-top">
                      <span>Compañia - Ver</span>
                      <div class="pull-right">
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar </button>
                        <a href="{{ route('companies.index') }}" type="button" class="btn btn-warning"> <i class="fa fa-reply" aria-hidden="true"></i> Atrás</a>
                        <a href="{{ route('companies.edit', $company -> id) }}" type="button" class="btn btn-primary "> <i class="fa fa-pencil-square-o" aria-hidden="true"></i>Editar</a>
                    </div>
                </div>

            
          <div class="signin">
            <div class="log-input">
              @include('flash::message')
              <div class="form-group">
                <label for="amount">Código: {{ $company -> code }}</label>
                </div>
              <div class="form-group">
                <label for="amount">Nombre:{{ $company -> name}}</label>
              </div>
              <div class="form-group">
                <label for="amount">Número de teléfono:{{ $company -> phone_number}}</label>
              </div>
              <div class="form-group">
                <label for="amount">Dirección:{{ $company -> address}}</label>
              </div>
              <div class="form-group">
                <label for="amount">Responsable:{{ $company -> responsable}}</label>
              </div>
              <div class="form-group">
                <label for="amount">Correo:{{ $company -> email}}</label>
              </div>
              <div class="form-group">
                <label for="amount">Usuario creador:{{ $company -> made_user}}</label>
              </div>
              <div class="form-group">
                <label for="amount">Limite de usuarios:{{ $company -> limit_user}}</label>
              </div>
              <div class="form-group">
                <label for="amount">RUC:{{ $company -> ruc}}</label>
              </div>
              <div class="form-group">
                <label for="amount">Icono:{{ $company -> favicon}}</label>
              </div>
              <div class="form-group">
                <label for="amount">Nombre del Host:{{ $company -> hostname}}</label>
              </div>
               
                        
                 </div>
              </div>
           </div>
         </div>
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('companies.destroy', $company->id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Compañia</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de eliminar el compañia <b>{{ $company -> name }}</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- end modal -->
@endsection

@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                <div class="sign-in-form-top">
                    <span>Compañias - Editar</span>
                    <div class="pull-right">
                        <a href="{{ route('companies.show', $company->id) }}" type="button" class="btn btn-warning"> <i class="fa fa-reply" aria-hidden="true"></i> Atrás</a>
                    </div>
                   </div>
                <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                    <form class="form-horizontal" method="POST" action="{{ route('companies.update', $company -> id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                            <label for="code" class="col-md-4 control-label">Código</label>

                            <div class="col-md-6">
                                <input id="code" type="text" class="form-control" name="code" value="{{ $company->code }}" required autofocus>

                                @if ($errors->has('code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $company->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                            <label for="phone_number" class="col-md-4 control-label">Número de Teléfono</label>

                            <div class="col-md-6">
                                <input id="phone_number" type="text" class="form-control" name="phone_number" value="{{ $company->phone_number }}" required autofocus>

                                @if ($errors->has('phone_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">Dirección</label>

                            <div class="col-md-6">
                                <textarea id="address" type="text" class="form-control" name="address" required autofocus>{{ $company->address }}</textarea>

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('responsable') ? ' has-error' : '' }}">
                            <label for="responsable" class="col-md-4 control-label">Responsable</label>

                            <div class="col-md-6">
                                <input id="responsable" type="text" class="form-control" name="responsable" value="{{ $company->responsable }}" required autofocus>

                                @if ($errors->has('responsable'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('responsable') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Correo</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $company->email }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('limit_user') ? ' has-error' : '' }}">
                            <label for="limit_user" class="col-md-4 control-label">Limite de Usuarios</label>

                            <div class="col-md-6">
                                <input id="limit_user" type="text" class="form-control" name="limit_user" value="{{ $company->limit_user }}" required>

                                @if ($errors->has('limit_user'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('limit_user') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('ruc') ? ' has-error' : '' }}">
                            <label for="ruc" class="col-md-4 control-label">RUC</label>

                            <div class="col-md-6">
                                <input id="ruc" type="text" class="form-control" name="ruc" value="{{ $company->ruc }}" required>

                                @if ($errors->has('ruc'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ruc') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="image" class="col-md-4 control-label">Icono</label>

                            <div class="col-md-6">
                                <input id="image" type="file" class="form-control" name="image" value="{{ $company->image }}" required>

                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('hostname') ? ' has-error' : '' }}">
                            <label for="hostname" class="col-md-4 control-label">Nombre del Host</label>

                            <div class="col-md-6">
                                <input id="hostname" type="text" class="form-control" name="hostname" value="{{ $company->hostname }}" required autofocus>

                                @if ($errors->has('hostname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('hostname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Aceptar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')

		<div class="sign-in-wrapper">
			<div class="graphs">
				<div class="sign-in-form">
					<div class="sign-in-form-top">
						<span class="title">Compañias</span>
						<div class="pull-right">                       
							<a href="{{ route('home')}}" class="btn btn-warning" id="atras" title="Atras"><i class="fa fa-reply" aria-hidden="true"></i></a>
							<a data-toggle="modal" data-target="#modal-add" type="button" class="btn btn-primary " title="Nuevo"> <i class="fa fa-plus-square-o" aria-hidden="true"></i></a>
						</div>
					</div>

					<div class="signin">
						<div class="log-input">
							@include('flash::message')
		                    @include('errors')
							<table id="companydata" class="table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<th>Código</th>
									<th>Nombre</th>
									<th>Teléfono</th>
									<th>RUC</th>
									<th>Opciones</th>
								</thead>
								<tbody>
										@foreach ($companies as $key => $company)
											<tr>
												<td>{{ $company->code }}</td>
												<td>{{ $company->name }}</td>
												<td>{{ $company->phone_number }}</td>
												<td>{{ $company->ruc }}</td>
												<td>
                                                 <button type="button" class="btn btn-primary " title="Modificar" data-toggle="modal" data-target="#modalEdit{{$key}}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>

													 <!-- Modal modificar registro -->
												     <div class="modal fade" id="modalEdit{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalEdit{{$key}}">
												         <form class="form-horizontal" action="{{ route('companies.update', $company->id) }}" method="POST" enctype="multipart/form-data">

													
												             {{ csrf_field() }}
												             <input type="hidden" name="_method" value="EDIT">
												             <div class="modal-dialog" role="document">
												                 <div class="modal-content">
												                     <div class="modal-header">
												                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												                        
												                         <h4 class="modal-title" id="myModalLabel"  style="color:#007bff">Editar Compañia</h4>
												                         
												                     </div>  
												                     	
												                        <div class="modal-footer text-center"> 		                       
												                         <input type="hidden" name="_method" value="PUT">

												                         <div class="{{ $errors->has('code') ? ' has-error' : '' }}">
												                             <label for="code" class="col-md-4 control-label">Código</label>

												                             <div class="col-md-6">
												                                 <input id="code" type="text" class="form-control" name="code" value="{{ $company->code }}" required autofocus>

												                                 @if ($errors->has('code'))
												                                     <span class="help-block">
												                                         <strong>{{ $errors->first('code') }}</strong>
												                                     </span>
												                                 @endif
												                             </div>
												                         </div>


												                         <div class="{{ $errors->has('name') ? ' has-error' : '' }}">
												                             <label for="name" class="col-md-4 ">Nombre</label>

												                             <div class="col-md-6">
												                                 <input id="name" type="text" class="form-control" name="name" value="{{ $company->name }}"  autofocus>

												                                 @if ($errors->has('name'))
												                                     <span class="help-block">
												                                         <strong>{{ $errors->first('name') }}</strong>
												                                     </span>
												                                 @endif
												                             </div>
												                         </div>

												                         <div class="{{ $errors->has('phone_number') ? ' has-error' : '' }}">
												                             <label for="phone_number" class="col-md-4 control-label">Número de Teléfono</label>

												                             <div class="col-md-6">
												                                 <input id="phone_number" type="text" class="form-control" name="phone_number" value="{{ $company->phone_number }}" autofocus>

												                                 @if ($errors->has('phone_number'))
												                                     <span class="help-block">
												                                         <strong>{{ $errors->first('phone_number') }}</strong>
												                                     </span>
												                                 @endif
												                             </div>
												                         </div>

												                         <div class="{{ $errors->has('address') ? ' has-error' : '' }}">
												                             <label for="address" class="col-md-4 control-label" style="padding: 10px;">Dirección</label>

												                             <div class="col-md-6" style="padding: 10px;">
												                                 <textarea style="width: 97%;" id="address" type="text" class="form-control" name="address" col="10" autofocus>{{ $company->address }}</textarea>

												                                 @if ($errors->has('address'))
												                                     <span class="help-block">
												                                         <strong>{{ $errors->first('address') }}</strong>
												                                     </span>
												                                 @endif
												                             </div>
												                         </div>
                                                                         <br>
												                         <div class="{{ $errors->has('responsable') ? ' has-error' : '' }}">
												                             <label for="responsable" class="col-md-4 control-label">Responsable</label>

												                             <div class="col-md-6">
												                                 <input id="responsable" type="text" class="form-control" name="responsable" value="{{ $company->responsable }}"  autofocus>

												                                 @if ($errors->has('responsable'))
												                                     <span class="help-block">
												                                         <strong>{{ $errors->first('responsable') }}</strong>
												                                     </span>
												                                 @endif
												                             </div>
												                         </div>

												                         <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
												                             <label for="email" class="col-md-4 control-label" style="padding: 10px;">Correo</label>

												                             <div class="col-md-6" style="padding: 10px;">
												                                 <input  style="width: 97%;" id="email" type="email" class="form-control" name="email" value="{{ $company->email }}" >

												                                 @if ($errors->has('email'))
												                                     <span class="help-block">
												                                         <strong>{{ $errors->first('email') }}</strong>
												                                     </span>
												                                 @endif
												                             </div>
												                         </div>

												                         <div class="{{ $errors->has('limit_user') ? ' has-error' : '' }}">
												                             <label for="limit_user" class="col-md-4 control-label">Limite de Usuarios</label>

												                             <div class="col-md-6">
												                                 <input id="limit_user" type="text" class="form-control" name="limit_user" value="{{ $company->limit_user }}" >

												                                 @if ($errors->has('limit_user'))
												                                     <span class="help-block">
												                                         <strong>{{ $errors->first('limit_user') }}</strong>
												                                     </span>
												                                 @endif
												                             </div>
												                         </div>

												                         <div class="{{ $errors->has('ruc') ? ' has-error' : '' }}">
												                             <label for="ruc" class="col-md-4 control-label">RUC</label>

												                             <div class="col-md-6">
												                                 <input id="ruc" type="text" class="form-control" name="ruc" value="{{ $company->ruc }}" >

												                                 @if ($errors->has('ruc'))
												                                     <span class="help-block">
												                                         <strong>{{ $errors->first('ruc') }}</strong>
												                                     </span>
												                                 @endif
												                             </div>
												                         </div>

												                         <div class="{{ $errors->has('image') ? ' has-error' : '' }}">
												                             <label for="image" class="col-md-4 control-label" style="padding: 10px;">Icono</label>

												                             <div class="col-md-6" style="padding: 10px;">
												                                 <input id="image" type="file" class="form-control" name="image" value="{{ $company->image }}" >

												                                 @if ($errors->has('image'))
												                                     <span class="help-block">
												                                         <strong>{{ $errors->first('image') }}</strong>
												                                     </span>
												                                 @endif
												                             </div>
												                         </div>
                                                                         

												                         <div  class="{{ $errors->has('hostname') ? ' has-error' : '' }}">
												                             <label for="hostname" class="col-md-4 control-label">Nombre del Host</label>

												                             <div class="col-md-6">
												                                 <input id="hostname" type="text" class="form-control" name="hostname" value="{{ $company->hostname }}"  autofocus>

												                                 @if ($errors->has('hostname'))
												                                     <span class="help-block">
												                                         <strong>{{ $errors->first('hostname') }}</strong>
												                                     </span>
												                                 @endif
												                             </div>
												                         </div>

												                     
																	 <div class="{{ $errors->has('system_vehicle_name') ? ' has-error' : '' }}">
											                             <label for="system_vehicle_name" class="col-md-4 control-label">Nombre para el vehiculo en el sistema</label>
											                             <div class="col-md-6">
											                                 <input id="system_vehicle_name" type="text" class="form-control" name="system_vehicle_name" value="{{ $company->system_vehicle_name }}" autofocus>

											                                 @if ($errors->has('system_vehicle_name'))
											                                     <span class="help-block">
											                                         <strong>{{ $errors->first('system_vehicle_name') }}</strong>
											                                     </span>
											                                 @endif
											                             </div>
											                         </div>
											                         <div class="{{ $errors->has('system_container_name') ? ' has-error' : '' }}">
											                             <label for="system_container_name" class="col-md-4 control-label">Nombre para el container en el sistema</label>
											                             <div class="col-md-6">
											                                 <input id="system_container_name" type="text" class="form-control" name="system_container_name" value="{{ $company->system_container_name }}" autofocus>

											                                 @if ($errors->has('system_container_name'))
											                                     <span class="help-block">
											                                         <strong>{{ $errors->first('system_container_name') }}</strong>
											                                     </span>
											                                 @endif
											                             </div>
											 			             </div>
                                                                                          
                                                                  	</div>	
                                                                  	<div style="text-align: center !important; padding: 1%;">
                                                                  	
                                                                  	<button  title="Editar" type="submit" class="btn btn-primary">Actualizar</button>                                           										                  
												              </div>
												             </div><!-- end modalcontent -->
												            </div><!-- end modaldialog -->
												         </form>
												     </div>
												     <!-- end modal -->


													 <button  type="button" class="btn boton_modaldanger" data-toggle="modal" data-target="#modalDelete{{$key}}" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></button>


													 <!-- Modal eliminar registro -->
													  <div class="modal fade" id="modalDelete{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalDelete{{$key}}">
														  <form action="{{ route('companies.destroy', $company->id) }}" method="POST">
															  {{ csrf_field() }}
															  <input type="hidden" name="_method" value="DELETE">
															  <div class="modal-dialog" role="document">
																  <div class="modal-content">
																	  <div class="modal-header">
																		  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																		  <h4 class="modal-title" id="myModalLabel" style="color:#007bff">Eliminar Compañia</h4>
																		  
                                                                       </div>
																	  <div class="modal-body">
																		  <h5>¿Esta seguro de eliminar el compañia <b>{{ $company -> name }}</b>?</h5>
																	  </div>
																	  <div class="modal-footer text-center">
																		  <button type="button" class="btn boton_modalwarning" data-dismiss="modal">Atrás</button>
																		  <button type="submit" class="btn boton_modaldanger">Eliminar</button>
																	  </div>
																  </div>
															  </div>
														  </form>
													  </div>
													  <!-- end modal -->
											 	</td>
											</tr>
										@endforeach

								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
		</div>


	@stop

    @section('modals')
    <!-- Modal agregar nuevo registro -->
    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="modal-add">
        <form class="form-horizontal"  action="{{ route('companies.store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{-- <input type="hidden" name="_method" value="ADD"> --}}
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel" style="color:#007bff">Agregar Compañia</h4>
                    </div>
                  <div class="modal-body">
                   <div class="signin">
                    <div class="log-input">
                    {{-- @include('flash::message')
                    @include('errors') --}}
                     <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                            <label for="phone_number" class="col-md-4 control-label">Número de Teléfono</label>
                            <div class="col-md-6">
                                <input id="phone_number" type="text" class="form-control" name="phone_number" value="{{ old('phone_number') }}"  autofocus>

                                @if ($errors->has('phone_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">Dirección</label>
                            <div class="col-md-6">
                                <textarea id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" autofocus></textarea>
                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('responsable') ? ' has-error' : '' }}">
                            <label for="responsable" class="col-md-4 control-label">Responsable</label>
                            <div class="col-md-6">
                                <input id="responsable" type="text" class="form-control" name="responsable" value="{{ old('responsable') }}"  autofocus>

                                @if ($errors->has('responsable'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('responsable') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Correo</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" >

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('limit_user') ? ' has-error' : '' }}">
                            <label for="limit_user" class="col-md-4 control-label">Limite de Usuarios</label>
                            <div class="col-md-6">
                                <input id="limit_user" type="text" class="form-control" name="limit_user" value="{{ old('limit_user') }}" >

                                @if ($errors->has('limit_user'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('limit_user') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('ruc') ? ' has-error' : '' }}">
                            <label for="ruc" class="col-md-4 control-label">RUC</label>
                            <div class="col-md-6">
                                <input id="ruc" type="text" class="form-control" name="ruc" value="{{ old('ruc') }}" >

                                @if ($errors->has('ruc'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ruc') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="image" class="col-md-4 control-label">Icono</label>
                            <div class="col-md-6">
                                <input id="image" type="file" class="form-control" name="image">

                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('hostname') ? ' has-error' : '' }}">
                            <label for="hostname" class="col-md-4 control-label">Nombre del Host</label>
                            <div class="col-md-6">
                                <input id="hostname" type="text" class="form-control" name="hostname" value="{{ old('hostname') }}" autofocus>

                                @if ($errors->has('hostname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('hostname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('system_vehicle_name') ? ' has-error' : '' }}">
                            <label for="system_vehicle_name" class="col-md-4 control-label">Nombre para el vehiculo en el sistema</label>
                            <div class="col-md-6">
                                <input id="system_vehicle_name" type="text" class="form-control" name="system_vehicle_name" value="{{ old('system_vehicle_name') }}" autofocus>

                                @if ($errors->has('system_vehicle_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('system_vehicle_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('system_container_name') ? ' has-error' : '' }}">
                            <label for="system_container_name" class="col-md-4 control-label">Nombre para el container en el sistema</label>
                            <div class="col-md-6">
                                <input id="system_container_name" type="text" class="form-control" name="system_container_name" value="{{ old('system_container_name') }}"  autofocus>

                                @if ($errors->has('system_container_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('system_container_name') }}</strong>
                                    </span>
                                @endif
                            </div>
			             </div>
			          </div>
			          <div class="modal-footer text-center">
			                        <button type="button" class="btn boton_modalwarning" data-dismiss="modal">Atrás</button>
			                        <button type="submit" class="btn ">Aceptar</button>
			            </div>
			      </div>
			  </div>
            </div>
        </form>
    </div>
    <!-- end modal -->

  

	@stop

	@section('scripts')

    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" />
    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.12.4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/Scriptdata.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.print.min.js') }}"></script>

	@endsection

@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                <div class="sign-in-form-top">
                    <span>Empresa - Nuevo</span> 
                    <div class="pull-right">                       
                        <a href="{{ route('companies.index') }}" type="button" class="btn btn-warning"> <i class="fa fa-reply" aria-hidden="true"></i>Atras</a>                           
                    </div>                   
                 </div>
                <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                    <form class="form-horizontal" method="post" action="{{ route('companies.store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                            <label for="phone_number" class="col-md-4 control-label">Número de Teléfono</label>
                            <div class="col-md-6">
                                <input id="phone_number" type="text" class="form-control" name="phone_number" value="{{ old('phone_number') }}" required autofocus>

                                @if ($errors->has('phone_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">Dirección</label>
                            <div class="col-md-6">
                                <textarea id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required autofocus></textarea>
                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('responsable') ? ' has-error' : '' }}">
                            <label for="responsable" class="col-md-4 control-label">Responsable</label>
                            <div class="col-md-6">
                                <input id="responsable" type="text" class="form-control" name="responsable" value="{{ old('responsable') }}" required autofocus>

                                @if ($errors->has('responsable'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('responsable') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Correo</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('limit_user') ? ' has-error' : '' }}">
                            <label for="limit_user" class="col-md-4 control-label">Limite de Usuarios</label>
                            <div class="col-md-6">
                                <input id="limit_user" type="text" class="form-control" name="limit_user" value="{{ old('limit_user') }}" required>

                                @if ($errors->has('limit_user'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('limit_user') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('ruc') ? ' has-error' : '' }}">
                            <label for="ruc" class="col-md-4 control-label">RUC</label>
                            <div class="col-md-6">
                                <input id="ruc" type="text" class="form-control" name="ruc" value="{{ old('ruc') }}" required>

                                @if ($errors->has('ruc'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ruc') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="image" class="col-md-4 control-label">Icono</label>
                            <div class="col-md-6">
                                <input id="image" type="file" class="form-control" name="image" required>

                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('hostname') ? ' has-error' : '' }}">
                            <label for="hostname" class="col-md-4 control-label">Nombre del Host</label>
                            <div class="col-md-6">
                                <input id="hostname" type="text" class="form-control" name="hostname" value="{{ old('hostname') }}" required autofocus>

                                @if ($errors->has('hostname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('hostname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('system_vehicle_name') ? ' has-error' : '' }}">
                            <label for="system_vehicle_name" class="col-md-4 control-label">Nombre para el vehiculo en el sistema</label>
                            <div class="col-md-6">
                                <input id="system_vehicle_name" type="text" class="form-control" name="system_vehicle_name" value="{{ old('system_vehicle_name') }}" required autofocus>

                                @if ($errors->has('system_vehicle_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('system_vehicle_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('system_container_name') ? ' has-error' : '' }}">
                            <label for="system_container_name" class="col-md-4 control-label">Nombre para el container en el sistema</label>
                            <div class="col-md-6">
                                <input id="system_container_name" type="text" class="form-control" name="system_container_name" value="{{ old('system_container_name') }}" required autofocus>

                                @if ($errors->has('system_container_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('system_container_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Aceptar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

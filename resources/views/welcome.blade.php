<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Control de Acceso</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
        <link href="{{ asset('assets/css/style1.css') }}" rel="stylesheet"/>

        <!-- Styles -->
    <style>
.grid-menu {
    position: fixed;
    display: flex;
    /*justify-content: center;*/
    justify-items: center;

    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
   /* background-color: rgba(43, 43, 43, 0.8);*/
    background: url('./assets/img/images.jpg') no-repeat !important;
   -webkit-background-size: 100% 100%;  /* Safari 3.0 */
     -moz-background-size: 100% 100%; /* Gecko 1.9.2 (Firefox 3.6) */
     -o-background-size: 100% 100%;  /* Opera 9.5 */
      background-size: 100% 100%;  /* Gecko 2.0 (Firefox 4.0) and other CSS3-compliant browsers */
}

</style>
    </head>
    <body>
        <section>
            <div class="barramenu">
        		<ul class="nofitications-dropdown">
        			<li class="login_box">
        				<span class="dropdown-toggle title" data-toggle="dropdown" aria-expanded="false" style="color: white;">Control de Acceso</span>
        			</li>
        		  {{-- <li class="login_box" id="loginContainer">
        				<div id="sb-search" class="sb-search">
        					<form>
        						<input class="sb-search-input" placeholder="Enter your search term..." type="search" id="search">
        						<input class="sb-search-submit" type="submit" value="">
        						<span class="sb-icon-search"></span>
        					</form>
        			     </div>
        				<!-- search-scripts -->
        				<script src="assets/js/classie.js"></script>
        				<script src="assets/js/uisearch.js"></script>
        				<script>
        					new UISearch( document.getElementById( 'sb-search' ) );
        				</script>
        				<!-- //search-scripts -->
        			</li>--}}
        		 	<li style="float: right; margin: 1%;">
        		 		<a style="color: white;" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();"><i class="fa fa-power-off "></i></a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                		</form>
                	</li>
        		</ul>
        	</div>
    </div>
        <div class="grid-menu transition-menu">
            @if (Route::has('login'))
                <div    class="grid-content">
                    @auth
                        <a class="grid-item" href="{{ url('/home') }}"><div class="grid-item-icon">
                     <img class="user-menu" src="assets/img/home.ico" alt="home.ico">
                       </div>
                      <span    class="grid-item-title">
                       Home
                  </span></a>
                    @else
                        <a class="grid-item" href="{{ route('login') }}"><div class="grid-item-icon">
                             <img class="user-menu" src="assets/img/login.ico" alt="login.ico">
                               </div>
                            <span    class="grid-item-title">
                               Login
                            </span>
                      </a>
                        <a class="grid-item" href="{{ url('/register') }}"><div class="grid-item-icon">
                     <img class="user-menu" src="assets/img/2.ico" alt="2.ico">
                       </div>
                      <span    class="grid-item-title">
                       Registrar
                  </span></a>
                    @endauth
                </div>
            @endif
        </div>
      <section>
    </body>
</html>

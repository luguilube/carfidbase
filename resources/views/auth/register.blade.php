@extends('layouts.app')

@section('content')

    <div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                 <div class="sign-in-form-top">
                            <span class="title" >
                            Registro</span>
                            <div class="pull-right">                       
                        <a href="{{ route('login') }}" type="button" class="btn btn-warning"> <i class="fa fa-share" aria-hidden="true"></i> Atrás</a>
                             </div>
                    </div>
                <div class="signin">
                  <div class="log-input">
                    <form  class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                     
                       
                         <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre</label>

                            <div class="col-md-6">
                                  <input id="name" type="text"  class ="form-control " value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif 
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Email</label>

                            <div class="col-md-6">
                                <input type="text" class ="form-control " value="{{ old('email') }}" required autofocus/>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input type="text" class ="form-control "  required autofocus/>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif 
                            </div>
                        </div>
                         <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Confirmar Contraseña</label>

                            <div class="col-md-6">
                                <input type="text" class ="form-control "  required autofocus/>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif 
                            </div>
                        </div>
                     <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Teléfono</label>

                            <div class="col-md-6">
                                <input type="text" class ="form-control "  required autofocus/>

                               @if ($errors->has('phone_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                       
                      <div style="text-align: center">
                       <button type="submit" class="btn btn-primary">
                                    Registrar
                        </button>
                         </div>
                     
                       </form>                   
                    </div>
                </div>
            </div>
        </div>
    </div>     
</div>
@endsection

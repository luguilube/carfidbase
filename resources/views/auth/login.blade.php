@extends('layouts.app')

@section('content')

    <div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form1">
                <div class="sign-in-form-top">
                        <span class="title">Login</span> 
                        
                 </div>
                <div class="signin">
                    <div class="signin-rit">
                        <a  href="{{ route('password.request') }}">¿Olvido su contraseña?</a> 
                        <div class="clearfix"> </div>
                        <div class="signin-rit">
                            <a  href="{{ route('register') }}">Registrarse</a>
                            <div class="clearfix"> </div>
                            <br>
                        </div>

                    </div>

                    <div class="log-input">
                        <form  method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}

                            <div class="log-input-left{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input type="text" class="user" value="Yourname" name="email" value="{{ old('email') }}" required autofocus/>
                            </div>
                            
                            <div class="clearfix"> </div>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif

                            <div class="log-input-left{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input id="password" type="password" name="password" class="lock" value="password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email address:';}"/>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                             
                            <div class="clearfix"> </div>
                            <button type="submit" class="btn btn-primary">
                                Login
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>     
</div>     
@endsection

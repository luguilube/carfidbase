@extends('layouts.app')

@section('content')

    <div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                <div class="sign-in-form-top">
                            <span class="title"  >Recuperar contraseña</span> 
                            <div class="pull-right">                       
                        <a href="{{ route('login') }}" type="button" class="btn btn-warning"> <i class="fa fa-share" aria-hidden="true"></i> Atrás</a>
                        </div>
                    </div>

               
             <div class="signin">
                <div class="log-input">
                    <form  method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="log-input-left{{ $errors->has('email') ? ' has-error' : '' }}">                          

                            
                                <input type="text" class="user" value="Ingrese email" name="email" value="{{ old('email') }}" required autofocus/>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            
                        </div>

                        <div class="log-input-left{{ $errors->has('password') ? ' has-error' : '' }}">
                            
                            
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                       

                        <div class="log-input-left{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            
                              <input id="password" type="password" name="password" class="lock" value="password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email address:';}"/>
                                

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            
                        </div>

                        
                            
                                <button type="submit" class="btn btn-primary">
                                    recuperar Contraseña
                                </button>
                            
                       
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')

    <div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                <div class="sign-in-form-top">
                            <span class="title" >Recuperar Contraseña</span>
                            <div class="pull-right">                       
                        <a href="{{ route('login') }}" type="button" class="btn btn-warning"> <i class="fa fa-share" aria-hidden="true"></i> Atrás</a>
                        </div>
                </div>               
               
               <div class="signin">
                <div class="log-input">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                       <div class="log-input-left{{ $errors->has('email') ? ' has-error' : '' }}">                          

                            
                                <input type="text" class="user" value="Ingrese Email" name="email" value="{{ old('email') }}" required autofocus/>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            
                        </div>

                      
                                <button type="submit" class="btn btn-primary">
                                    Enviar link
                                </button>
                           
                    </form>
                   </div>
                 </div>
             </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                <div class="sign-in-form-top">
                    <span  class="title"  >Seleccione la Empresa</span>
                </div>
                <div class="signin">
                    <form action="{{ route('select.selected') }}" method="POST" >
                        {{ csrf_field() }}
                        @foreach ($companies as $company)
                            <button class="btn btn-md btn-primary" style="margin: 1%;" type="submit" name="company" value="{{ $company->id }}">{{ strtoupper($company->name) }}</button>
                        @endforeach
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

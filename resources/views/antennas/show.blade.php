@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
    <div class="graphs">
        <div class="sign-in-form">
            <div class="sign-in-form-top">
              	<span>Antena - Ver</span>
				<div class="pull-right ">
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</button>
					<a href="{{ route('antennas.index') }}" type="button" class="btn btn-warning "> <i class="fa fa-reply" aria-hidden="true"></i> Atrás</a>
					<a href="{{ route('antennas.edit', $antenna -> id) }}" type="button" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
				</div>
            </div>
	        <div class="signin">
	            <div class="log-input">
					@include('flash::message')
					@include('errors')
					<div class="form-group">
					  	<label for="amount">Dirección IP: {{ $antenna -> ip_address }}</label>
					</div>
				   	<div class="form-group">
					 	<label for="amount">Código: {{ $antenna -> code }}</label>
				   	</div>
					<div class="form-group">
					 	<label for="amount">Descripción: {{ $antenna -> description }}</label>
				   	</div>
			 	</div>
		 	</div>
		</div>
	</div>
</div>
@stop
@section('modals')
<!-- Modal -->
<!-- MODAL PARA ELIMINAR REGISTRO -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
    <form action="{{ route('antennas.destroy', $antenna -> id) }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="modal-delete" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Eliminar Antena</h4>
                </div>
                <div class="modal-body">
                    <p>¿Esta seguro de eliminar la antena <b>{{ $antenna -> name}}</b>?</p>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- END Modal  -->
@endsection

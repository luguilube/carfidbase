@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                <div class="sign-in-form-top">
                    <span>Antenas - Nuevo</span> 
                    <div class="pull-right">                       
                        <a href="{{ route('antennas.index') }}" type="button" class="btn btn-warning"> <i class="fa fa-reply" aria-hidden="true"></i>Atras</a>                           
                    </div>                   
                 </div>
                <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                    <form class="form-horizontal" method="POST" action="{{ route('antennas.store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('mini_server_id') ? ' has-error' : '' }}">
                            <label for="mini_server_id" class="col-md-4 control-label">Mini Servidor</label>

                            <div class="col-md-6">
                                <select id="mini_server_id" name="mini_server_id" class="form-control" required="required">
                                    <option value="">Seleccione un Mini Servidor...</option>
                                    @foreach($mini_servers as $element)
                                        <option value="{{ $element->id }}">{{ $element -> user_name }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('mini_server_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mini_server_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('ip_address') ? ' has-error' : '' }}">
                            <label for="ip_address" class="col-md-4 control-label">Dirección IP</label>

                            <div class="col-md-6">
                                <input id="ip_address" type="text" class="form-control" name="ip_address" value="{{ old('ip_address') }}" required autofocus>

                                @if ($errors->has('ip_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ip_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('port') ? ' has-error' : '' }}">
                            <label for="port" class="col-md-4 control-label">Puerto</label>

                            <div class="col-md-6">
                                <input id="port" type="text" class="form-control" name="port" value="{{ old('port') }}" required autofocus>

                                @if ($errors->has('port'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('port') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('power') ? ' has-error' : '' }}">
                            <label for="power" class="col-md-4 control-label">Poder</label>

                            <div class="col-md-6">
                                <input id="power" type="text" class="form-control" name="power" value="{{ old('power') }}" required autofocus>

                                @if ($errors->has('power'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('power') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Descripción</label>

                            <div class="col-md-6">
                                <textarea id="description" type="text" class="form-control" name="description" value="{{ old('description') }}" autofocus></textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Registrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

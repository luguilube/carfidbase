@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">


            <div class="graphs">
                <div class="sign-in-form">
                    <div class="sign-in-form-top">
                        <span class="title">Lista de antenas</span>
                        <div class="pull-right">                        
                            <a href="{{ route('home')}}" class="btn btn-warning" id="atras"><i class="fa fa-reply" aria-hidden="true"></i></a>
                             <a data-toggle="modal" data-target="#modal-add" type="button" class="btn btn-primary " title="Nuevo"> <i class="fa fa-plus-square-o" aria-hidden="true"></i></a>     
                        </a>
                    </div>
                    </div>              
                    
                    <div class="signin">  
                        <div class="log-input">
                @include('flash::message')
                <div class="panel-body">
                     <table  id="companydata" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <th>Dirección IP</th>
                            <th>Código</th>
                            <th>Descripción</th>
                            <th>Opciones</th>
                        </thead>
                        <tbody> 
                        	@foreach ($antennas as $key => $antenna)
                                    <tr>

                                        <td>{{ $antenna->ip_address }}</td>
                                        <td>{{ $antenna->port }}</td>
                                        <td>{{ $antenna->power }}</td>
                                        <td>{{ $antenna->description }}</td>
                                        <td>{{ $antenna->creator_user }}</td>
                                        <td>{{ $antenna->miniServers->user_name }}</td>
                                        <td>{{ $antenna->created_at }}</td>
                                        <td>{{ $antenna->updated_at }}</td>
                                        <td>
                                            <button type="button" class="btn btn-primary " title="Modificar" data-toggle="modal" data-target="#modalEdit{{$key}}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>

                                                     <!-- Modal modificar registro -->
                                                     <div class="modal fade" id="modalEdit{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalEdit{{$key}}">
                                                         <form class="form-horizontal" action="{{ route('antennas.update', $antenna->id) }}" method="POST" enctype="multipart/form-data">

                                                    
                                                             {{ csrf_field() }}
                                                             <input type="hidden" name="_method" value="EDIT">
                                                             <div class="modal-dialog" role="document">
                                                                 <div class="modal-content">
                                                                     <div class="modal-header">
                                                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                        
                                                                         <h4 class="modal-title" id="myModalLabel"  style="color:#007bff">Editar Antena</h4>
                                                                         
                                                                     </div>  
                                                                        
                                                                        <div class="modal-footer text-center">                             
                                                                         <input type="hidden" name="_method" value="PUT">
                                                                        {{-- <div class="{{ $errors->has('mini_server_id') ? ' has-error' : '' }}">
                                                                            <label for="mini_server_id" class="col-md-4 control-label">Mini Servidor</label>

                                                                            <div class="col-md-6">
                                                                                <select id="mini_server_id" name="mini_server_id" class="form-control" required="required">
                                                                                    <option value="">Seleccione un Mini Servidor...</option>
                                                                                    @foreach($mini_servers as $element)
                                                                                        <option value="{{ $element->id }}"
                                                                                            @if ($element->id == $antenna->mini_server_id)
                                                                                                selected="selected"
                                                                                            @endif
                                                                                            >{{ $element -> user_name }}
                                                                                        </option>
                                                                                    @endforeach
                                                                                </select>

                                                                                @if ($errors->has('mini_server_id'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('mini_server_id') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>--}}

                                                                        <div class="{{ $errors->has('ip_address') ? ' has-error' : '' }}">
                                                                            <label for="ip_address" class="col-md-4 control-label">Dirección IP</label>

                                                                            <div class="col-md-6">
                                                                                <input id="ip_address" type="text" class="form-control" name="ip_address" value="{{ $antenna->ip_address }}" required autofocus>

                                                                                @if ($errors->has('ip_address'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('ip_address') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        <div class="{{ $errors->has('port') ? ' has-error' : '' }}">
                                                                            <label for="port" class="col-md-4 control-label">Puerto</label>

                                                                            <div class="col-md-6">
                                                                                <input id="port" type="text" class="form-control" name="port" value="{{ $antenna->port }}" required autofocus>

                                                                                @if ($errors->has('port'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('port') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        <div class="{{ $errors->has('power') ? ' has-error' : '' }}">
                                                                            <label for="power" class="col-md-4 control-label">Poder</label>

                                                                            <div class="col-md-6">
                                                                                <input id="power" type="text" class="form-control" name="power" value="{{ $antenna->power }}" required autofocus>

                                                                                @if ($errors->has('power'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('power') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        <div class="{{ $errors->has('description') ? ' has-error' : '' }}">
                                                                            <label for="description" class="col-md-4 control-label">Descripción</label>

                                                                            <div class="col-md-6">
                                                                                <textarea id="description" type="text" class="form-control" name="description" autofocus>{{ $antenna->description }}</textarea>

                                                                                @if ($errors->has('description'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('description') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                                                                                                           
                                                                    </div>  
                                                                    <div style="text-align: center !important; padding: 1%;">                                                                    
                                                                    <button  title="Editar" type="submit" class="btn btn-primary">Actualizar</button>                                                                                                     
                                                                    </div>
                                                               </div>
                                                             </div><!-- end modalcontent -->
                                                            </div><!-- end modaldialog -->
                                                         </form>
                                                     </div>
                                                     <!-- end modal -->


                                                     <button  type="button" class="btn boton_modaldanger" data-toggle="modal" data-target="#modalDelete{{$key}}" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></button>


                                                     <!-- Modal eliminar registro -->
                                                      <div class="modal fade" id="modalDelete{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalDelete{{$key}}">
                                                          <form action="{{ route('antennas.destroy', $antenna->id) }}" method="POST">
                                                              {{ csrf_field() }}
                                                              <input type="hidden" name="_method" value="DELETE">
                                                              <div class="modal-dialog" role="document">
                                                                  <div class="modal-content">
                                                                      <div class="modal-header">
                                                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                          <h4 class="modal-title" id="myModalLabel" style="color:#007bff">Eliminar Antena</h4>
                                                                          
                                                                       </div>
                                                                      <div class="modal-body">
                                                                          <h5>¿Esta seguro de eliminar antena <b></b>?</h5>
                                                                      </div>
                                                                      <div class="modal-footer text-center">
                                                                          <button type="button" class="btn boton_modalwarning" data-dismiss="modal">Atrás</button>
                                                                          <button type="submit" class="btn boton_modaldanger">Eliminar</button>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </form>
                                                      </div>
                                                      <!-- end modal -->
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
 @section('modals')
    <!-- Modal agregar nuevo registro -->
    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="modal-add">
        <form class="form-horizontal" method="POST" action="{{ route('antennas.store') }}" enctype="multipart/form-data" }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="ADD">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Agregar Antena</h4>
                    </div>
                  <div class="modal-body">
                   <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                    {{--<div class="form-group{{ $errors->has('mini_server_id') ? ' has-error' : '' }}">
                            <label for="mini_server_id" class="col-md-4 control-label">Mini Servidor</label>

                            <div class="col-md-6">
                                <select id="mini_server_id" name="mini_server_id" class="form-control" required="required">
                                    <option value="">Seleccione un Mini Servidor...</option>
                                    @foreach($mini_servers as $element)
                                        <option value="{{ $element->id }}">{{ $element -> user_name }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('mini_server_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mini_server_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>--}} 

                        <div class="form-group{{ $errors->has('ip_address') ? ' has-error' : '' }}">
                            <label for="ip_address" class="col-md-4 control-label">Dirección IP</label>

                            <div class="col-md-6">
                                <input id="ip_address" type="text" class="form-control" name="ip_address" value="{{ old('ip_address') }}" required autofocus>

                                @if ($errors->has('ip_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ip_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('port') ? ' has-error' : '' }}">
                            <label for="port" class="col-md-4 control-label">Puerto</label>

                            <div class="col-md-6">
                                <input id="port" type="text" class="form-control" name="port" value="{{ old('port') }}" required autofocus>

                                @if ($errors->has('port'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('port') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('power') ? ' has-error' : '' }}">
                            <label for="power" class="col-md-4 control-label">Poder</label>

                            <div class="col-md-6">
                                <input id="power" type="text" class="form-control" name="power" value="{{ old('power') }}" required autofocus>

                                @if ($errors->has('power'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('power') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Descripción</label>

                            <div class="col-md-6">
                                <textarea id="description" type="text" class="form-control" name="description" value="{{ old('description') }}" autofocus></textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                     <div class="modal-footer text-center">
                                    <button type="button" class="btn boton_modalwarning" data-dismiss="modal">Atrás</button>
                                    <button type="submit" class="btn ">Aceptar</button>
                 </div>  
                  </div>
              </div>          
            </div>
        </form>
    </div>
    <!-- end modal -->
@stop
@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" />
    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.12.4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/Scriptdata.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.print.min.js') }}"></script>
@endsection

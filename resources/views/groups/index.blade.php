@extends('layouts.app')

@section('content')
   <div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                  
                  <div class="sign-in-form-top">
                        <span class="title" >Lista de Grupos</span>
                        <div class="pull-right">
                            <a href="{{ route('home') }}" type="button" class="btn btn-warning"> <i class="fa fa-reply" aria-hidden="true"></i> </a>
                             <a data-toggle="modal" data-target="#modal-add" type="button" class="btn btn-primary " title="Nuevo"> <i class="fa fa-plus-square-o" aria-hidden="true"></i></a>                        
                    </div>
                 </div>   
                @include('flash::message')
                <div class="signin">                     
                  <div class="log-input">
                    <table id="companydata" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Opciones</th>
                        </thead>
                        <tbody>
                            
                                @foreach ($groups as $key => $group)
                                    <tr>
                                        <td {{ $group->id }}</td>
                                        <td>{{ $group->name }}</td>
                                        <td>{{ $group->description }}</td>
                                        <td>
                                            <button type="button" class="btn btn-primary " title="Modificar" data-toggle="modal" data-target="#modalEdit{{$key}}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>

                                                     <!-- Modal modificar registro -->
                                                     <div class="modal fade" id="modalEdit{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalEdit{{$key}}">
                                                         <form class="form-horizontal" action="{{ route('groups.update', $group->id) }}" method="POST" enctype="multipart/form-data">

                                                    
                                                             {{ csrf_field() }}
                                                             <input type="hidden" name="_method" value="EDIT">
                                                             <div class="modal-dialog" role="document">
                                                                 <div class="modal-content">
                                                                     <div class="modal-header">
                                                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                        
                                                                         <h4 class="modal-title" id="myModalLabel"  style="color:#007bff">Editar Rol</h4>
                                                                         
                                                                     </div>  
                                                                        
                                                                        <div class="modal-footer text-center">                             
                                                                         <input type="hidden" name="_method" value="PUT">
                                                                           <div class="{{ $errors->has('name') ? ' has-error' : '' }}">
                                                                            <label for="name" class="col-md-4 control-label">Nombre</label>

                                                                            <div class="col-md-6">
                                                                                <input id="name" type="text" class="form-control" name="name" value="{{ $group->name }}"  autofocus>

                                                                                @if ($errors->has('name'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('name') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        

                                                                        <div class="{{ $errors->has('description') ? ' has-error' : '' }}">
                                                                            <label for="description" class="col-md-4 control-label">Descripción</label>

                                                                            <div class="col-md-6">
                                                                                <input id="description" type="text" class="form-control" name="description" value="{{ $group->description }}" autofocus>

                                                                                @if ($errors->has('description'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('description') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                       {{-- <div style="border-style: solid;border-color: #e0ddddb3;">
                                                                            <table id="checkdata" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>Todos
                                                                                            <form action="#">
                                                                                                 <input type="checkbox" id="checkAll"/>      
                                                                                                    <fieldset></th>
                                                                                        <th>Código</th>
                                                                                        <th>Nombre</th>
                                                                                        <th>Número de Teléfono</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                @foreach($clients as $element)
                                                                                    <tr>
                                                                                        <td width="40">
                                                                                            <input id="{{ $element -> id }}" type="checkbox" name="clients[]" class="seleted-check" value="{{ $element -> id }}">
                                                                                        </td>
                                                                                        </fieldset>
                                                                                                </form>
                                                                                        <td>{{ $element -> code }}</td>
                                                                                        <td>{{ $element -> name }}</td>
                                                                                        <td>{{ $element -> phone_number }}</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                                </tbody>
                                                                            </table>
                                                                        </div> --}}                                                                                    
                                                                    </div>  
                                                                    <div style="text-align: center !important; padding: 1%;">                                                                    
                                                                    <button  title="Editar" type="submit" class="btn btn-primary">Actualizar</button>                                                                                                     
                                                                    </div>
                                                               </div>
                                                             </div><!-- end modalcontent -->
                                                            </div><!-- end modaldialog -->
                                                         </form>
                                                     </div>
                                                     <!-- end modal -->


                                                     <button  type="button" class="btn boton_modaldanger" data-toggle="modal" data-target="#modalDelete{{$key}}" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></button>


                                                     <!-- Modal eliminar registro -->
                                                      <div class="modal fade" id="modalDelete{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalDelete{{$key}}">
                                                          <form action="{{ route('groups.destroy', $group->id) }}" method="POST">
                                                              {{ csrf_field() }}
                                                              <input type="hidden" name="_method" value="DELETE">
                                                              <div class="modal-dialog" role="document">
                                                                  <div class="modal-content">
                                                                      <div class="modal-header">
                                                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                          <h4 class="modal-title" id="myModalLabel" style="color:#007bff">Eliminar grupo</h4>
                                                                          
                                                                       </div>
                                                                      <div class="modal-body">
                                                                          <h5>¿Esta seguro de eliminar el grupo <b>{{ $group -> name }}</b>?</h5>
                                                                      </div>
                                                                      <div class="modal-footer text-center">
                                                                          <button type="button" class="btn boton_modalwarning" data-dismiss="modal">Atrás</button>
                                                                          <button type="submit" class="btn boton_modaldanger">Eliminar</button>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </form>
                                                      </div>
                                                      <!-- end modal -->
                                            
                                        </td>
                                    </tr>
                                @endforeach
                            
                        </tbody>
                    </table>
                
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
@stop
@section('modals')
    <!-- Modal agregar nuevo registro -->
    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="modal-add">
        <form class="form-horizontal" method="POST" action="{{ route('groups.store') }}" enctype="multipart/form-data" }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="ADD">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Agregar Grupo</h4>
                    </div>
                  <div class="modal-body">
                   <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                     
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"  autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Descripción</label>

                            <div class="col-md-6">
                                <textarea id="description" type="text" class="form-control" name="description" value="{{ old('description') }}" autofocus></textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       {{-- <div style="border-style: solid;border-color: #e0ddddb3;">
                            <table id="checkdata" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Todos
                                            <form action="#">
                                                 <input type="checkbox" id="checkAll"/>      
                                                    <fieldset></th>
                                        <th>Código</th>
                                        <th>Nombre</th>
                                        <th>Número de Teléfono</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($clients as $element)
                                    <tr>
                                        <td width="40">
                                            <input id="{{ $element -> id }}" type="checkbox" name="clients[]" class="seleted-check" value="{{ $element -> id }}">
                                        </td>
                                                     </fieldset>
                                                </form>
                                        <td>{{ $element -> code }}</td>
                                        <td>{{ $element -> name }}</td>
                                        <td>{{ $element -> phone_number }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>--}}
                        <div class="modal-footer text-center">
                                    <button type="button" class="btn boton_modalwarning" data-dismiss="modal">Atrás</button>
                                    <button type="submit" class="btn ">Aceptar</button>
                        </div>
                  </div>
              </div>          
            </div>
        </form>
    </div>
    <!-- end modal -->
@stop
@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" /> 
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" /> 
    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.12.4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/Scriptdata.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.print.min.js') }}"></script>
@endsection

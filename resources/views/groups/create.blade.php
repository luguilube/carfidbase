@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
               <div class="sign-in-form-top">
                      <div class="pull-right">                       
                                <a href="{{ route('groups.index') }}" type="button" class="btn btn-warning"> <i class="fa fa-reply" aria-hidden="true"></i> Atrás</a>                                                     
                    </div> 
                </div>
                <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                    <form class="form-horizontal" method="POST" action="{{ route('groups.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Descripción</label>

                            <div class="col-md-6">
                                <textarea id="description" type="text" class="form-control" name="description" value="{{ old('description') }}" autofocus></textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       {{-- <div style="border-style: solid;border-color: #e0ddddb3;">
                            <table id="checkdata" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Todos
                                            <form action="#">
                                                 <input type="checkbox" id="checkAll"/>      
                                                    <fieldset></th>
                                        <th>Código</th>
                                        <th>Nombre</th>
                                        <th>Número de Teléfono</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($clients as $element)
                                    <tr>
                                        <td width="40">
                                            <input id="{{ $element -> id }}" type="checkbox" name="clients[]" class="seleted-check" value="{{ $element -> id }}">
                                        </td>
                                                     </fieldset>
                                                </form>
                                        <td>{{ $element -> code }}</td>
                                        <td>{{ $element -> name }}</td>
                                        <td>{{ $element -> phone_number }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>--}}

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Registrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
               </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" /> 
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" /> 
    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.12.4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/Scriptdata.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.print.min.js') }}"></script>
@endsection

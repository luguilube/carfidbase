<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>{{ config('app.name', 'Control de Acceso') }}</title>
		<!-- Styles -->
		<link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
		<link href="{{ asset('assets/css/style1.css') }}" rel="stylesheet"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
		 <!-- Bootstrap Core CSS -->
		<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel='stylesheet'/>
		<link href="{{ asset('assets/css/font-awesome.css') }}" rel="stylesheet"/>
		<link rel="stylesheet" href="{{ asset('assets/css/icon-font.min.css') }}" />
 		<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" />


	</head>
	<body>
		@yield('content')

		@yield('modals')

		
     	<script src="{{ asset('js/app.js') }}"></script>


		@yield('scripts')

		



		<!-- chart -->
		<script src="{{ asset('assets/js/Chart.js') }}"></script>		<!-- //chart -->
		<script src="{{ asset('assets/js/wow.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/jquery-1.12.4.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/dataTables.buttons.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/buttons.flash.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/pdfmake.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/vfs_fonts.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/buttons.html5.min.js') }}"></script>
	    <script type="text/javascript" src="{{ asset('assets/js/buttons.print.min.js') }}"></script>
	    <script type="text/javascript" src="{{ asset('assets/js/jszip.min.js') }}"></script>
        @yield('scripts')

	  <!-- App scripts -->


		<script type="text/javascript" src="{{ asset('assets/plugins/jquery-validation/lib/jquery-1.11.1.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/plugins/jquery-validation/dist/jquery.validate.js') }}"></script>


        @stack('scripts')

	</body>
</html>

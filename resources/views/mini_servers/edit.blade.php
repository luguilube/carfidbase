@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                <div class="sign-in-form-top">
                    <span>Serve - Editar</span>
                    <div class="pull-right">
                        <a href="{{ route('mini-servers.show', $mini-server->id) }}" type="button" class="btn btn-warning"> <i class="fa fa-reply" aria-hidden="true"></i> Atrás</a>
                    </div>
                   </div>
                <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                    <form class="form-horizontal" method="POST" action="{{ route('mini-servers.update', $mini_server -> id) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group{{ $errors->has('fixed_ip') ? ' has-error' : '' }}">
                            <label for="fixed_ip" class="col-md-4 control-label">Fixed  IP</label>

                            <div class="col-md-6">
                                <input id="fixed_ip" type="text" class="form-control" name="fixed_ip" value="{{ $mini_server->fixed_ip }}" required autofocus>

                                @if ($errors->has('fixed_ip'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fixed_ip') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
                            <label for="user_name" class="col-md-4 control-label">Nick Name</label>

                            <div class="col-md-6">
                                <input id="user_name" type="text" class="form-control" name="user_name" value="{{ $mini_server->user_name }}" autofocus>

                                @if ($errors->has('user_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('port') ? ' has-error' : '' }}">
                            <label for="port" class="col-md-4 control-label">Puerto</label>

                            <div class="col-md-6">
                                <input id="port" type="text" class="form-control" name="port" value="{{ $mini_server->port }}" required autofocus>

                                @if ($errors->has('port'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('port') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Descripción</label>

                            <div class="col-md-6">
                                <textarea id="description" type="text" class="form-control" name="description" autofocus>{{ $mini_server->description }}</textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirmar Contraseña</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Guardar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

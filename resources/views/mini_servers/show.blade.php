@extends('layouts.app')

@section('content')
 <div class="sign-in-wrapper">
    <div class="graphs">
        <div class="sign-in-form">
            <div class="sign-in-form-top">
                <span>Mini Servidor -Ver</span>
				<div class="pull-right text-center">
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</button>
					<a href="{{ route('mini-servers.index') }}" type="button" class="btn btn-warning "><i class="fa fa-reply" aria-hidden="true"></i> Atrás 
					</a>
					<a href="{{ route('mini-servers.edit', $mini_server -> id) }}" type="button" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar
                    </a>
				</div>
			     <br>
            </div>
            <div class="signin">
                <div class="log-input">
    				@include('flash::message')
    				@include('errors')
    				<div class="form-group">
    				  	<label for="amount">Fixed IP: {{ $mini_server -> fixed_ip }}</label>
    				</div>
    			   	<div class="form-group">
    				 	<label for="amount">Nombre de Usuario: {{ $mini_server -> user_name }}</label>
    			   	</div>
    			   	<div class="form-group">
    				 	<label for="amount">Puerto: {{ $mini_server -> port }}</label>
    			   	</div>
    				<div class="form-group">
    				 	<label for="amount">Descripción: {{ $mini_server -> description }}</label>
    			   	</div>
    		 	</div>
	 	     </div>
       </div>
    </div>
</div>
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('mini-servers.destroy', $mini_server->id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Container</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de eliminar el Mini-servidor<b>{{ $mini_server -> name }}</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal"> <i class="fa fa-reply" aria-hidden="true"></i> Atrás</button>
                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Eliminar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- end modal -->
@endsection

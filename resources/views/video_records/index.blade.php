@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
            <div class="graphs">
                <div class="sign-in-form">
                    <div class="sign-in-form-top">
                         <span class="title">DVRs</span>
                            <div class="pull-right">
                            <a href="{{ route('home')}}" class="btn btn-warning" id="atras">
                                <i class="fa fa-reply" aria-hidden="true"></i></a>
                            <a data-toggle="modal" data-target="#modal-add" type="button" class="btn btn-primary " title="Nuevo"> <i class="fa fa-plus-square-o" aria-hidden="true"></i></a>
                          </div>
                    </div>

                    <div class="signin">
                        <div class="log-input">
                @include('flash::message')

                    <table id="companydata" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <th>Nick Name</th>
                            <th>Dirección IP</th>
                            <th>Puerto</th>
                            <th>Nombre de Usuario</th>
                            <th>Opciones</th>
                        </thead>
                        <tbody>

                                @foreach ($video_records as $key => $video_record)
                                    <tr>
                                        <td>{{ $video_record->nick_name }}</td>
                                        <td>{{ $video_record->ip_address }}</td>
                                        <td>{{ $video_record->port }}</td>
                                        <td>{{ $video_record->user_name }}</td>
                                        <td>
                                            <button type="button" class="btn btn-primary " title="Modificar" data-toggle="modal" data-target="#modalEdit{{$key}}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>

                                                     <!-- Modal modificar registro -->
                                                     <div class="modal fade" id="modalEdit{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalEdit{{$key}}">
                                                         <form class="form-horizontal" action="{{ route('video_records.update', $video_record->id) }}" method="POST" enctype="multipart/form-data">

                                                    
                                                             {{ csrf_field() }}
                                                             <input type="hidden" name="_method" value="EDIT">
                                                             <div class="modal-dialog" role="document">
                                                                 <div class="modal-content">
                                                                     <div class="modal-header">
                                                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                        
                                                                         <h4 class="modal-title" id="myModalLabel"  style="color:#007bff">Editar DVRs</h4>
                                                                         
                                                                     </div>  
                                                                        
                                                                        <div class="modal-footer text-center">                             
                                                                         <input type="hidden" name="_method" value="PUT">
                                                                          <div class="form-group{{ $errors->has('nick_name') ? ' has-error' : '' }}">
                                                                                <label for="nick_name" class="col-md-4 control-label">Nick Name</label>

                                                                                <div class="col-md-6">
                                                                                    <input id="nick_name" type="text" class="form-control" name="nick_name" value="{{ $video_record->nick_name }}" autofocus>

                                                                                    @if ($errors->has('nick_name'))
                                                                                        <span class="help-block">
                                                                                            <strong>{{ $errors->first('nick_name') }}</strong>
                                                                                        </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group{{ $errors->has('ip_address') ? ' has-error' : '' }}">
                                                                                <label for="ip_address" class="col-md-4 control-label">Dirección IP</label>

                                                                                <div class="col-md-6">
                                                                                    <input id="ip_address" type="text" class="form-control" name="ip_address" value="{{ $video_record->ip_address }}" required autofocus>

                                                                                    @if ($errors->has('ip_address'))
                                                                                        <span class="help-block">
                                                                                            <strong>{{ $errors->first('ip_address') }}</strong>
                                                                                        </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group{{ $errors->has('port') ? ' has-error' : '' }}">
                                                                                <label for="port" class="col-md-4 control-label">Puerto</label>

                                                                                <div class="col-md-6">
                                                                                    <input id="port" type="text" class="form-control" name="port" value="{{ $video_record->port }}" required autofocus>

                                                                                    @if ($errors->has('port'))
                                                                                        <span class="help-block">
                                                                                            <strong>{{ $errors->first('port') }}</strong>
                                                                                        </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
                                                                                <label for="user_name" class="col-md-4 control-label">Nombre de Usuario</label>

                                                                                <div class="col-md-6">
                                                                                    <input id="user_name" type="text" class="form-control" name="user_name" value="{{ $video_record->user_name }}" required autofocus>

                                                                                    @if ($errors->has('user_name'))
                                                                                        <span class="help-block">
                                                                                            <strong>{{ $errors->first('user_name') }}</strong>
                                                                                        </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                                                <label for="password" class="col-md-4 control-label">Contraseña</label>

                                                                                <div class="col-md-6">
                                                                                    <input id="password" type="password" class="form-control" name="password" required>

                                                                                    @if ($errors->has('password'))
                                                                                        <span class="help-block">
                                                                                            <strong>{{ $errors->first('password') }}</strong>
                                                                                        </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label for="password-confirm" class="col-md-4 control-label">Confirmar Contraseña</label>

                                                                                <div class="col-md-6">
                                                                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                                                                </div>
                        </div>
                                                                                                                                                           
                                                                    </div>  
                                                                    <div style="text-align: center !important; padding: 1%;">                                                                    
                                                                    <button  title="Editar" type="submit" class="btn btn-primary">Actualizar</button>                                                                                                     
                                                                    </div>
                                                               </div>
                                                             </div><!-- end modalcontent -->
                                                            </div><!-- end modaldialog -->
                                                         </form>
                                                     </div>
                                                     <!-- end modal -->


                                                     <button  type="button" class="btn boton_modaldanger" data-toggle="modal" data-target="#modalDelete{{$key}}" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></button>


                                                     <!-- Modal eliminar registro -->
                                                      <div class="modal fade" id="modalDelete{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalDelete{{$key}}">
                                                          <form action="{{ route('video_records.destroy', $video_record->id) }}" method="POST">
                                                              {{ csrf_field() }}
                                                              <input type="hidden" name="_method" value="DELETE">
                                                              <div class="modal-dialog" role="document">
                                                                  <div class="modal-content">
                                                                      <div class="modal-header">
                                                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                          <h4 class="modal-title" id="myModalLabel" style="color:#007bff">Eliminar DVRs</h4>
                                                                          
                                                                       </div>
                                                                      <div class="modal-body">
                                                                          <h5>¿Esta seguro de eliminar DVRs <b>{{ $video_record -> nick_name }}</b>?</h5>
                                                                      </div>
                                                                      <div class="modal-footer text-center">
                                                                          <button type="button" class="btn boton_modalwarning" data-dismiss="modal">Atrás</button>
                                                                          <button type="submit" class="btn boton_modaldanger">Eliminar</button>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </form>
                                                      </div>
                                                      <!-- end modal -->
                                            
                                        </td>
                                    </tr>
                                @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section('modals')
    <!-- Modal agregar nuevo registro -->
    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="modal-add">
        <form class="form-horizontal" method="POST" action="{{ route('antennas.store') }}" enctype="multipart/form-data" }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="ADD">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Agregar DVRs </h4>
                    </div>
                  <div class="modal-body">
                   <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                    <div class="form-group{{ $errors->has('nick_name') ? ' has-error' : '' }}">
                            <label for="nick_name" class="col-md-4 control-label">Nick Name</label>

                            <div class="col-md-6">
                                <input id="nick_name" type="text" class="form-control" name="nick_name" value="{{ old('nick_name') }}" autofocus>

                                @if ($errors->has('nick_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nick_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('ip_address') ? ' has-error' : '' }}">
                            <label for="ip_address" class="col-md-4 control-label">Dirección IP</label>

                            <div class="col-md-6">
                                <input id="ip_address" type="text" class="form-control" name="ip_address" value="{{ old('ip_address') }}" required autofocus>

                                @if ($errors->has('ip_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ip_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('port') ? ' has-error' : '' }}">
                            <label for="port" class="col-md-4 control-label">Puerto</label>

                            <div class="col-md-6">
                                <input id="port" type="text" class="form-control" name="port" value="{{ old('port') }}" required autofocus>

                                @if ($errors->has('port'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('port') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
                            <label for="user_name" class="col-md-4 control-label">Nombre de Usuario</label>

                            <div class="col-md-6">
                                <input id="user_name" type="text" class="form-control" name="user_name" value="{{ old('user_name') }}" required autofocus>

                                @if ($errors->has('user_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirmar Contraseña</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                          <div class="modal-footer text-center">
                                    <button type="button" class="btn boton_modalwarning" data-dismiss="modal">Atrás</button>
                                    <button type="submit" class="btn ">Aceptar</button>
                        </div> 
                     
                  </div>
              </div>          
            </div>
        </form>
    </div>
    <!-- end modal -->
@stop
@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" />
    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.12.4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/Scriptdata.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.print.min.js') }}"></script>
@endsection

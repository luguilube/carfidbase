@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                <div class="sign-in-form-top">
                    <span>Video Grabador - Nuevo</span> 
                    <div class="pull-right">
                        <a href="{{ route('video-records.index') }}" type="button" class="btn btn-warning"> <i class="fa fa-reply" aria-hidden="true"></i> Atrás</a>
                    </div>
                 </div>
                <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                    <form class="form-horizontal" method="POST" action="{{ route('video-records.store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('nick_name') ? ' has-error' : '' }}">
                            <label for="nick_name" class="col-md-4 control-label">Nick Name</label>

                            <div class="col-md-6">
                                <input id="nick_name" type="text" class="form-control" name="nick_name" value="{{ old('nick_name') }}" autofocus>

                                @if ($errors->has('nick_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nick_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('ip_address') ? ' has-error' : '' }}">
                            <label for="ip_address" class="col-md-4 control-label">Dirección IP</label>

                            <div class="col-md-6">
                                <input id="ip_address" type="text" class="form-control" name="ip_address" value="{{ old('ip_address') }}" required autofocus>

                                @if ($errors->has('ip_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ip_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('port') ? ' has-error' : '' }}">
                            <label for="port" class="col-md-4 control-label">Puerto</label>

                            <div class="col-md-6">
                                <input id="port" type="text" class="form-control" name="port" value="{{ old('port') }}" required autofocus>

                                @if ($errors->has('port'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('port') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
                            <label for="user_name" class="col-md-4 control-label">Nombre de Usuario</label>

                            <div class="col-md-6">
                                <input id="user_name" type="text" class="form-control" name="user_name" value="{{ old('user_name') }}" required autofocus>

                                @if ($errors->has('user_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirmar Contraseña</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-check-circle" aria-hidden="true"></i> Aceptar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

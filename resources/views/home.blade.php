@extends('layouts.app')


@section('content')
<style>
.grid-menu {
	  position: fixed;
	  display: flex;
	  /*justify-content: center;*/
	  justify-items: center;

	  top: 0;
	  bottom: 0;
	  left: 0;
	  right: 0;
	 /* background-color: rgba(43, 43, 43, 0.8);*/
	  background: url('./assets/img/images.jpg') no-repeat !important;
	 -webkit-background-size: 100% 100%;  /* Safari 3.0 */
     -moz-background-size: 100% 100%; /* Gecko 1.9.2 (Firefox 3.6) */
     -o-background-size: 100% 100%;  /* Opera 9.5 */
      background-size: 100% 100%;  /* Gecko 2.0 (Firefox 4.0) and other CSS3-compliant browsers */
}

</style>
<section>
	<div class="barramenu">
		<ul class="nofitications-dropdown">
			<li class="login_box">
				<span class="dropdown-toggle title" data-toggle="dropdown" aria-expanded="false" style="color: white;">Control de Acceso</span>
			</li>
		  {{-- <li class="login_box" id="loginContainer">
				<div id="sb-search" class="sb-search">
					<form>
						<input class="sb-search-input" placeholder="Enter your search term..." type="search" id="search">
						<input class="sb-search-submit" type="submit" value="">
						<span class="sb-icon-search"></span>
					</form>
			     </div>
				<!-- search-scripts -->
				<script src="assets/js/classie.js"></script>
				<script src="assets/js/uisearch.js"></script>
				<script>
					new UISearch( document.getElementById( 'sb-search' ) );
				</script>
				<!-- //search-scripts -->
			</li>--}}
		 	<li style="float: right; margin: 1%;">
		 		<a style="color: white;" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();"><i class="fa fa-power-off "></i></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
        		</form>
        	</li>
		</ul>
	</div>
   <div class="grid-menu transition-menu">
	 		<div class="grid-content">
				@include('flash::message')
	 			@foreach ($options as $element)
					<a class="grid-item" href="{{route($element->url)}}" >
						<div class="grid-item-icon">
							@if (!empty($element->icon))
								<img class="user-menu" src="{{ Storage::disk('options') -> url($element->icon) }}" alt="">
							@else
								<img class="user-menu" src="{{ Storage::disk('options') -> url('0.ico') }}" alt="">
							@endif
						</div>
						<span class="grid-item-title">
							@if (strcmp($element->title, 'vehiculos') === 0)
								@if (session('company')->system_vehicle_name != null)
									{{session('company')->system_vehicle_name}}
								@else
									{{$element->title}}
								@endif
							@elseif (strcmp($element->title, 'contenedores') === 0)
								@if (session('company')->system_container_name != null)
									{{session('company')->system_container_name}}
								@else
									{{$element->title}}
								@endif
							@else
								{{$element->title}}
							@endif
					</span>
	  			</a>
			@endforeach
    	</div>
	</div>
	<script src="{{ asset('js/app.js') }}"></script>
</section>
@endsection

@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                <div class="sign-in-form-top">
                    <span>Camara - Nuevo</span> 
                    <div class="pull-right">                       
                        <a href="{{ route('cameras.index') }}" type="button" class="btn btn-warning"> <i class="fa fa-reply" aria-hidden="true"></i>Atras</a>                           
                    </div>                   
                 </div>
                <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                    <form class="form-horizontal" method="POST" action="{{ route('cameras.store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <input type="hidden" name="total_positions" value="{{$total_positions}}">

                        <div class="form-group{{ $errors->has('video_record_id') ? ' has-error' : '' }}">
                            <label for="video_record_id" class="col-md-4 control-label">DVR</label>

                            <div class="col-md-6">
                                <select id="video_record_id" name="video_record_id" class="form-control" required="required">
                                    <option value="">Seleccione un DVR...</option>
                                    @foreach($video_records as $element)
                                        <option value="{{ $element->id }}">{{ $element -> nick_name }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('video_record_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('video_record_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                            <label for="code" class="col-md-4 control-label">Código</label>

                            <div class="col-md-6">
                                <input id="code" type="text" class="form-control" name="code" value="{{ old('code') }}" required autofocus>

                                @if ($errors->has('code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Descripción</label>

                            <div class="col-md-6">
                                <textarea id="description" type="text" class="form-control" name="description" value="{{ old('description') }}" autofocus></textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                            <label for="position" class="col-md-4 control-label">Posición</label>

                            <div class="col-md-6">
                                <select id="position" name="position" class="form-control" required="required">
                                    @for ($i=1; $i <= $total_positions; $i++)
                                        <option value="{{ $i }}"
                                        @if ($i == $total_positions)
                                            selected="selected"
                                        @endif>
                                        {{ $i }}</option>
                                    @endfor
                                </select>

                                @if ($errors->has('position'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Aceptar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

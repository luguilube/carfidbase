@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
    <div class="graphs">
        <div class="sign-in-form">

            <div class="sign-in-form-top">                
                <span class="title">Camaras</span>
                    <div class="pull-right">                       
                        <a href="{{ route('home')}}" class="btn btn-warning" id="atras"><i class="fa fa-reply" aria-hidden="true"></i></a>
                        <a data-toggle="modal" data-target="#modal-add" type="button" class="btn btn-primary " title="Nuevo"> <i class="fa fa-plus-square-o" aria-hidden="true"></i></a>                 

                    </div>
                </div>
                <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    
                    <table id="companydata" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <th>ID</th>
                            <th>Descripción</th>
                            <th>Codigo</th>
                            <th>Estatus</th>
                            <th>VideoRecord</th>
                            <th>Posición</th>
                            <th>Usuario creador</th>
                            <th>Creación</th>
                            <th>Actualización</th>
                            <th>Opciones</th>
                        </thead>
                        <tbody>
                     
                            @foreach ($cameras as $key => $camera)
                                <tr>
                                    <td>{{ $camera->id }}</td>
                                    <td>{{ $camera->description }}</td>
                                    <td>{{ $camera->code }}</td>
                                    <td>
                                       {{-- @if ($camera->enable == 1)
                                            Activa
                                        @else
                                            Inactiva
                                        @endif--}}
                                    </td>
                                    <td></td>
                                    <td>{{ $camera->position }}</td>
                                    <td>{{ $camera->creator_user }}</td>
                                    <td>{{ $camera->created_at }}</td>
                                    <td>{{ $camera->updated_at}}</td>
                                    <td>
                                        <button type="button" class="btn btn-primary " title="Modificar" data-toggle="modal" data-target="#modalEdit{{$key}}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>

                                                     <!-- Modal modificar registro -->
                                                     <div class="modal fade" id="modalEdit{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalEdit{{$key}}">
                                                         <form class="form-horizontal" action="{{ route('cameras.update', $camera->id) }}" method="POST" enctype="multipart/form-data">

                                                    
                                                             {{ csrf_field() }}
                                                             <input type="hidden" name="_method" value="EDIT">
                                                             <div class="modal-dialog" role="document">
                                                                 <div class="modal-content">
                                                                     <div class="modal-header">
                                                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                        
                                                                         <h4 class="modal-title" id="myModalLabel"  style="color:#007bff">Editar Camara</h4>
                                                                         
                                                                     </div>  
                                                                        
                                                                        <div class="modal-footer text-center">                             
                                                                         <input type="hidden" name="_method" value="PUT">
                                                                         {{-- <div class="{{ $errors->has('video_record_id') ? ' has-error' : '' }}">
                                                                                <label for="video_record_id" class="col-md-4 control-label">DVR</label>

                                                                                <div class="col-md-6">
                                                                                    <select id="video_record_id" name="video_record_id" class="form-control" required="required">
                                                                                        <option value="">Seleccione un DVR...</option>
                                                                                        @foreach($video_records as $element)
                                                                                            <option value="{{ $element->id }}"
                                                                                                @if ($element->id == $camera->video_record_id)
                                                                                                    selected="selected"
                                                                                                @endif
                                                                                                >{{ $element -> nick_name }}
                                                                                            </option>
                                                                                        @endforeach
                                                                                    </select>

                                                                                    @if ($errors->has('video_record_id'))
                                                                                        <span class="help-block">
                                                                                            <strong>{{ $errors->first('video_record_id') }}</strong>
                                                                                        </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>--}}

                                                                            <div class="{{ $errors->has('code') ? ' has-error' : '' }}">
                                                                                <label for="code" class="col-md-4 control-label">Código</label>

                                                                                <div class="col-md-6">
                                                                                    <input id="code" type="text" class="form-control" name="code" value="{{ $camera->code }}" required autofocus>

                                                                                    @if ($errors->has('code'))
                                                                                        <span class="help-block">
                                                                                            <strong>{{ $errors->first('code') }}</strong>
                                                                                        </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>

                                                                            <div class="{{ $errors->has('enable') ? ' has-error' : '' }}">
                                                                                <label for="enable" class="col-md-4 control-label">Estatus</label>

                                                                                <div class="col-md-6">
                                                                                    <select id="enable" name="enable" class="form-control" required="required">
                                                                                        <option value="">Seleccione un eenable...</option>
                                                                                        <option value="1"
                                                                                            @if ($camera->enable == 1)
                                                                                                selected="selected"
                                                                                            @endif>Activo
                                                                                        </option>
                                                                                        <option value="0"
                                                                                            @if ($camera->enable == 0)
                                                                                                selected="selected"
                                                                                            @endif>Inactivo
                                                                                        </option>
                                                                                    </select>

                                                                                    @if ($errors->has('enable'))
                                                                                        <span class="help-block">
                                                                                            <strong>{{ $errors->first('enable') }}</strong>
                                                                                        </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>

                                                                            <div class="{{ $errors->has('description') ? ' has-error' : '' }}">
                                                                                <label for="description" class="col-md-4 control-label">Descripción</label>

                                                                                <div class="col-md-6">
                                                                                    <textarea id="description" type="text" class="form-control" name="description"autofocus>{{ $camera->description }}</textarea>

                                                                                    @if ($errors->has('description'))
                                                                                        <span class="help-block">
                                                                                            <strong>{{ $errors->first('description') }}</strong>
                                                                                        </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>

                                                                          {{--  <div class="{{ $errors->has('position') ? ' has-error' : '' }}">
                                                                                <label for="position" class="col-md-4 control-label">Posición</label>

                                                                                <div class="col-md-6">
                                                                                    <select id="position" name="position" class="form-control" required="required">
                                                                                        @for ($i=1; $i <= $total_positions; $i++)
                                                                                            <option value="{{ $i }}"
                                                                                            @if ($i == $camera->position)
                                                                                                selected="selected"
                                                                                            @endif>
                                                                                            {{ $i }}</option>
                                                                                        @endfor
                                                                                    </select>

                                                                                    @if ($errors->has('position'))
                                                                                        <span class="help-block">
                                                                                            <strong>{{ $errors->first('position') }}</strong>
                                                                                        </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>--}}
                                                                                                                                                           
                                                                    </div>  
                                                                    <div style="text-align: center !important; padding: 1%;">                                                                    
                                                                    <button  title="Editar" type="submit" class="btn btn-primary">Actualizar</button>                                                                                                     
                                                                    </div>
                                                               </div>
                                                             </div><!-- end modalcontent -->
                                                            </div><!-- end modaldialog -->
                                                         </form>
                                                     </div>
                                                     <!-- end modal -->


                                                     <button  type="button" class="btn boton_modaldanger" data-toggle="modal" data-target="#modalDelete{{$key}}" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></button>


                                                     <!-- Modal eliminar registro -->
                                                      <div class="modal fade" id="modalDelete{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalDelete{{$key}}">
                                                          <form action="{{ route('cameras.destroy', $camera->id) }}" method="POST">
                                                              {{ csrf_field() }}
                                                              <input type="hidden" name="_method" value="DELETE">
                                                              <div class="modal-dialog" role="document">
                                                                  <div class="modal-content">
                                                                      <div class="modal-header">
                                                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                          <h4 class="modal-title" id="myModalLabel" style="color:#007bff">Eliminar camara</h4>
                                                                          
                                                                       </div>
                                                                      <div class="modal-body">
                                                                          <h5>¿Esta seguro de eliminar la camara <b>{{ $camera -> id }}</b>?</h5>
                                                                      </div>
                                                                      <div class="modal-footer text-center">
                                                                          <button type="button" class="btn boton_modalwarning" data-dismiss="modal">Atrás</button>
                                                                          <button type="submit" class="btn boton_modaldanger">Eliminar</button>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </form>
                                                      </div>
                                                      <!-- end modal -->
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                  <h5 class="text-center"><b>Operaciones</b></h5>
                    <hr>
                    <div>
                        @if (!empty($cameras[0]->id))
                            <ul style="list-style:none;">
                            @foreach ($cameras as $element)
                                @if ($element-> enable==1)

                                        <li class="col-lg-12 col-md-6"><b># Camara</b> {{$element->code}}</li>
                                        <li class=" col-lg-12  col-md-6">
                                    <img class=" img-thumbnail col-md-6" src="data:image/jpeg;base64,{!!$element->thumb_image!!}" alt="prueba">
                                </li>
                 
                                @endif
                            @endforeach
                        </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('modals')
    <!-- Modal agregar nuevo registro -->
    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="modal-add">
        <form class="form-horizontal" method="POST" action="{{ route('cameras.store') }}" enctype="multipart/form-data" }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="ADD">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Agregar camara</h4>
                    </div>
                  <div class="modal-body">
                   <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                     {{--<div class="form-group{{ $errors->has('video_record_id') ? ' has-error' : '' }}">
                            <label for="video_record_id" class="col-md-4 control-label">DVR</label>

                            <div class="col-md-6">
                                <select id="video_record_id" name="video_record_id" class="form-control" required="required">
                                    <option value="">Seleccione un DVR...</option>
                                    @foreach($video_records as $element)
                                        <option value="{{ $element->id }}">{{ $element -> nick_name }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('video_record_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('video_record_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>--}}

                        <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                            <label for="code" class="col-md-4 control-label">Código</label>

                            <div class="col-md-6">
                                <input id="code" type="text" class="form-control" name="code" value="{{ old('code') }}" required autofocus>

                                @if ($errors->has('code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Descripción</label>

                            <div class="col-md-6">
                                <textarea id="description" type="text" class="form-control" name="description" value="{{ old('description') }}" autofocus></textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       {{-- <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                            <label for="position" class="col-md-4 control-label">Posición</label>

                            <div class="col-md-6">
                                <select id="position" name="position" class="form-control" required="required">
                                    @for ($i=1; $i <= $total_positions; $i++)
                                        <option value="{{ $i }}"
                                        @if ($i == $total_positions)
                                            selected="selected"
                                        @endif>
                                        {{ $i }}</option>
                                    @endfor
                                </select>

                                @if ($errors->has('position'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>--}}
                          <div class="modal-footer text-center">
                                    <button type="button" class="btn boton_modalwarning" data-dismiss="modal">Atrás</button>
                                    <button type="submit" class="btn ">Aceptar</button>
                        </div>
                     
                  </div>
              </div>          
            </div>
        </form>
    </div>
    <!-- end modal -->
@stop
@section('scripts')
  <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>
        <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" />
        <script type="text/javascript" src="{{ asset('assets/js/jquery-1.12.4.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/Scriptdata.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/dataTables.buttons.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/buttons.flash.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/pdfmake.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/vfs_fonts.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/buttons.html5.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/buttons.print.min.js') }}"></script>
@endsection

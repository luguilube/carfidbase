@extends('layouts.app')

@section('content')
 <div class="sign-in-wrapper">
    <div class="graphs">
        <div class="sign-in-form">
            <div class="sign-in-form-top">
                <span>Camara - Ver</span>
				<div class="pull-right ">
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</button>
					<a href="{{ route('cameras.index') }}" type="button" class="btn btn-warning">
						<i class="fa fa-reply" aria-hidden="true"></i> Atrás
					</a>
					<a href="{{ route('cameras.edit', $camera -> id) }}" type="button" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
				</div>
            </div>
          	<div class="signin">
	            <div class="log-input">
					@include('flash::message')
					@include('errors')
					<div class="form-group">
					  	<label for="amount">ID: {{ $camera -> id }}</label>
					</div>
				   	<div class="form-group">
					 	<label for="amount">Dirección IP: {{ $camera -> ip_address }}</label>
				   	</div>
				   	<div class="form-group">
					 	<label for="amount">Código: {{ $camera -> code }}</label>
				   	</div>
					<div class="form-group">
					 	<label for="amount">Descripción: {{ $camera -> description }}</label>
				   	</div>
					<div class="form-group">
					 	<label for="amount">Estatus:
							@if ($camera->enable == 1)
								Activa
							@else
								Inactiva
							@endif
						</label>
				   	</div>
					<div class="form-group">
					 	<label for="amount">Posición: {{ $camera -> position }}</label>
				   	</div>
					<div class="form-group">
						<img class="image-thumbnail" src="data:image/jpeg;base64,{!!$camera->thumb_image!!}" alt="prueba">
				   	</div>
			 	</div>
	 		</div>
		</div>
	</div>
</div>
@stop
@section('modals')
<!-- Modal -->
<!-- MODAL PARA ELIMINAR REGISTRO -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
    <form action="{{ route('cameras.destroy', $camera -> id) }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="modal-delete" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Eliminar Camara</h4>
                </div>
                <div class="modal-body">
                    <p>¿Esta seguro de eliminar la Camara <b>{{ $camera -> name}}</b>?</p>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- END Modal  -->
<!-- END MODAL PARA ELIMINAR REGISTRO -->
@endsection

@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
	<div class="graphs">
		<div class="sign-in-form">
			<div class="sign-in-form-top">
				<span>Usuario - Ver</span>
				<div class="pull-right">
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar </button>
					<a href="{{ route('users.index') }}" type="button" class="btn btn-warning">
					 	<i class="fa fa-reply" aria-hidden="true"></i> Atrás</a>
					<a href="{{ route('users.edit', $user -> id) }}" type="button" class="btn btn-primary"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
				</div>
			</div>
			<div class="signin">
				<ul class="log-input panel-list" style="list-style:none">
					@include('flash::message')
				   	<li class="row">
				   		<div class="col-md-6">
							<p  class="text-capitalize"><b>Nombre</b> {{ $user -> name }}</p>
						</div>
				   		<div class="col-md-6">
							<p><b>Correo</b> {{ $user -> email }}</p>
						</div>
				   	</li>
				   	<li class="row">
				   		<div class="col-md-12">
							<p><b>Teléfono</b> {{ $user -> phone_number }}</p>
						</div>
				   	</li>
					<div class="panel-heading clearfix">
				   		<h5 class="row"><b>Roles asignados</b></h5>
				   </div>
				   @foreach($user->roles as $element)
					   <div>
						 <p for="amount" class="text-capitalize">* {{ $element -> name }}</p>
					   </div>
				   @endforeach
		 		</ul>
 			</div>
	 	</div>
	</div>
</div>
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('users.destroy', $user->id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Usuario</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de eliminar el usuario <b class="text-capitalize">{{ $user -> name }}</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- end modal -->
@endsection

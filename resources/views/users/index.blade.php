@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
    <div class="graphs">
        <div class="sign-in-form">
              <div class="sign-in-form-top">
                    <span class="title">Usuarios</span>
                    <div class="pull-right">
                        <a href="{{ route('home')}}" class="btn btn-warning" id="atras"><i class="fa fa-reply" aria-hidden="true"></i></a> 
                        <a data-toggle="modal" data-target="#modal-add" type="button" class="btn btn-primary " title="Nuevo"> <i class="fa fa-plus-square-o" aria-hidden="true"></i></a>
                   
                    </div>
                </div>
                <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                        <table id="companydata" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <th>Nombre</th>
                                <th>Número de Teléfono</th>
                                <th>Opciones</th>
                            </thead>
                            <tbody>
                               
                                    @foreach ($users as $key =>  $user)
                                        <tr>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->phone_number }}</td>
                                            <td>
                                                <button type="button" class="btn btn-primary " title="Modificar" data-toggle="modal" data-target="#modalEdit{{$key}}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>

                                                     <!-- Modal modificar registro -->
                                                     <div class="modal fade" id="modalEdit{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalEdit{{$key}}">
                                                         <form class="form-horizontal" action="{{ route('users.update', $user->id) }}" method="POST" enctype="multipart/form-data">

                                                    
                                                             {{ csrf_field() }}
                                                             <input type="hidden" name="_method" value="EDIT">
                                                             <div class="modal-dialog" role="document">
                                                                 <div class="modal-content">
                                                                     <div class="modal-header">
                                                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                        
                                                                         <h4 class="modal-title" id="myModalLabel"  style="color:#007bff">Editar Usuario</h4>
                                                                         
                                                                     </div>  
                                                                        
                                                                        <div class="modal-footer text-center">                             
                                                                         <input type="hidden" name="_method" value="PUT">
                                                                         <div class="{{ $errors->has('name') ? ' has-error' : '' }}">
                                                                                <label for="name" class="col-md-4 control-label">Nombre</label>

                                                                                <div class="col-md-6">
                                                                                    <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" required autofocus>

                                                                                    @if ($errors->has('name'))
                                                                                        <span class="help-block">
                                                                                            <strong>{{ $errors->first('name') }}</strong>
                                                                                        </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>

                                                                            <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                                                                                <label for="email" class="col-md-4 control-label">Correo</label>

                                                                                <div class="col-md-6">
                                                                                    <input id="email" type="text" class="form-control" name="email" value="{{ $user->email }}"  autofocus>

                                                                                    @if ($errors->has('email'))
                                                                                        <span class="help-block">
                                                                                            <strong>{{ $errors->first('email') }}</strong>
                                                                                        </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>

                                                                            <div class="{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                                                                <label for="phone_number" class="col-md-4 control-label">Número de Teléfono</label>

                                                                                <div class="col-md-6">
                                                                                    <input id="phone_number" type="text" class="form-control" name="phone_number" value="{{ $user->phone_number }}" required autofocus>

                                                                                    @if ($errors->has('phone_number'))
                                                                                        <span class="help-block">
                                                                                            <strong>{{ $errors->first('phone_number') }}</strong>
                                                                                        </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>

                                                                          {{-- <div class="form-group"  style="     border-style: solid;
                                                                                 border-color: #e0ddddb3;">
                                                                                <label>Lista de Roles</label>
                                                                                @foreach ($roles as $element)
                                                                                    <div class="input-group float-box-l">
                                                                                        <input id="{{ $element -> name }}" name="roles[]"
                                                                                        @if (in_array($element -> id, array_column($user->roles->toArray(), 'id')))
                                                                                            checked="checked"
                                                                                        @endif
                                                                                        type="checkbox" value="{{ $element -> id }}">
                                                                                        <label class="text-capitalize left " for="{{ $element -> name }}"> {{ $element -> name }}</label>
                                                                                    </div>
                                                                                @endforeach
                                                                            </div>

                                                                            <div class="table-responsive"  style="border-style: solid;   border-color: #e0ddddb3; padding: 5%;">
                                                                                <table  class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Todos <form action="#">
                                                                                            <input type="checkbox" id="checkAll"/>      
                                                                                                <fieldset</th>
                                                                                            <th>Compañias</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    @foreach($companies as $element)
                                                                                        <tr>
                                                                                            <td width="40">
                                                                                                <input id="{{ $element -> id }}" type="checkbox" name="companies[]"
                                                                                                @if (in_array($element -> id, array_column($user->company->toArray(), 'id')))
                                                                                                    checked="checked"
                                                                                                @endif
                                                                                                 class="seleted-check" value="{{ $element -> id }}">
                                                                                            </td>
                                                                                            </fieldset>
                                                                                          </form>
                                                                                            <td>{{ $element -> name }}</td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                    </tbody>
                                                                                </table>
                                                                               </div>--}} 


                                                                        </div>                    
                                                                     
                                                                    <div style="text-align: center !important; padding: 1%;">                                                                   
                                                                    <button  title="Editar" type="submit" class="btn btn-primary">Actualizar</button>
                                                                 </div>                                                                                       
                                                              </div>
                                                             </div><!-- end modalcontent -->
                                                            </div><!-- end modaldialog -->
                                                         </form>
                                                     </div>
                                                     <!-- end modal -->


                                                     <button  type="button" class="btn boton_modaldanger" data-toggle="modal" data-target="#modalDelete{{$key}}" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></button>


                                                     <!-- Modal eliminar registro -->
                                                      <div class="modal fade" id="modalDelete{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalDelete{{$key}}">
                                                          <form action="{{ route('users.destroy', $user->id) }}" method="POST">
                                                              {{ csrf_field() }}
                                                              <input type="hidden" name="_method" value="DELETE">
                                                              <div class="modal-dialog" role="document">
                                                                  <div class="modal-content">
                                                                      <div class="modal-header">
                                                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                          <h4 class="modal-title" id="myModalLabel" style="color:#007bff">Eliminar Usuario</h4>
                                                                          
                                                                       </div>
                                                                      <div class="modal-body">
                                                                          <h5>¿Esta seguro de eliminar el compañia <b>{{ $user -> name }}</b>?</h5>
                                                                      </div>
                                                                      <div class="modal-footer text-center">
                                                                          <button type="button" class="btn boton_modalwarning" data-dismiss="modal">Atrás</button>
                                                                          <button type="submit" class="btn boton_modaldanger">Eliminar</button>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </form>
                                                      </div>
                                                      <!-- end modal -->
                                            </td>
                                        </tr>
                                    @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
@stop
@section('modals')
    <!-- Modal agregar nuevo registro -->
    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="modal-add">
        <form class="form-horizontal" method="POST" action="{{ route('users.store') }}">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="ADD">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Agregar Usuario</h4>
                    </div>
                  <div class="modal-body">
                   <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Correo</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                            <label for="phone_number" class="col-md-4 control-label">Número de Teléfono</label>

                            <div class="col-md-6">
                                <input id="phone_number" type="text" class="form-control" name="phone_number" value="{{ old('phone_number') }}" required autofocus>

                                @if ($errors->has('phone_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                     {{--<div class="table-responsive" style="border-style: solid;border-color: #e0ddddb3;                                                               margin: 5%;
                                                                 padding: 5%;">
                            <table  class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th> Todos <form action="#">
                                        <input type="checkbox" id="checkAll"/>      
                                            <fieldset></th>
                                        <th>Compañias</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($companies as $element)
                                    <tr>
                                        <td width="40">
                                            <input id="{{ $element -> id }}" type="checkbox" name="companies[]" class="seleted-check" value="{{ $element -> id }}">
                                        </td>
                                        </fieldset>
                                      </form>
                                        <td>{{ $element -> name }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>--}} 

                        

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Aceptar
                                </button>
                            </div>
                        </div>
                     
                  </div>
              </div>          
            </div>
        </form>
    </div>
    <!-- end modal -->
@stop
@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" /> 
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" /> 
    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.12.4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/Scriptdata.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.print.min.js') }}"></script>
@endsection

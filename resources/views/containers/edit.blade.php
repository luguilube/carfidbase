@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                <div class="sign-in-form-top">
                    <span>Containers - Editar</span>
                    <div class="pull-right">
                        <a href="{{ route('containers.show', $container->id) }}" type="button" class="btn btn-warning"> <i class="fa fa-reply" aria-hidden="true"></i> Atrás</a>
                    </div>
                   </div>
                <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                    <form class="form-horizontal" method="POST" action="{{ route('containers.update', $container -> id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group{{ $errors->has('vehicle_id') ? ' has-error' : '' }}">
                            <label for="vehicle_id" class="col-md-4 control-label">{{session('company')->system_vehicle_name }}</label>

                            <div class="col-md-6">
                                <select id="vehicle_id" name="vehicle_id" class="form-control" required="required">
                                    <option value="">Seleccione un {{ session('company')->system_vehicle_name }}...</option>
                                    @foreach($vehicles as $vehicle)
                                        <option value="{{ $vehicle->id }}"
                                            @if ($container->vehicle_id = $vehicle->id)
                                                selected="selected"
                                            @endif>
                                            {{ $vehicle -> plate_number }}
                                        </option>
                                    @endforeach
                                </select>

                                @if ($errors->has('vehicle_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('vehicle_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('plate_number') ? ' has-error' : '' }}">
                            <label for="plate_number" class="col-md-4 control-label">Número de Placa del {{session('company')->system_vehicle_name }}</label>

                            <div class="col-md-6">
                                <input id="plate_number" type="text" class="form-control" name="plate_number" value="{{ $container->plate_number }}" required autofocus>

                                @if ($errors->has('plate_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('plate_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('tag') ? ' has-error' : '' }}">
                            <label for="tag" class="col-md-4 control-label">Tag</label>

                            <div class="col-md-6">
                                <input id="tag" type="text" class="form-control" name="tag" value="{{ $container->tag }}" required autofocus>

                                @if ($errors->has('tag'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tag') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="image" class="col-md-4 control-label">Fotos: {{ session('company')->system_vehicle_name }}</label>

                            <div class="col-md-6">
                                <input id="image" type="file" class="form-control" name="image" required>

                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('height_container') ? ' has-error' : '' }}">
                            <label for="height_container" class="col-md-4 control-label">Altura del {{ session('company')->system_container_name }}</label>

                            <div class="col-md-6">
                                <input id="height_container" type="number" class="form-control" name="height_container" value="{{ $container->height_container }}" onchange="sumarcontainer(this.value);" required autofocus>

                                @if ($errors->has('height_container'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('height_container') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('width_container') ? ' has-error' : '' }}">
                            <label for="width_container" class="col-md-4 control-label">Anchura del {{ session('company')->system_container_name }}</label>

                            <div class="col-md-6">
                                <input id="width_container" type="number" class="form-control" name="width_container" value="{{ $container->width_container }}" onchange="sumarcontainer(this.value);" required autofocus>

                                @if ($errors->has('width_container'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('width_container') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('long_container') ? ' has-error' : '' }}">
                            <label for="long_container" class="col-md-4 control-label">Largo del {{ session('company')->system_container_name }}</label>

                            <div class="col-md-6">
                                <input id="long_container" type="number" class="form-control" name="long_container" value="{{ $container->long_container }}" onchange="sumarcontainer(this.value);" required autofocus>

                                @if ($errors->has('long_container'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('long_container') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <span>El resultado es: </span> <span id="total_container1" class="total_container1"></span>
                        <input id="total_container" type="hidden" name="total_container" onchange="sumaryarda(this.value);" >

                        <div class="form-group{{ $errors->has('height_piston') ? ' has-error' : '' }}">
                            <label for="height_piston" class="col-md-4 control-label">Altura del Piston</label>

                            <div class="col-md-6">
                                <input id="height_piston" type="number" class="form-control" name="height_piston" value="{{ $container->height_piston }}" onchange="sumarpiston(this.value);" required autofocus>

                                @if ($errors->has('height_piston'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('height_piston') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('width_piston') ? ' has-error' : '' }}">
                            <label for="width_piston" class="col-md-4 control-label">Anchura del Piston</label>

                            <div class="col-md-6">
                                <input id="width_piston" type="number" class="form-control" name="width_piston" value="{{ $container->width_piston }}" onchange="sumarpiston(this.value);" required autofocus>

                                @if ($errors->has('width_piston'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('width_piston') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('long_piston') ? ' has-error' : '' }}">
                            <label for="long_piston" class="col-md-4 control-label">Largo del Piston</label>

                            <div class="col-md-6">
                                <input id="long_piston" type="number" class="form-control" name="long_piston" value="{{ $container->long_piston }}" onchange="sumarpiston(this.value);" required autofocus>

                                @if ($errors->has('long_piston'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('long_piston') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <span>El resultado es: </span> <span id="total_piston1" class="total_piston1"></span>
                        <input id="total_piston" type="hidden" name="total_piston" onchange="sumaryarda(this.value);" >

                        <div class="form-group{{ $errors->has('height_extension') ? ' has-error' : '' }}">
                            <label for="height_extension" class="col-md-4 control-label">Altura de la Extensión</label>

                            <div class="col-md-6">
                                <input id="height_extension" type="number" class="form-control" name="height_extension" value="{{ $container->height_extension }}" onchange="sumarextension(this.value);" required autofocus>

                                @if ($errors->has('height_extension'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('height_extension') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('width_extension') ? ' has-error' : '' }}">
                            <label for="width_extension" class="col-md-4 control-label">Anchura de la Extensión</label>

                            <div class="col-md-6">
                                <input id="width_extension" type="number" class="form-control" name="width_extension" value="{{ $container->width_extension }}" onchange="sumarextension(this.value);" required autofocus>

                                @if ($errors->has('width_extension'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('width_extension') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('long_extension') ? ' has-error' : '' }}">
                            <label for="long_extension" class="col-md-4 control-label">Largo de la Extensión</label>

                            <div class="col-md-6">
                                <input id="long_extension" type="number" class="form-control" name="long_extension" value="{{ $container->long_extension }}" onchange="sumarextension(this.value);" required autofocus>

                                @if ($errors->has('long_extension'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('long_extension') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <span>El resultado es: </span> <span id="total_extension1" class="total_extension1"></span>
                        <input id="total_extension" type="hidden" name="total_extension" onchange="sumaryarda(this.value);" >

                        <div class="form-group{{ $errors->has('full_payment') ? ' has-error' : '' }}">
                            <label for="full_payment" class="col-md-4 control-label">Precio total</label>

                            <div class="col-md-6">
                                <input id="full_payment" type="text" class="form-control" name="full_payment" value="{{ $container->full_payment }}" required autofocus>

                                @if ($errors->has('full_payment'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('full_payment') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <span>Total de la Yarda: </span> <span id="yards1" class="yards1"></span>
                        <input id="yards" type="hidden" name="yards">

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Aceptar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
<script type="text/javascript" src="{{ asset('assets/js/jquery-3.2.1.min.js') }}"></script>
<script type="text/javascript">
// $(document).ready(function(){
    function sumarcontainer (valor) {
        var total = 0;
        valor = parseInt(valor); // Convertir el valor a un entero (número).

        total = document.getElementById('total_container1').innerHTML;

        // Aquí valido si hay un valor previo, si no hay datos, le pongo un cero "0".
        total = (total == null || total == undefined || total == "") ? 1 : total;

        /* Esta es la suma. */
        total = (parseInt(total) * parseInt(valor));
        total = Math.abs(total);
        // Colocar el resultado de la suma en el control "span".
        document.getElementById('total_container1').innerHTML = total;
        $('#total_container').val(total);
    }
     function sumarpiston (valor) {
        var total = 0;
        valor = parseInt(valor); // Convertir el valor a un entero (número).

        total = document.getElementById('total_piston1').innerHTML;

        // Aquí valido si hay un valor previo, si no hay datos, le pongo un cero "0".
        total = (total == null || total == undefined || total == "") ? 1 : total;

        /* Esta es la suma. */
        total = (parseInt(total) * parseInt(valor));
        total = Math.abs(total);
        // Colocar el resultado de la suma en el control "span".
        document.getElementById('total_piston1').innerHTML = total;
        $('#total_piston').val(total);
    }
     function sumarextension (valor) {
        var total = 0;
        valor = parseInt(valor); // Convertir el valor a un entero (número).

        total = document.getElementById('total_extension1').innerHTML;

        // Aquí valido si hay un valor previo, si no hay datos, le pongo un cero "0".
        total = (total == null || total == undefined || total == "") ? 1 : total;

        /* Esta es la suma. */
        total = (parseInt(total) * parseInt(valor));
        total = Math.abs(total);
        // Colocar el resultado de la suma en el control "span".
        document.getElementById('total_extension1').innerHTML = total;
        $('#total_extension').val(total);

        this.sumaryarda();
    }

    function sumaryarda () {
        var total = 0;

        total_c = $('#total_container').val();
        total_p = $('#total_piston').val();
        total_e = $('#total_extension').val();

        // Aquí valido si hay un valor previo, si no hay datos, le pongo un cero "0".
        total_c = (total_c == null || total_c == undefined || total_c == "") ? 0 : total_c;
        total_p = (total_p == null || total_p == undefined || total_p == "") ? 0 : total_p;
        total_e = (total_e == null || total_e == undefined || total_e == "") ? 0 : total_e;

        /* Esta es la suma. */
        total = (parseInt(total_c) + parseInt(total_e)) - parseInt(total_p);

        // Colocar el resultado de la suma en el control "span".
        document.getElementById('yards1').innerHTML = total;
        $('#yards').val(total);
    }
// });
</script>
@endsection

@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                 <div class="sign-in-form-top">
                      <span>Containers</span>
                    <div class="pull-right">
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar </button>
                        <a href="{{ route('containers.index') }}" type="button" class="btn btn-warning"> <i class="fa fa-share" aria-hidden="true"></i> Atrás</a>
                        <a href="{{ route('containers.edit', $container -> id) }}" type="button" class="btn btn-primary "> <i class="fa fa-pencil-square-o" aria-hidden="true"></i>Editar</a>
                    </div>
                </div>


          <div class="signin">
              @include('flash::message')
              @include('errors')
            <div class="log-input">
                <div class="panel-body row">
                     <ul class="panel-list">
                        <li class="row">
                            <div class="col-md-4">
                                <p><b>Código</b></p>
                                <p>{{ $container -> id }}</p>
                            </div>
                            <div class="col-md-4">
                                <p><b>TAG</b></p>
                                <p class="text-capitalize">{{ $container -> tag }}</p>
                            </div>
                            <div class="col-md-4">
                                <p><b># Placa</b></p>
                                <p>{{ $container -> plate_number }}</p>
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-md-12">
                                <p><b>Yardas</b></p>
                                <p>{{ $container -> yards }}</p>
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-md-4">
                                <p><b>Ancho container</b></p>
                                <p>{{ $container -> width_container}}</p>
                            </div>
                            <div class="col-md-4">
                                <p><b>Alto container</b></p>
                                <p>{{ $container -> height_container}}</p>
                            </div>
                            <div class="col-md-4">
                                <p><b>Largo container</b></p>
                                <p>{{ $container -> long_container}}</p>
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-md-4">
                                <p><b>Ancho pistón</b></p>
                                <p>{{ $container -> width_piston}}</p>
                            </div>
                            <div class="col-md-4">
                                <p><b>Alto pistón</b></p>
                                <p class="text-capitalize">{{ $container -> height_piston}}</p>
                            </div>
                            <div class="col-md-4">
                                <p><b>Largo pistón</b></p>
                                <p class="text-capitalize">{{ $container -> long_piston}}</p>
                            </div>
                        </li>
                         <li class="row">
                            <div class="col-md-4">
                                <p><b>Ancho extensión</b></p>
                                <p class="text-capitalize">{{ $container -> width_extension}}</p>
                            </div>
                            <div class="col-md-4">
                                <p><b>Alto extensión</b></p>
                                <p class="text-capitalize">{{ $container -> height_extension}}</p>
                            </div>
                            <div class="col-md-4">
                                <p><b>Largo extensión</b></p>
                                <p class="text-capitalize">{{ $container -> long_extension}}</p>
                            </div>
                        </li>
                         <li class="row">
                            <div class="col-md-12">
                                <p><b>Pago total</b></p>
                                <p class="text-capitalize">{{ $container-> full_payment }}</p>
                            </div>
                        </li>
                         <li class="row">
                          <div class="col-md-12">
                                <p><b>Creador usuario</b></p>
                                <p class="text-capitalize">{{ $container-> full_payment }}</p>
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-12 col-sm-6">
                                <p><b>Creación</b></p>
                                <p class="text-capitalize">{{ date_format($container -> created_at, 'd/m/Y h:i:s A') }}</p>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <p><b>Modificación</b></p>
                                <p class="text-capitalize">{{ date_format($container -> updated_at, 'd/m/Y h:i:s A' ) }}</p>
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-md-4">
                                <p><b>Total container</b></p>
                                <p class="text-capitalize">{{ $container -> total_container}}</p>
                            </div>
                            <div class="col-md-4">
                                <p><b>Total pistón</b></p>
                                <p class="text-capitalize">{{ $container -> total_piston}}</p>
                            </div>
                            <div class="col-md-4">
                                <p><b>Total extensión</b></p>
                                <p class="text-capitalize">{{ $container -> total_extension}}</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('containers.destroy', $container->id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Container</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de eliminar el container <b>{{ $container -> name }}</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- end modal -->
@endsection

@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
            <div class="graphs">
                <div class="sign-in-form">
                    <div class="sign-in-form-top">
                        <span class="title">
                         Listado de {{ session('company')->system_container_name }} 
                        </span>
                        <div class="pull-right">
                            <a href="{{ route('home')}}" class="btn btn-warning" id="atras"><i class="fa fa-reply" aria-hidden="true"></i></a>
                            <a data-toggle="modal" data-target="#modal-add" type="button" class="btn btn-primary " title="Nuevo"> <i class="fa fa-plus-square-o" aria-hidden="true"></i></a>

                        </div>
                    </div>

                    <div class="signin">
                        <div class="log-input">
                    @include('flash::message')

                        <div class="table-responsive">
                        <table id="companydata" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <th>Código</th>
                                <th>TAG</th>
                                <th># Placa</th>
                                <th>Yardas</th>
                                <th>Ancho container</th>
                                <th>Alto container</th>
                                <th>Largo container</th>
                                <th>Ancho pistón</th>
                                <th>Alto pistón</th>
                                <th>Largo pistón</th>
                                <th>Ancho extensión</th>
                                <th>Alto extensión</th>
                                <th>Largo extensión</th>
                                <th>Total a pagar</th>
                                <th>Creador de usuario</th>
                                <th>Fecha de creación</th>
                                <th>Fecha de modificación</th>
                                <th>Total container</th>
                                <th>Total pistón</th>
                                <th>Total extensión</th>
                                <th>Código vehiculo</th>
                                <th>Opciones</th>
                            </thead>
                            <tbody>

                                    @foreach ($containers as $key => $element)
                                        <tr>
                                            <td>{{$element -> id}}</td>
                                            <td>{{ $element -> tag }}</td>
                                            <td>{{ $element-> plate_number }}</td>
                                            <td>{{ $element -> yards }}</td>
                                            <td>{{ $element -> width_container }}</td>
                                            <td>{{ $element -> height_container }}</td>
                                            <td>{{ $element -> long_container }}</td>
                                            <td>{{ $element -> width_piston}}</td>
                                            <td>{{ $element -> height_piston }}</td>
                                            <td>{{ $element -> long_piston }}</td>
                                            <td>{{ $element -> width_extension }}</td>
                                            <td>{{ $element -> height_extension }}</td>
                                            <td>{{ $element -> long_extension }}</td>
                                            <td>{{ $element-> full_payment }}</td>
                                            <td>{{ $element -> creator_user }}</td>
                                            <td>{{ $element -> created_at }}</td>
                                            <td>{{ $element -> updated_at }}</td>
                                            <td>{{ $element -> total_container }}</td>
                                            <td>{{ $element -> total_piston }}</td>
                                            <td>{{ $element -> total_extension }}</td>
                                            <td> {{$element -> vehicle->plate_number}}</td>
                                            <td>
                                                <button type="button" class="btn btn-primary " title="Modificar" data-toggle="modal" data-target="#modalEdit{{$key}}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>

                                                     <!-- Modal modificar registro -->
                                                     <div class="modal fade" id="modalEdit{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalEdit{{$key}}">
                                                         <form class="form-horizontal" action="{{ route('containers.update', $element->id) }}" method="POST" enctype="multipart/form-data">

                                                    
                                                             {{ csrf_field() }}
                                                             <input type="hidden" name="_method" value="EDIT">
                                                             <div class="modal-dialog" role="document">
                                                                 <div class="modal-content">
                                                                     <div class="modal-header">
                                                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                        
                                                                         <h4 class="modal-title" id="myModalLabel"  style="color:#007bff">Editar Elemento</h4>
                                                                         
                                                                     </div>  
                                                                        
                                                                        <div class="modal-footer text-center">                             
                                                                         <input type="hidden" name="_method" value="PUT">
                                                                         {{-- <div class="{{ $errors->has('vehicle_id') ? ' has-error' : '' }}">
                                                                            <label for="vehicle_id" class="col-md-4 control-label">{{session('company')->system_vehicle_name }}</label>

                                                                            <div class="col-md-6">
                                                                                <select id="vehicle_id" name="vehicle_id" class="form-control" required="required">
                                                                                    <option value="">Seleccione un {{ session('company')->system_vehicle_name }}...</option>
                                                                                    @foreach($vehicles as $vehicle)
                                                                                        <option value="{{ $vehicle->id }}"
                                                                                            @if ($element->vehicle_id = $vehicle->id)
                                                                                                selected="selected"
                                                                                            @endif>
                                                                                            {{ $vehicle -> plate_number }}
                                                                                        </option>
                                                                                    @endforeach
                                                                                </select>

                                                                                @if ($errors->has('vehicle_id'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('vehicle_id') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        <div class="{{ $errors->has('plate_number') ? ' has-error' : '' }}">
                                                                            <label for="plate_number" class="col-md-4 control-label">Número de Placa del {{session('company')->system_vehicle_name }}</label>

                                                                            <div class="col-md-6">
                                                                                <input id="plate_number" type="text" class="form-control" name="plate_number" value="{{ $element->plate_number }}" required autofocus>

                                                                                @if ($errors->has('plate_number'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('plate_number') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>--}}

                                                                        <div class="{{ $errors->has('tag') ? ' has-error' : '' }}">
                                                                            <label for="tag" class="col-md-4 control-label">Tag</label>

                                                                            <div class="col-md-6">
                                                                                <input id="tag" type="text" class="form-control" name="tag" value="{{ $element->tag }}"  autofocus>

                                                                                @if ($errors->has('tag'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('tag') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        <div class="{{ $errors->has('image') ? ' has-error' : '' }}">
                                                                            <label for="image" class="col-md-4 control-label">Fotos: {{ session('company')->system_vehicle_name }}</label>

                                                                            <div class="col-md-6">
                                                                                <input id="image" type="file" class="form-control" name="image" required>

                                                                                @if ($errors->has('image'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('image') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        <div class="{{ $errors->has('height_container') ? ' has-error' : '' }}">
                                                                            <label for="height_container" class="col-md-4 control-label">Altura del {{ session('company')->system_container_name }}</label>

                                                                            <div class="col-md-6">
                                                                                <input id="height_container" type="number" class="form-control" name="height_container" value="{{ $element->height_container }}" onchange="sumarcontainer(this.value);" required autofocus>

                                                                                @if ($errors->has('height_container'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('height_container') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        <div class="{{ $errors->has('width_container') ? ' has-error' : '' }}">
                                                                            <label for="width_container" class="col-md-4 control-label">Anchura del {{ session('company')->system_container_name }}</label>

                                                                            <div class="col-md-6">
                                                                                <input id="width_container" type="number" class="form-control" name="width_container" value="{{ $element->width_container }}" onchange="sumarcontainer(this.value);" required autofocus>

                                                                                @if ($errors->has('width_container'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('width_container') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        <div class="{{ $errors->has('long_container') ? ' has-error' : '' }}">
                                                                            <label for="long_container" class="col-md-4 control-label">Largo del {{ session('company')->system_container_name }}</label>

                                                                            <div class="col-md-6">
                                                                                <input id="long_container" type="number" class="form-control" name="long_container" value="{{ $element->long_container }}" onchange="sumarcontainer(this.value);" required autofocus>

                                                                                @if ($errors->has('long_container'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('long_container') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>                                                                     
                                                                      
                                                                          <div class="form-group{{ $errors->has('height_piston') ? ' has-error' : '' }}">
                                                                                        <label  class="col-md-4 control-label">El resultado es:<span id="total_container1" class="total_container1"></span></label>

                                                                                        <div class="col-md-6">
                                                                                             <input id="total_container" type="hidden" name="total_container" onchange="sumaryarda(this.value);" >
                                                                                         </div>
                                                                        </div>

                                                                        <div class="{{ $errors->has('height_piston') ? ' has-error' : '' }}">
                                                                            <label for="height_piston" class="col-md-4 control-label">Altura del Piston</label>

                                                                            <div class="col-md-6">
                                                                                <input id="height_piston" type="number" class="form-control" name="height_piston" value="{{ $element->height_piston }}" onchange="sumarpiston(this.value);"  autofocus>

                                                                                @if ($errors->has('height_piston'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('height_piston') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        <div class="{{ $errors->has('width_piston') ? ' has-error' : '' }}">
                                                                            <label for="width_piston" class="col-md-4 control-label">Anchura del Piston</label>

                                                                            <div class="col-md-6">
                                                                                <input id="width_piston" type="number" class="form-control" name="width_piston" value="{{ $element->width_piston }}" onchange="sumarpiston(this.value);"  autofocus>

                                                                                @if ($errors->has('width_piston'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('width_piston') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        <div class="{{ $errors->has('long_piston') ? ' has-error' : '' }}">
                                                                            <label for="long_piston" class="col-md-4 control-label">Largo del Piston</label>

                                                                            <div class="col-md-6">
                                                                                <input id="long_piston" type="number" class="form-control" name="long_piston" value="{{ $element->long_piston }}" onchange="sumarpiston(this.value);"  autofocus>

                                                                                @if ($errors->has('long_piston'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('long_piston') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        
                                                                        
                                                                          <div class="form-group{{ $errors->has('height_piston') ? ' has-error' : '' }}">
                                                                                        <label  class="col-md-4 control-label">El resultado es: <span id="total_piston1" class="total_piston1"></span></label>

                                                                                        <div class="col-md-6">
                                                                                             <input id="total_piston" type="hidden" name="total_piston" onchange="sumaryarda(this.value);" >
                                                                                         </div>
                                                                        </div>

                                                                        <div class="{{ $errors->has('height_extension') ? ' has-error' : '' }}">
                                                                            <label for="height_extension" class="col-md-4 control-label">Altura de la Extensión</label>

                                                                            <div class="col-md-6">
                                                                                <input id="height_extension" type="number" class="form-control" name="height_extension" value="{{ $element->height_extension }}" onchange="sumarextension(this.value);" required autofocus>

                                                                                @if ($errors->has('height_extension'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('height_extension') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        <div class="{{ $errors->has('width_extension') ? ' has-error' : '' }}">
                                                                            <label for="width_extension" class="col-md-4 control-label">Anchura de la Extensión</label>

                                                                            <div class="col-md-6">
                                                                                <input id="width_extension" type="number" class="form-control" name="width_extension" value="{{ $element->width_extension }}" onchange="sumarextension(this.value);" required autofocus>

                                                                                @if ($errors->has('width_extension'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('width_extension') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        <div class="{{ $errors->has('long_extension') ? ' has-error' : '' }}">
                                                                            <label for="long_extension" class="col-md-4 control-label">Largo de la Extensión</label>

                                                                            <div class="col-md-6">
                                                                                <input id="long_extension" type="number" class="form-control" name="long_extension" value="{{ $element->long_extension }}" onchange="sumarextension(this.value);"  autofocus>

                                                                                @if ($errors->has('long_extension'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('long_extension') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        
                                                                      
                                                                         <div class="form-group{{ $errors->has('height_piston') ? ' has-error' : '' }}">
                                                                                        <label  class="col-md-4 control-label">El resultado es: <span id="total_extension1" class="total_extension1"></span></label>

                                                                                        <div class="col-md-6">
                                                                                               <input id="total_extension" type="hidden" name="total_extension" onchange="sumaryarda(this.value);" >
                                                                                         </div>
                                                                        </div>

                                                                        <div class="{{ $errors->has('full_payment') ? ' has-error' : '' }}">
                                                                            <label for="full_payment" class="col-md-4 control-label">Precio total</label>

                                                                            <div class="col-md-6">
                                                                                <input id="full_payment" type="text" class="form-control" name="full_payment" value="{{ $element->full_payment }}"  autofocus>

                                                                                @if ($errors->has('full_payment'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('full_payment') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        
                                                                        
                                                                        <div class="form-group{{ $errors->has('height_piston') ? ' has-error' : '' }}">
                                                                                        <label  class="col-md-4 control-label">Total de la Yarda:  <span id="yards1" class="yards1"></span></label>

                                                                                        <div class="col-md-6">
                                                                                            <input id="yards" type="hidden" name="yards">
                                                                                         </div>
                                                                        </div>
                                                                                                                                                           
                                                                    </div>  
                                                                    <div style="text-align: center !important; padding: 1%;">                                                                    
                                                                    <button  title="Editar" type="submit" class="btn btn-primary">Actualizar</button>                                                                                                     
                                                                    </div>
                                                               </div>
                                                             </div><!-- end modalcontent -->
                                                            </div><!-- end modaldialog -->
                                                         </form>
                                                     </div>
                                                     <!-- end modal -->


                                                     <button  type="button" class="btn boton_modaldanger" data-toggle="modal" data-target="#modalDelete{{$key}}" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></button>


                                                     <!-- Modal eliminar registro -->
                                                      <div class="modal fade" id="modalDelete{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalDelete{{$key}}">
                                                          <form action="{{ route('containers.destroy', $element->id) }}" method="POST">
                                                              {{ csrf_field() }}
                                                              <input type="hidden" name="_method" value="DELETE">
                                                              <div class="modal-dialog" role="document">
                                                                  <div class="modal-content">
                                                                      <div class="modal-header">
                                                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                          <h4 class="modal-title" id="myModalLabel" style="color:#007bff">Eliminar Elemento</h4>
                                                                          
                                                                       </div>
                                                                      <div class="modal-body">
                                                                          <h5>¿Esta seguro de eliminar elemento <b>{{ $element -> id }}</b>?</h5>
                                                                      </div>
                                                                      <div class="modal-footer text-center">
                                                                          <button type="button" class="btn boton_modalwarning" data-dismiss="modal">Atrás</button>
                                                                          <button type="submit" class="btn boton_modaldanger">Eliminar</button>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </form>
                                                      </div>
                                                      <!-- end modal -->
                                            </td>
                                        </tr>
                                    @endforeach

                            </tbody>
                        </table>
                      </div>

                    </div>
                </div>
            </div>
        </div>
@stop
@section('modals')
    <!-- Modal agregar nuevo registro -->
    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="modal-add">
        <form class="form-horizontal" method="POST" action="{{ route('containers.store') }}" enctype="multipart/form-data" }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="ADD">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Agregar Elemento</h4>
                    </div>
                  <div class="modal-body">
                   <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                   {{-- <div class="form-group{{ $errors->has('vehicle_id') ? ' has-error' : '' }}">
                            <label for="vehicle_id" class="col-md-4 control-label">{{session('company')->system_vehicle_name }}</label>

                            <div class="col-md-6">
                                <select id="vehicle_id" name="vehicle_id" class="form-control" required="required">
                                    <option value="">Seleccione un {{ session('company')->system_vehicle_name }}...</option>
                                    @foreach($vehicles as $vehicle)
                                        <option value="{{ $vehicle->id }}">{{ $vehicle -> plate_number }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('vehicle_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('vehicle_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>--}}

                        <div class="form-group{{ $errors->has('plate_number') ? ' has-error' : '' }}">
                            <label for="plate_number" class="col-md-4 control-label">Número de Placa del {{session('company')->system_vehicle_name }}</label>

                            <div class="col-md-6">
                                <input id="plate_number" type="text" class="form-control" name="plate_number" value="{{ old('plate_number') }}" required autofocus>

                                @if ($errors->has('plate_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('plate_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('tag') ? ' has-error' : '' }}">
                            <label for="tag" class="col-md-4 control-label">Tag</label>

                            <div class="col-md-6">
                                <input id="tag" type="text" class="form-control" name="tag" value="{{ old('tag') }}" autofocus>

                                @if ($errors->has('tag'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tag') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="image" class="col-md-4 control-label">Fotos: {{ session('company')->system_vehicle_name }}</label>

                            <div class="col-md-6">
                                <input id="image" type="file" class="form-control" name="image[]" multiple>

                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('height_container') ? ' has-error' : '' }}">
                            <label for="height_container" class="col-md-4 control-label">Altura del {{ session('company')->system_container_name }}</label>

                            <div class="col-md-6">
                                <input id="height_container" type="number" class="form-control container" name="height_container" onchange="sumarcontainer(this.value);" required autofocus>
                                @if ($errors->has('height_container'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('height_container') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('width_container') ? ' has-error' : '' }}">
                            <label for="width_container" class="col-md-4 control-label">Anchura del {{ session('company')->system_container_name }}</label>

                            <div class="col-md-6">
                                <input id="width_container" type="number" class="form-control container" name="width_container" onchange="sumarcontainer(this.value);" required autofocus>
                                @if ($errors->has('width_container'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('width_container') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('long_container') ? ' has-error' : '' }}">
                            <label for="long_container" class="col-md-4 control-label">Largo del {{ session('company')->system_container_name }}</label>

                            <div class="col-md-6">
                                <input id="long_container" type="number" class="form-control container" name="long_container" onchange="sumarcontainer(this.value);" required autofocus>
                                @if ($errors->has('long_container'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('long_container') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <label>Total:</label> <label id="total_container1" class="total_container1"></label>
                                <div class="col-md-6">
                            <input id="total_container" class="form-control" type="hidden" name="total_container" onchange="sumaryarda(this.value);" >
                        </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('height_piston') ? ' has-error' : '' }}">
                            <label for="height_piston" class="col-md-4 control-label">Altura del Piston</label>

                            <div class="col-md-6">
                                <input id="height_piston" type="number" class="form-control piston" name="height_piston" onchange="sumarpiston(this.value);" autofocus>

                                @if ($errors->has('height_piston'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('height_piston') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('width_piston') ? ' has-error' : '' }}">
                            <label for="width_piston" class="col-md-4 control-label">Anchura del Piston</label>

                            <div class="col-md-6">
                                <input id="width_piston" type="number" class="form-control piston" name="width_piston" onchange="sumarpiston(this.value);" autofocus>

                                @if ($errors->has('width_piston'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('width_piston') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('long_piston') ? ' has-error' : '' }}">
                            <label for="long_piston" class="col-md-4 control-label">Largo del Piston</label>

                            <div class="col-md-6">
                                <input id="long_piston" type="number" class="form-control" name="long_piston" onchange="sumarpiston(this.value);"  autofocus>

                                @if ($errors->has('long_piston'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('long_piston') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <label>Total:</label>
                                <label id="total_piston1" class="total_piston1"></label>
                                <div class="col-md-6">
                                    <input id="total_piston" type="hidden" name="total_piston" onchange="sumaryarda(this.value);" >
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('height_extension') ? ' has-error' : '' }}">
                            <label for="height_extension" class="col-md-4 control-label">Altura de la Extensión</label>

                            <div class="col-md-6">
                                <input id="height_extension" type="number" class="form-control extension1" name="height_extension" onchange="sumarextension(this.value);"  autofocus>

                                @if ($errors->has('height_extension'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('height_extension') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('width_extension') ? ' has-error' : '' }}">
                            <label for="width_extension" class="col-md-4 control-label">Anchura de la Extensión</label>

                            <div class="col-md-6">
                                <input id="width_extension" type="number" class="form-control extension1" name="width_extension" onchange="sumarextension(this.value);"  autofocus>

                                @if ($errors->has('width_extension'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('width_extension') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('long_extension') ? ' has-error' : '' }}">
                            <label for="long_extension" class="col-md-4 control-label">Largo de la Extensión</label>

                            <div class="col-md-6">
                                <input id="long_extension" type="number" class="form-control extension1" name="long_extension" onchange="sumarextension(this.value);" autofocus>

                                @if ($errors->has('long_extension'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('long_extension') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <label>Total:</label>
                                <label id="total_extension1" class="total_extension1"></label>
                            <div class="col-md-6">
                                <input id="total_extension" type="hidden" name="total_extension" onchange="sumaryarda(this.value);" >
                            </div>
                        </div>
                        </div>
                        <div class="form-group{{ $errors->has('full_payment') ? ' has-error' : '' }}">
                            <label for="full_payment" class="col-md-4 control-label">Precio total</label>

                            <div class="col-md-6">
                                <input id="full_payment" type="text" class="form-control" name="full_payment" value="{{ old('full_payment') }}"  autofocus>

                                @if ($errors->has('full_payment'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('full_payment') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Total yardas:</label>
                                    <span id="yards1" class="yards1"></span>
                            <div class="col-md-6">
                                <input id="yards" type="hidden" name="yards">
                            </div>
                        </div>
                    </div>
                     
                      <div class="modal-footer text-center">
                                    <button type="button" class="btn boton_modalwarning" data-dismiss="modal">Atrás</button>
                                    <button type="submit" class="btn ">Aceptar</button>
                        </div>
                  </div>
              </div>          
            </div>
        </form>
    </div>
    <!-- end modal -->
@stop
@section('scripts')
<script type="text/javascript" src="{{ asset('assets/js/jquery-3.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/jquery-validation/lib/jquery-1.11.1.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/jquery-validation/dist/jquery.validate.js') }}"></script>
<script type="text/javascript">
// $(document).ready(function(){
    function sumarcontainer (valor) {
        var total = 0;
        valor = parseInt(valor); // Convertir el valor a un entero (número).

        total = document.getElementById('total_container1').innerHTML;

        // Aquí valido si hay un valor previo, si no hay datos, le pongo un cero "0".
        total = (total == null || total == undefined || total == "") ? 1 : total;

        /* Esta es la suma. */
        total = (parseInt(total) * parseInt(valor));
        total = Math.abs(total);
        // Colocar el resultado de la suma en el control "span".
        document.getElementById('total_container1').innerHTML = total;
        $('#total_container').val(total);
    }
       function sumarpiston (valor) {
        var total = 0;
        valor = parseInt(valor); // Convertir el valor a un entero (número).

        total = document.getElementById('total_piston1').innerHTML;

        // Aquí valido si hay un valor previo, si no hay datos, le pongo un cero "0".
        total = (total == null || total == undefined || total == "") ? 1 : total;

        /* Esta es la suma. */
        total = (parseInt(total) * parseInt(valor));
        total = Math.abs(total);
        // Colocar el resultado de la suma en el control "span".
        document.getElementById('total_piston1').innerHTML = total;
        $('#total_piston').val(total);
    }
     function sumarextension (valor) {
        var total = 0;
        valor = parseInt(valor); // Convertir el valor a un entero (número).

        total = document.getElementById('total_extension1').innerHTML;

        // Aquí valido si hay un valor previo, si no hay datos, le pongo un cero "0".
        total = (total == null || total == undefined || total == "") ? 1 : total;

        /* Esta es la suma. */
        total = (parseInt(total) * parseInt(valor));
        total = Math.abs(total);
        // Colocar el resultado de la suma en el control "span".
        document.getElementById('total_extension1').innerHTML = total;
        $('#total_extension').val(total);

        this.sumaryarda();
    }

    function sumaryarda () {
        var total = 0;

        total_c = $('#total_container').val();
        total_p = $('#total_piston').val();
        total_e = $('#total_extension').val();

        // Aquí valido si hay un valor previo, si no hay datos, le pongo un cero "0".
        total_c = (total_c == null || total_c == undefined || total_c == "") ? 0 : total_c;
        total_p = (total_p == null || total_p == undefined || total_p == "") ? 0 : total_p;
        total_e = (total_e == null || total_e == undefined || total_e == "") ? 0 : total_e;

        /* Esta es la suma. */
        total = (parseInt(total_c) + parseInt(total_e)) - parseInt(total_p);

        // Colocar el resultado de la suma en el control "span".
        document.getElementById('yards1').innerHTML = total;
        $('#yards').val(total);
    }
// });
</script>
@stop
@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" />
    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.12.4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/Scriptdata.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.print.min.js') }}"></script>

@endsection

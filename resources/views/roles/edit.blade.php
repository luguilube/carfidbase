@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                <div class="sign-in-form-top">
                    <span>Editar Roles</span>
                    <div class="pull-right">                                         
                        <a href="{{ route('roles.show', $role -> id)}}" class="btn btn-warning" id="atras">
                            <i class="fa fa-reply" aria-hidden="true"></i> Atrás
                        </a>
                    </div>
                </div>
                <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                    <form class="form-horizontal" method="POST" action="{{ route('roles.update', $role -> id) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $role->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Descripción</label>

                            <div class="col-md-6">
                                <input id="description" type="text" class="form-control" name="description" value="{{ $role->description }}" autofocus>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div  style="border-style: solid;border-color: #e0ddddb3;">
                            <label>Privilegios <span class="text-danger">*</span></label>
                            <table  class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <th> Todos <form action="#">
                                        <input type="checkbox" id="checkAll"/>      
                                            <fieldset></th>
                                    @foreach ($permissions as $permissión)
                                        <th>{{ ucwords($permissión->name) }}</th>
                                    @endforeach
                                </thead>
                                 </fieldset>
                                </form>
                                @php $a=0; $b=0; @endphp
                                @foreach($modules as $i)
                                <tr>
                                    <td style="text-transform:capitalize; font-weight:bold;">{{$i->name}}</td>
                                    @foreach($permissions as $j)
                                    <td>
                                        @if (count($rma)>0)
                                            {{-- <input name="{{$i -> id}}/{{$j -> id}}" type="checkbox" @if(($i -> id == $rma[$b]["module_id"]) && ($j -> id == $rma[$b]["permission_id"])) checked="checked" @endif> --}}
                                            <input class="{{$i->id}}" name="privileges[]" type="checkbox" @if(($i -> id == $rma[$b]["module_id"]) && ($j -> id == $rma[$b]["permission_id"])) checked="checked" @endif value="{{$i -> id}}/{{$j -> id}}">
                                            @if(($i -> id == $rma[$b]["module_id"]) && ($j -> id == $rma[$b]["permission_id"]) && ($b < count($rma) -1))
                                                @php $b++; @endphp
                                            @endif
                                        @else
                                            <input  class="{{$i->id}}" name="privileges[]" type="checkbox" value="{{$i -> id}}/{{$j -> id}}">
                                        @endif
                                    </td>
                                    @endforeach
                                </tr>
                                @endforeach
                            </table>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Actualizar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
               </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" /> 
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" /> 
    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.12.4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/Scriptdata.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.print.min.js') }}"></script>
@endsection

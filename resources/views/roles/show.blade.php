@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
    <div class="graphs">
        <div class="sign-in-form">          
             <div class="sign-in-form-top">
                  <span>Compañia</span>
                  <div class="pull-right">                      
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>Eliminar
                    </button>
                    <a href="{{ route('roles.index') }}" type="button" class="btn btn-warning"> 
                        <i class="fa fa-reply" aria-hidden="true">Atrás</i>
                    <a href="{{ route('roles.edit', $role -> id) }}" type="button" class="btn btn-primary "> 
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>Editar
                    </a>
                </div>
            </div>
            @include('flash::message')
            <div class="signin">
                <div class="log-input">
                    <ul class="panel-list" style="list-style:none">
                        <li class="row">
                            <div class="col-xs-12 col-sm-6">
                                <p><b>Nombre de rol</b></p>
                                <p class="text-capitalize">{{ $role -> name }}</p>
                            </div>
                        </li>
                        <li>
                            <p><b>Descripción</b></p>
                            <p>{{ $role -> description }}</p>
                        </li>
                        <li class="row">
                            <div class="col-xs-12 col-sm-6">
                                <p><b>Fecha de creación</b></p>
                                <p>{{ date_format($role -> created_at, 'd/m/Y') }}</p>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <p><b>Fecha de modificación</b></p>
                                <p>{{ date_format($role -> updated_at, 'd/m/Y') }}</p>
                            </div>
                        </li>
                        <li>
                        <div style="border-style: solid;border-color: #e0ddddb3;">
                            <label>Privilegios</label>
                            <table  class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <th></th>
                                    @foreach($permissions as $permission)
                                        <th>{{ ucwords($permission->name) }}</th>
                                    @endforeach
                                </thead>
                                @php $a=0; $b=0; @endphp
                                @foreach($modules as $i)
                                <tr>
                                    <td style="text-transform:capitalize; font-weight:bold;">{{$i->name}}</td>
                                    @foreach($permissions as $j)
                                    <td>
                                        @if (count($rma) > 0)
                                            @if(($i -> id == $rma[$b]["module_id"]) && ($j -> id == $rma[$b]["permission_id"]))
                                                <p class="fa fa-check text-success">Si</p>
                                            @else
                                                <p class="fa fa-check text-danger">No</p>
                                            @endif
                                            @if(($i -> id == $rma[$b]["module_id"]) && ($j -> id == $rma[$b]["permission_id"]) && ($b < count($rma) -1))
                                                @php $b++; @endphp
                                            @endif
                                        @else
                                            <p class="fa fa-check text-danger">No</p>
                                        @endif
                                    </td>
                                    @endforeach
                                </tr>
                                @endforeach
                            </table>
                          <div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" /> 
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" /> 
    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.12.4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/Scriptdata.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.print.min.js') }}"></script>
@stop
@section('modals')
    <!-- Modal DELETE-->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('roles.destroy', $role->id) }}" method="POST"> 
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar rol</h4>
                    </div>
                    <div class="modal-body">     
                        <p>¿Esta seguro de eliminar el rol <b>{{ $role -> name }}</b>?</p>               
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>            
        </form>
    </div>
    <!-- end modal DELETE-->
@endsection

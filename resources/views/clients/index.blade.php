@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
			<div class="graphs">
				<div class="sign-in-form">
					<div class="sign-in-form-top">
						<span class="title" >Clientes</span>
    					<div class="pull-right">
    						<a href="{{ route('home')}}" class="btn btn-warning" id="atras"><i class="fa fa-reply" aria-hidden="true"></i></a>  
    						<a data-toggle="modal" data-target="#modal-add" type="button" class="btn btn-primary " title="Nuevo"> <i class="fa fa-plus-square-o" aria-hidden="true"></i></a>
    					</div>
				    </div>
					<div class="signin">
						<div class="log-input">
				        @include('flash::message')			

					<table  id="companydata" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
							<th>Código</th>
							<th>Nombre</th>
							<th>Dirección</th>
							<th>Teléfono</th>
							<th>RUC</th>
							<th>Usuario creador</th>
							<th>Compañia</th>
							<th>Creacion</th>
							<th>Actulización</th>
              <th>Opciones</th>
						</thead>
						<tbody>

								@foreach ($clients as $key => $client)
									<tr>
										<td {{ $client-> code }}</td>
										<td>{{ $client-> name }}</td>
										<td>{{ $client -> address }}</td>
										<td>{{ $client-> phone_number }}</td>
										<td>{{ $client-> ruc }}</td>
										<td>{{ $client ->creator_user}}</td>
										<td>{{ $client-> company->name }}</td>
										<td>{{ $client->created_at }}</td>
                                        <td>{{ $client->updated_at }}</td>
                                        <td>
                                            <button type="button" class="btn btn-primary " title="Modificar" data-toggle="modal" data-target="#modalEdit{{$key}}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>

                                                     <!-- Modal modificar registro -->
                                                     <div class="modal fade" id="modalEdit{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalEdit{{$key}}">
                                                         <form class="form-horizontal" action="{{ route('clients.update', $client->id) }}" method="POST" enctype="multipart/form-data">

                                                    
                                                             {{ csrf_field() }}
                                                             <input type="hidden" name="_method" value="EDIT">
                                                                      <input type="hidden" name="_method" value="EDIT">
                                                             <div class="modal-dialog" role="document">
                                                                 <div class="modal-content">
                                                                     <div class="modal-header">
                                                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                        
                                                                         <h4 class="modal-title" id="myModalLabel"  style="color:#007bff">Cliente</h4>
                                                                         
                                                                     </div>  
                                                                        
                                                                     <div class="modal-footer text-center"> 
                                                                     <div class="{{ $errors->has('code') ? ' has-error' : '' }}">
                                                                        <label for="code" class="col-md-4 control-label">Código</label>

                                                                        <div class="col-md-6">
                                                                            <input id="code" type="text" class="form-control" name="code" value="{{ $client->code }}" autofocus>

                                                                            @if ($errors->has('code'))
                                                                                <span class="help-block">
                                                                                    <strong>{{ $errors->first('code') }}</strong>
                                                                                </span>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                    <div class="{{ $errors->has('name') ? ' has-error' : '' }}">
                                                                        <label for="name" class="col-md-4 control-label">Nombre</label>

                                                                        <div class="col-md-6">
                                                                            <input id="name" type="text" class="form-control" name="name" value="{{ $client->name }}" required autofocus>

                                                                            @if ($errors->has('name'))
                                                                                <span class="help-block">
                                                                                    <strong>{{ $errors->first('name') }}</strong>
                                                                                </span>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                    <div class="{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                                                        <label for="phone_number" class="col-md-4 control-label">Número de Teléfono</label>

                                                                        <div class="col-md-6">
                                                                            <input id="phone_number" type="text" class="form-control" name="phone_number" value="{{ $client->phone_number }}" autofocus>

                                                                            @if ($errors->has('phone_number'))
                                                                                <span class="help-block">
                                                                                    <strong>{{ $errors->first('phone_number') }}</strong>
                                                                                </span>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                    <div class="{{ $errors->has('address') ? ' has-error' : '' }}">
                                                                        <label for="address" class="col-md-4 control-label" style="padding: 10px;">Dirección</label>

                                                                        <div class="col-md-6" style="padding: 10px;">
                                                                            <textarea id="address" type="text" class="form-control" style="width: 97%;" name="address"  autofocus>{{ $client->address }}</textarea>

                                                                            @if ($errors->has('address'))
                                                                                <span class="help-block">
                                                                                    <strong>{{ $errors->first('address') }}</strong>
                                                                                </span>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                    <div class="{{ $errors->has('ruc') ? ' has-error' : '' }}">
                                                                        <label for="ruc" class="col-md-4 control-label">RUC</label>

                                                                        <div class="col-md-6">
                                                                            <input id="ruc" type="text" class="form-control" name="ruc" value="{{ $client->ruc }}" autofocus>

                                                                            @if ($errors->has('ruc'))
                                                                                <span class="help-block">
                                                                                    <strong>{{ $errors->first('ruc') }}</strong>
                                                                                </span>
                                                                            @endif
                                                                        </div>
                                                                    </div>                                                                                                        
                                                                    </div>  
                                                                    <div style="text-align: center !important; padding: 1%;">
                                                                        <button  title="Editar" type="submit" class="btn btn-primary">Actualizar</button>                                 </div>
                                                               </div>
                                                             </div><!-- end modalcontent -->
                                                            </div><!-- end modaldialog -->
                                                         </form>
                                                     </div>
                                                     <!-- end modal -->


                                                     <button  type="button" class="btn boton_modaldanger" data-toggle="modal" data-target="#modalDelete{{$key}}" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></button>


                                                     <!-- Modal eliminar registro -->
                                                      <div class="modal fade" id="modalDelete{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalDelete{{$key}}">
                                                          <form action="{{ route('clients.destroy', $client->id) }}" method="POST">
                                                              {{ csrf_field() }}
                                                              <input type="hidden" name="_method" value="DELETE">
                                                              <div class="modal-dialog" role="document">
                                                                  <div class="modal-content">
                                                                      <div class="modal-header">
                                                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                          <h4 class="modal-title" id="myModalLabel" style="color:#007bff">Eliminar Cliente</h4>
                                                                          
                                                                       </div>
                                                                      <div class="modal-body">
                                                                          <h5>¿Esta seguro de eliminar el Cliente <b>{{ $client -> name }}</b>?</h5>
                                                                      </div>
                                                                      <div class="modal-footer text-center">
                                                                          <button type="button" class="btn boton_modalwarning" data-dismiss="modal">Atrás</button>
                                                                          <button type="submit" class="btn boton_modaldanger">Eliminar</button>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </form>
                                                      </div>
                                                      <!-- end modal -->
                                        </td>
									</tr>
								@endforeach

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('modals')
    <!-- Modal agregar nuevo registro -->
    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="modal-add">
        <form class="form-horizontal" method="POST" action="{{ route('clients.store') }}" enctype="multipart/form-data" }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="ADD">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Agregar Cliente</h4>
                    </div>
                  <div class="modal-body">
                   <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                     
                     <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                            <label for="phone_number" class="col-md-4 control-label">Teléfono</label>
                            <div class="col-md-6">
                                <input id="phone_number" type="text" class="form-control" name="phone_number" value="{{ old('phone_number') }}" autofocus>

                                @if ($errors->has('phone_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">Dirección</label>

                            <div class="col-md-6">
                                <textarea id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required autofocus></textarea>

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('ruc') ? ' has-error' : '' }}">
                            <label for="ruc" class="col-md-4 control-label">RUC</label>

                            <div class="col-md-6">
                                <input id="ruc" type="text" class="form-control" name="ruc" value="{{ old('ruc') }}" autofocus>

                                @if ($errors->has('ruc'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ruc') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-4">

                                <button type="submit" class="btn btn-success">Aceptar</button> 
                  </div>
              </div>          
            </div>
        </form>
    </div>
    <!-- end modal -->
@stop
@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" />
    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.12.4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/Scriptdata.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.print.min.js') }}"></script>
@endsection

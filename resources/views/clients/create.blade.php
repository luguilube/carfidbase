@extends('layouts.app')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />

@section('content')
<div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                <div class="sign-in-form-top">
                    <span>Cliente - Nuevo</span>
                    <div class="pull-right">
                        <a href="{{ route('clients.index') }}" type="button" class="btn btn-warning"> <i class="fa fa-reply" aria-hidden="true"></i> Atras</a>                           
                    </div>
                 </div>
                <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')
                    <form class="form-horizontal" method="POST" action="{{ route('clients.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                            <label for="phone_number" class="col-md-4 control-label">Teléfono</label>
                            <div class="col-md-6">
                                <input id="phone_number" type="text" class="form-control" name="phone_number" value="{{ old('phone_number') }}" autofocus>

                                @if ($errors->has('phone_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">Dirección</label>

                            <div class="col-md-6">
                                <textarea id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required autofocus></textarea>

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('ruc') ? ' has-error' : '' }}">
                            <label for="ruc" class="col-md-4 control-label">RUC</label>

                            <div class="col-md-6">
                                <input id="ruc" type="text" class="form-control" name="ruc" value="{{ old('ruc') }}" autofocus>

                                @if ($errors->has('ruc'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ruc') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-4">

                                <button type="submit" class="btn btn-success">
                                    Aceptar
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')

<script type="text/javascript" src="{{ asset('assets/plugins/jquery-1.11.1.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/dist/jquery.validate.js') }}"></script>


@endsection

@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                 <div class="sign-in-form-top">
                      <span>Cliente - Ver</span>
				
				<div class="pull-right">
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</button>
					<a href="{{ route('clients.index')}}" class="btn btn-warning" id="atras"><i class="fa fa-reply" aria-hidden="true"></i> Atrás</a>  
					<a href="{{ route('clients.edit', $client->id) }}" type="button" class="btn btn-primary"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
				</div>
                </div>

            
          <div class="signin">
            <div class="log-input">
					@include('flash::message')
					@include('errors')
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="amount"><b>Código </b>{{ $client -> code }}</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="amount"><b>Nombre</b> {{ $client -> name }}</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="amount"><b>Teléfono</b> {{ $client -> phone_number }}</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="amount"><b>Dirección</b> {{ $client -> address }}</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="amount"><b>RUC</b> {{ $client -> ruc }}</label>
								</div>
							</div>
						</div>
					</div>
				</div>

			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left m-t-6">Vehiculos</h3>
				</div>
				<div class="panel-body row">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="sample_1">
							<thead>
								<tr>
									<th>ID</th>
									<th># Placa</th>
									<th>TAG</th>
									<th>Creador de usuario</th>
									<th>Código del cliente</th>
									<th>Fecha creación</th>
									<th>Fecha modificación</th>
								</tr>
							</thead>
							<tbody>
								@foreach($client -> vehicle as $element)
									<tr>
										<td> <a style="color: #3097D1;" href="{{route('vehicles.show', $element -> id )}}">{{$element -> id}}</a></td>
										<td>{{ $element -> plate_number }}</td>
										<td class="text-capitalize">{{$element -> tag}}</td>
										<td>{{$element -> creator_user }}</td>
										<td>{{$element -> client -> name }}</td>
										<td> {{$element -> created_at}}</td>
										<td> {{$element -> updated_at }}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
      </div>
	</div>
</div>
@stop
@section('modals')
<!-- Modal -->
<!-- MODAL PARA ELIMINAR REGISTRO -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
    <form action="{{ route('clients.destroy', $client -> id) }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="modal-delete" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Eliminar cliente</h4>
                </div>
                <div class="modal-body">
                    <p>¿Esta seguro de eliminar el cliente <b>{{ $client -> name}}</b>?</p>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- END Modal  -->
<!-- END MODAL PARA ELIMINAR REGISTRO -->
@endsection

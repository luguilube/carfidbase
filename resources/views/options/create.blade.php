@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                <div class="sign-in-form-top">
                <span>Nuevo - Opción</span>
                 <div class="pull-right">                       
                        <a href="{{ route('options.index') }}" type="button" class="btn btn-warning"> <i class="fa fa-reply" aria-hidden="true"></i>Atras</a>                           
                    </div>                   
                 </div>
                <div class="signin">
                    <div class="log-input">

                
                    @include('flash::message')
                    @include('errors')
                    <form class="form-horizontal" method="POST" action="{{ route('options.store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <input type="hidden" name="total_positions" value="{{ $total_positions }}">

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Título</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" autofocus>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                            <label for="url" class="col-md-4 control-label">URL</label>

                            <div class="col-md-6">
                                <input id="url" type="text" class="form-control" name="url" value="{{ old('url') }}" required autofocus>

                                @if ($errors->has('url'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                            <label for="position" class="col-md-4 control-label">Posición</label>

                            <div class="col-md-6">
                                <select id="position" name="position" class="form-control" required="required">
                                    @for ($i=1; $i <= $total_positions; $i++)
                                        <option value="{{ $i }}"
                                        @if ($i == $total_positions)
                                            selected="selected"
                                        @endif>
                                        {{ $i }}</option>
                                    @endfor
                                </select>

                                @if ($errors->has('position'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="image" class="col-md-4 control-label">Icono</label>

                            <div class="col-md-6">
                                <input id="image" type="file" class="form-control" name="image" required>

                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group"><div class="col-md-4 col-md-offset-4">
                           
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                                    Aceptar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
   
@endsection

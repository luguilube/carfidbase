@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
        <div class="graphs">
            <div class="sign-in-form">
                 <div class="sign-in-form-top">
                      <span>Opción - Ver</span>
						
	                    <div class="pull-right text-center">
	                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar </button>
	                        <a href="{{ route('options.index') }}" type="button" class="btn btn-warning"> <i class="fa fa-reply" aria-hidden="true"></i> Atrás</a>
	                        <a href="{{ route('options.edit', $option -> id) }}" type="button" class="btn btn-primary "> <i class="fa fa-pencil-square-o" aria-hidden="true"></i>Editar</a>
	                       </div>
                </div>

            
          <div class="signin">
            <div class="log-input">
					@include('flash::message')
					@include('errors')
				   	 <div class="panel-body">
                     <ul style="list-style:none;">
                        <li class="row">
                            <div class="col-md-4">
                                <p><b>TÍtulo</b></p>
                                <p>{{ $option -> title }}</p>
                            </div>
                            <div class="col-md-4">
                                <p><b>URL</b></p>
                                <p class="text-capitalize">{{ $option -> url }}</p>
                            </div>
                            <div class="col-md-4">
                                <p><b>Posición</b></p>
                                <p>{{ $option -> position }}</p>
                            </div>
                        </li>
                     </ul>
			 	</div>
		 	</div>
		</div>
	</div>
   </div>
</div>
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('options.destroy', $option->id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Opción</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de eliminar la opción <b>{{ $option -> title }}</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- end modal -->
@endsection

@extends('layouts.app')

@section('content')
<div class="sign-in-wrapper">
            <div class="graphs">
                <div class="sign-in-form">
                    <div class="sign-in-form-top">
                    <span class="title">Opciones</span>
                        <div class="pull-right">
                            <a href="{{ route('home')}}" class="btn btn-md btn-warning" id="atras">
                                <i class="fa fa-reply" aria-hidden="true"></i></a>     
                            <a data-toggle="modal" data-target="#modal-add" type="button" class="btn btn-primary " title="Nuevo"> <i class="fa fa-plus-square-o" aria-hidden="true"></i></a>
                                              
                        </div>
                    </div>              
                    
                    <div class="signin">  
                        <div class="log-input">
                          @include('flash::message')               
                             <table id="companydata" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <th>Título</th>
                                    <th>URL</th>
                                    <th>Posición</th>
                                    <th>Opciones</th>
                                </thead>
                                <tbody>
                                  
                                        @foreach ($options as $key => $option)
                                            <tr>
                                                <td>{{ $option->title }}</td>
                                                <td>{{ $option->url }}</td>
                                                <td>{{ $option->position }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-primary " title="Modificar" data-toggle="modal" data-target="#modalEdit{{$key}}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>

                                                     <!-- Modal modificar registro -->
                                                     <div class="modal fade" id="modalEdit{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalEdit{{$key}}">
                                                         <form class="form-horizontal" action="{{ route('options.update', $option->id) }}" method="POST" enctype="multipart/form-data">

                                                    
                                                             {{ csrf_field() }}
                                                             <input type="hidden" name="_method" value="EDIT">
                                                             <div class="modal-dialog" role="document">
                                                                 <div class="modal-content">
                                                                     <div class="modal-header">
                                                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                        
                                                                         <h4 class="modal-title" id="myModalLabel"  style="color:#007bff">Editar Rol</h4>
                                                                         
                                                                     </div>  
                                                                        
                                                                        <div class="modal-footer text-center">                             
                                                                         <input type="hidden" name="_method" value="PUT">
                                                                   {{-- <input type="hidden" name="total_positions" value="{{ $total_positions }}">--}}

                                                                    <div class="{{ $errors->has('title') ? ' has-error' : '' }}">
                                                                        <label for="title" class="col-md-4 control-label">Título</label>

                                                                        <div class="col-md-6">
                                                                            <input id="title" type="text" class="form-control" name="title" value="{{ $option->title }}" autofocus>

                                                                            @if ($errors->has('title'))
                                                                                <span class="help-block">
                                                                                    <strong>{{ $errors->first('title') }}</strong>
                                                                                </span>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                    <div class="{{ $errors->has('url') ? ' has-error' : '' }}">
                                                                        <label for="url" class="col-md-4 control-label">URL</label>

                                                                        <div class="col-md-6">
                                                                            <input id="url" type="text" class="form-control" name="url" value="{{ $option->url }}" required autofocus>

                                                                            @if ($errors->has('url'))
                                                                                <span class="help-block">
                                                                                    <strong>{{ $errors->first('url') }}</strong>
                                                                                </span>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                   {{-- <div class="{{ $errors->has('position') ? ' has-error' : '' }}">
                                                                        <label for="position" class="col-md-4 control-label">Posición</label>

                                                                        <div class="col-md-6">
                                                                            <select id="position" name="position" class="form-control" required="required">
                                                                                @for ($i=1; $i <= $total_positions; $i++)
                                                                                    <option value="{{ $i }}"
                                                                                    @if ($i == $option->position)
                                                                                        selected="selected"
                                                                                    @endif>
                                                                                    {{ $i }}</option>
                                                                                @endfor
                                                                            </select>

                                                                            @if ($errors->has('position'))
                                                                                <span class="help-block">
                                                                                    <strong>{{ $errors->first('position') }}</strong>
                                                                                </span>
                                                                            @endif
                                                                        </div>
                                                                    </div>--}}

                                                                    <div class="{{ $errors->has('image') ? ' has-error' : '' }}">
                                                                        <label for="image" class="col-md-4 control-label">Icono</label>

                                                                        <div class="col-md-6">
                                                                            <input id="image" type="file" class="form-control" name="image" required>

                                                                            @if ($errors->has('image'))
                                                                                <span class="help-block">
                                                                                    <strong>{{ $errors->first('image') }}</strong>
                                                                                </span>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                                                                                                                                                       
                                                                    </div>  
                                                                    <div style="text-align: center !important; padding: 1%;">                                                                    
                                                                    <button  title="Editar" type="submit" class="btn btn-primary">Actualizar</button>                                                                                                     
                                                                    </div>
                                                               </div>
                                                             </div><!-- end modalcontent -->
                                                            </div><!-- end modaldialog -->
                                                         </form>
                                                     </div>
                                                     <!-- end modal -->


                                                     <button  type="button" class="btn boton_modaldanger" data-toggle="modal" data-target="#modalDelete{{$key}}" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></button>


                                                     <!-- Modal eliminar registro -->
                                                      <div class="modal fade" id="modalDelete{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalDelete{{$key}}">
                                                          <form action="{{ route('options.destroy', $option->id) }}" method="POST">
                                                              {{ csrf_field() }}
                                                              <input type="hidden" name="_method" value="DELETE">
                                                              <div class="modal-dialog" role="document">
                                                                  <div class="modal-content">
                                                                      <div class="modal-header">
                                                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                          <h4 class="modal-title" id="myModalLabel" style="color:#007bff">Eliminar opción </h4>
                                                                          
                                                                       </div>
                                                                      <div class="modal-body">
                                                                          <h5>¿Esta seguro de eliminar la opción <b>{{ $option -> title }}</b>?</h5>
                                                                      </div>
                                                                      <div class="modal-footer text-center">
                                                                          <button type="button" class="btn boton_modalwarning" data-dismiss="modal">Atrás</button>
                                                                          <button type="submit" class="btn boton_modaldanger">Eliminar</button>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </form>
                                                      </div>
                                                      <!-- end modal -->
                                                </td>
                                            </tr>
                                        @endforeach
                                    
                                </tbody>
                            </table>                    
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('modals')
    <!-- Modal agregar nuevo registro -->
    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="modal-add">
        <form class="form-horizontal" method="POST" action="{{ route('options.store') }}" enctype="multipart/form-data" }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="ADD">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Agregar Opciones</h4>
                    </div>
                  <div class="modal-body">
                   <div class="signin">
                    <div class="log-input">
                    @include('flash::message')
                    @include('errors')

                        {{--<input type="hidden" name="total_positions" value="{{ $total_positions }}">--}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Título</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" autofocus>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                            <label for="url" class="col-md-4 control-label">URL</label>

                            <div class="col-md-6">
                                <input id="url" type="text" class="form-control" name="url" value="{{ old('url') }}" required autofocus>

                                @if ($errors->has('url'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       {{--<div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                            <label for="position" class="col-md-4 control-label">Posición</label>

                            <div class="col-md-6">
                                <select id="position" name="position" class="form-control" required="required">
                                    @for ($i=1; $i <= $total_positions; $i++)
                                        <option value="{{ $i }}"
                                        @if ($i == $total_positions)
                                            selected="selected"
                                        @endif>
                                        {{ $i }}</option>
                                    @endfor
                                </select>

                                @if ($errors->has('position'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>--}} 

                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="image" class="col-md-4 control-label">Icono</label>

                            <div class="col-md-6">
                                <input id="image" type="file" class="form-control" name="image" required>

                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                     <div class="modal-footer text-center">
                                <button type="button" class="btn boton_modalwarning" data-dismiss="modal">Atrás</button>
                                <button type="submit" class="btn ">Aceptar</button>
                    </div>
                     
                  </div>

              </div>          
            </div>

        </form>
    </div>
    <!-- end modal -->
@stop
@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" /> 
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" /> 
    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.12.4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/Scriptdata.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/buttons.print.min.js') }}"></script>
@endsection
